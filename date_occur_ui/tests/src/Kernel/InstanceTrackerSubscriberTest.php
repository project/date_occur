<?php

namespace Drupal\Tests\date_occur_ui\Kernel;

use Prophecy\PhpUnit\ProphecyTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Tests\date_occur_sapi\Kernel\LoggingTrackerDecorator;

/**
 * Test tracker with occurrence instances.
 *
 * @group date_recur_recurrence
 */
class InstanceTrackerSubscriberTest extends DateOccurInstanceTestBase {

  use ProphecyTrait;
  /**
   * A mock tracker.
   *
   * @var \Drupal\search_api\Tracker\TrackerInterface
   */
  protected $mockTracker;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $index = $this->createTestIndex();
    $this->mockTracker = new LoggingTrackerDecorator([], 'default', [], $index->getTrackerInstance());
    $index->setTracker($this->mockTracker);

    $index_storage = $this->prophesize(EntityStorageInterface::class);
    $index_storage->loadMultiple()->willReturn([$index]);

    // Ensure the loadMultiple call to the SAPI index storage will return the
    // version with the mocked tracker.
    $entity_type_manager = $this->container->get('entity_type.manager');
    $manager_reflection = new \ReflectionObject($entity_type_manager);
    $handlers = $manager_reflection->getProperty('handlers');
    $handlers->setAccessible(TRUE);
    $handlers->setValue($entity_type_manager, [
      'storage' => [
        'search_api_index' => $index_storage->reveal(),
      ],
    ]);
  }

  /**
   * Check recurrence occurrence reference indexed.
   */
  public function testOnSaveFullEntityLifecycle() {
    $recur_instance = $this->createTestEntity();
    $this->assertTrackerState([
      'trackItemsInserted',
      [
        'date_occur:entity_test__date_recur_field/1:0:2014-06-15T23:00:00Z--2014-06-16T07:00:00Z',
        'date_occur:entity_test__date_recur_field/1:0:2014-06-16T23:00:00Z--2014-06-17T07:00:00Z',
        'date_occur:entity_test__date_recur_field/1:0:2014-06-17T23:00:00Z--2014-06-18T07:00:00Z',
        'date_occur:entity_test__date_recur_field/1:1:2011-12-14T23:00:00Z--2011-12-14T23:30:00Z',
        'date_occur:entity_test__date_recur_field/1:1:2011-12-15T23:00:00Z--2011-12-15T23:30:00Z',
        'date_occur:entity_test__date_recur_field/1:1:2011-12-20T23:00:00Z--2011-12-20T23:30:00Z',
        'date_occur:entity_test__date_recur_field/1:1:2011-12-21T23:00:00Z--2011-12-21T23:30:00Z',
        'date_occur:entity_test__date_recur_field/1:1:2011-12-22T23:00:00Z--2011-12-22T23:30:00Z',
      ],
    ]);

    // Create instance.
    $recurrence_instance = $recur_instance->createDuplicate();
    $recurrence_instance->date_recur_field = [
      'value' => '2014-06-17T00:00:00',
      'end_value' => '2014-06-17T08:00:00',
      'timezone' => 'Australia/Sydney',
    ];
    $recurrence_instance->occur_parent_reference = [
      [
        'target_id' => $recur_instance->id(),
        'field_delta' => 0,
        'value' => '2014-06-17T23:00:00',
        'end_value' => '2014-06-18T07:00:00',
      ],
    ];
    $recurrence_instance->save();
    $this->assertTrackerState([
      'trackItemsUpdated',
      [
        'date_occur:entity_test__date_recur_field/1:0:2014-06-17T23:00:00Z--2014-06-18T07:00:00Z',
      ],
    ]);

    // Update instance.
    $recurrence_instance->date_recur_field = [
      'value' => '2014-06-17T02:00:00',
      'end_value' => '2014-06-17T03:00:00',
      'timezone' => 'Australia/Sydney',
    ];
    $recurrence_instance->save();
    $this->assertTrackerState([
      'trackItemsUpdated',
      [
        'date_occur:entity_test__date_recur_field/1:0:2014-06-17T23:00:00Z--2014-06-18T07:00:00Z',
      ],
    ]);

    // Delete instance.
    $recurrence_instance->delete();
    $this->assertTrackerState([
      'trackItemsUpdated',
      [
        'date_occur:entity_test__date_recur_field/1:0:2014-06-17T23:00:00Z--2014-06-18T07:00:00Z',
      ],
    ]);

  }

  /**
   * Assert calls expected to the tracker and reset it's internal state.
   */
  protected function assertTrackerState(array $expectedCalls): void {
    // Unroll the format of expected calls in the case where there is only one
    // call to simplify the assertion format.
    $actual_calls = count($this->mockTracker->calls) === 1 ? $this->mockTracker->calls[0] : $this->mockTracker->calls;
    try {
      $this->assertEquals($expectedCalls, $actual_calls);
    }
    catch (\Exception $e) {
      $this->fail(sprintf("Calls to the tracker did not match the expected calls, \n\n %s \n\n actual calls were: \n\n %s", var_export($expectedCalls, TRUE), var_export($actual_calls, TRUE)));
    }
    $this->mockTracker->reset();
  }

}
