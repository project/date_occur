<?php

namespace Drupal\Tests\date_occur_ui\Kernel;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\field\FieldConfigInterface;
use Drupal\field\FieldStorageConfigInterface;
use Drupal\Tests\date_occur_sapi\Kernel\DateOccurSapiKernelTestBase;

/**
 * Base for tests with date_recur and date_occur_parent fields.
 */
class DateOccurInstanceTestBase extends DateOccurSapiKernelTestBase {

  /**
   * Entity Field Manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface|null
   */
  protected ?EntityFieldManagerInterface $entityFieldManager;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'date_occur',
    'date_occur_sapi',
    'date_occur_ui',
    'entity_test',
    'datetime',
    'datetime_range',
    'search_api',
    'search_api_db',
    'date_recur',
    'field',
    'replicate',
    'user',
  ];

  /**
   * Recurring Date Occurrence field storge.
   *
   * @var \Drupal\field\FieldStorageConfigInterface
   */
  protected FieldStorageConfigInterface $parentReferenceFieldStorage;

  /**
   * Recurring date field configuration.
   *
   * @var \Drupal\field\FieldConfigInterface
   */
  protected FieldConfigInterface $parentReferenceFieldConfig;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig('date_occur');
    $this->entityFieldManager = \Drupal::service('entity_field.manager');

    $this->recurFieldStorage->setCardinality(1);
    $this->recurFieldStorage->save();

    // Add a recurrence field to the test entity.
    $this->parentReferenceFieldStorage = FieldStorageConfig::create([
      'entity_type' => 'entity_test',
      'field_name' => 'occur_parent_reference',
      'type' => 'date_occur_parent',
      'settings' => [
        'target_type' => 'entity_test',
        'target_field' => 'date_recur_field',
      ],
    ]);
    $this->parentReferenceFieldStorage->save();

    $field = [
      'field_name' => 'occur_parent_reference',
      'entity_type' => 'entity_test',
      'bundle' => 'entity_test',
    ];
    $this->parentReferenceFieldConfig = FieldConfig::create($field);
    $this->parentReferenceFieldConfig->save();
  }

}
