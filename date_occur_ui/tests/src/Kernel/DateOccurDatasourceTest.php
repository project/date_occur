<?php

namespace Drupal\Tests\date_occur_ui\Kernel;

use Drupal\date_occur_ui\Plugin\search_api\datasource\DateOccurInstances;
use Drupal\Tests\date_occur_sapi\Kernel\DateOccurDatasourceTest as OriginalDatasourceTest;

/**
 * Check the datasource is overridden and its tests still pass.
 *
 * This could be extended to include the InstanceDatasourceTest if
 * base field bundle overrides for the third party settings (see
 * date_occur_instance_field_storage_config_insert for example).
 *
 * @group date_recur_recurrence
 * @covers \Drupal\date_occur_ui\Plugin\search_api\datasource\DateRecurOccourences
 */
class DateOccurDatasourceTest extends OriginalDatasourceTest {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'search_api',
    'date_recur',
    'date_recur_entity_test',
    'search_api_db',
    'field',
    'user',
    'date_occur',
    'date_occur_sapi',
    'date_occur_ui',
    'replicate',
  ];

  /**
   * Check plugin extended.
   */
  public function testExtended() {
    $this->assertInstanceOf(DateOccurInstances::class, $this->datasource);
  }

}
