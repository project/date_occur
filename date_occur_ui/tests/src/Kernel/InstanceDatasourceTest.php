<?php

namespace Drupal\Tests\date_occur_ui\Kernel;

use Drupal\entity_test\Entity\EntityTest;

/**
 * Test datasource with occurrence instances.
 *
 * @group date_recur_recurrence
 * @covers \Drupal\date_occur_sapi\Plugin\search_api\datasource\DateRecurOccourences
 */
class InstanceDatasourceTest extends DateOccurInstanceTestBase {

  /**
   * The test datasource.
   *
   * @var \Drupal\search_api\Datasource\DatasourceInterface
   */
  protected $datasource;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->datasource = $this->createTestIndex()->getDatasource('date_occur:entity_test__date_recur_field');
  }

  /**
   * Check recurrence occurrence reference indexed.
   */
  public function testValues() {
    $recur_instance = EntityTest::create();
    $recur_instance->date_recur_field = [
      [
        'value' => '2008-06-16T00:00:00',
        'end_value' => '2008-06-16T06:00:00',
        'rrule' => 'FREQ=DAILY;COUNT=5',
        'timezone' => 'Australia/Sydney',
      ],
    ];
    $recur_instance->save();

    $recurrence_instance = $recur_instance->createDuplicate();
    $recurrence_instance->date_recur_field = [
      [
        'value' => '2008-06-17T01:00:00',
        'end_value' => '2008-06-17T07:00:00',
        'timezone' => 'Australia/Sydney',
      ],
    ];
    $recurrence_instance->occur_parent_reference = [
      [
        'target_id' => $recur_instance->id(),
        'field_delta' => 0,
        'value' => '2008-06-17T00:00:00',
        'end_value' => '2008-06-17T06:00:00',
      ],
    ];
    $recurrence_instance->save();

    $item_definition = \Drupal::typedDataManager()->createDataDefinition('date_occurrence');
    $expected_items = [
      '1:0:2008-06-16T00:00:00Z--2008-06-16T06:00:00Z' => \Drupal::typedDataManager()
        ->create($item_definition, [
          'start_date' => '2008-06-16T00:00:00+00:00',
          'end_date' => '2008-06-16T06:00:00+00:00',
          'entity' => EntityTest::load($recur_instance->id()),
          'field_delta' => 0,
        ]),
      '1:0:2008-06-17T00:00:00Z--2008-06-17T06:00:00Z' => \Drupal::typedDataManager()
        ->create($item_definition, [
          'start_date' => '2008-06-17T01:00:00+00:00',
          'end_date' => '2008-06-17T07:00:00+00:00',
          'entity' => EntityTest::load($recurrence_instance->id()),
          'parent_entity' => EntityTest::load($recur_instance->id()),
          'field_delta' => 0,
        ]),
    ];

    $items = $this->datasource->loadMultiple(array_keys($expected_items));
    $this->assertEquals($expected_items, $items);

    // Ensure the occurrence hasn't been indexed seperately.
#    $items = $this->datasource->loadMultiple(['2:0:2008-06-17T01:00:00Z--2008-06-17T07:00:00Z']);
#    $this->assertEmpty($items);
  }

}
