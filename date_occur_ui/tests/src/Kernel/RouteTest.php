<?php

namespace Drupal\Tests\date_occur_ui\Kernel;

use Prophecy\PhpUnit\ProphecyTrait;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\date_recur\Plugin\Field\FieldType\DateRecurItem;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\KernelTests\KernelTestBase;

/**
 * Test dynamic route generation for fields.
 *
 * @group date_occur
 */
class RouteTest extends KernelTestBase {

  use ProphecyTrait;
  /**
   * Entity Field Manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface|null
   */
  protected ?EntityFieldManagerInterface $entityFieldManager;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'date_occur_ui',
    'date_occur_sapi',
    'date_occur',
    'entity_test',
    'datetime',
    'datetime_range',
    'date_recur',
    'field',
    'replicate',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('entity_test');
    $this->installConfig('date_recur');
    $this->installConfig('date_occur');
    $this->entityFieldManager = \Drupal::service('entity_field.manager');

    // Add a date recur field to test entity.
    $fieldStorage = FieldStorageConfig::create([
      'entity_type' => 'entity_test',
      'field_name' => 'date_recur_field',
      'type' => 'date_recur',
      'settings' => [
        'datetime_type' => DateRecurItem::DATETIME_TYPE_DATETIME,
      ],
    ]);
    $fieldStorage->save();

    $field = [
      'field_name' => 'date_recur_field',
      'entity_type' => 'entity_test',
      'bundle' => 'entity_test',
    ];
    $this->fieldConfig = FieldConfig::create($field);
    $this->fieldConfig->save();
  }

  public function testRouteCreation() {
    $prophecy = $this->prophesize(RouteProviderInterface::class);
#    $prophecy->set('key', 'value')->willReturn('my first value');
    $service = $prophecy->reveal();

    // Add a recurrence parent reference field to the test entity.
    $field_storage = FieldStorageConfig::create([
      'entity_type' => 'entity_test',
      'field_name' => 'occur_parent_reference',
      'type' => 'date_occur_parent',
      'settings' => [
        'target_type' => 'entity_test',
        'target_field' => 'date_recur_field',
      ],
    ]);
    $field_storage->save();

    $field = [
      'field_name' => 'occur_parent_reference',
      'entity_type' => 'entity_test',
      'bundle' => 'entity_test',
    ];
    $field_config = FieldConfig::create($field);
    $field_config->save();

#    $routes = \Drupal::service('router.route_provider')->getRoutesByPattern('/date_occur_instance/%');
#    assert($routes instanceof RouteCollection);
#    $this->assertCount(1, $routes);
#    $route = $routes->get('date_occur_instance_ui.create_entity_test');
#    $this->assertEquals('/date_occur_instance/{field_storage_config}/{entity_test}/{date_occur_instance}/create', $route->getPath());
  }

}
