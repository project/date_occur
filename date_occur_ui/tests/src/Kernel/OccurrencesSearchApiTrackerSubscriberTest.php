<?php

namespace Drupal\Tests\date_occur_ui\Kernel;

use Drupal\Tests\date_occur_sapi\Kernel\OccurrencesSearchApiTrackerSubscriberTest as OriginalOccurrencesSearchApiTrackerSubscriberTest;

/**
 * Ensure Date Occur SAPI tracker tests not broken.
 *
 * @group date_recur_recurrence
 */
class OccurrencesSearchApiTrackerSubscriberTest extends OriginalOccurrencesSearchApiTrackerSubscriberTest {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'search_api',
    'date_recur',
    'date_recur_entity_test',
    'search_api_db',
    'field',
    'user',
    'date_occur',
    'date_occur_sapi',
    'date_occur_ui',
    'replicate',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // While this won't work generally as the third party settings aren't
    // applied to the parent date_recur field as a base field.
    // Existing tests should also not break.
    // Base field should work for a seperate entity overriding, or same entity
    // different date field configuration. @todo test and check.
    //
    // Tests for the tracker with reused date field are in
    // InstanceTrackerSubscriberTest.
    //
    // See also DateOccurDatasourceTest and InstanceDatasourceTest.
  }

}
