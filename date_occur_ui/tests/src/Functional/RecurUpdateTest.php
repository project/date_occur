<?php

namespace Drupal\Tests\date_occur_ui\Functional;

use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\date_recur\Plugin\Field\FieldType\DateRecurItem;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\BrowserTestBase;
use RRule\RfcParser;

/**
 * Tests form create and edit.
 *
 * @group date_occur
 */
class RecurUpdateTest extends BrowserTestBase {

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = [
    'entity_test',
    'field_ui',
    'path',
    'date_occur_ui',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The name of the content type created for testing purposes.
   *
   * @var string
   */
  protected $type;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $fieldStorage = FieldStorageConfig::create([
      'entity_type' => 'entity_test',
      'field_name' => 'date_recur_field',
      'type' => 'date_recur',
      'settings' => [
        'datetime_type' => DateRecurItem::DATETIME_TYPE_DATETIME,
      ],
    ]);
    $fieldStorage->save();

    $field = [
      'field_name' => 'date_recur_field',
      'entity_type' => 'entity_test',
      'bundle' => 'entity_test',
    ];
    $this->fieldConfig = FieldConfig::create($field);
    $this->fieldConfig->save();

    $fieldStorage = FieldStorageConfig::create([
      'entity_type' => 'entity_test',
      'field_name' => 'date_parent_field',
      'type' => 'date_occur_parent',
      'settings' => [
        'target_type' => 'entity_test',
        'target_field' => 'date_recur_field',
      ],
    ]);
    $fieldStorage->save();

    $field = [
      'field_name' => 'date_parent_field',
      'entity_type' => 'entity_test',
      'bundle' => 'entity_test',
    ];
    $fieldConfig = FieldConfig::create($field);
    $fieldConfig->save();

    $entity_display = EntityFormDisplay::load('entity_test.entity_test.default');
    $entity_display->setComponent('date_recur_field', [
      'type' => 'date_recur_basic_widget',
      'settings' => [],
    ]);
    $entity_display->save();

    // Create user with access.
    $this->edit_user = $this->drupalCreateUser([
      'edit own entity_test content',
    ]);
    $this->drupalLogin($this->edit_user);
  }

  public function testUpdateFuture() {
    $entity = EntityTest::create([
      'name' => $this->randomString(),
      'user_id' => $this->edit_user->id(),
    ]);
    $rrule = 'FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR';
    $timezone = 'Indian/Christmas';
    $entity->date_recur_field = [
      [
        // 10am-4pm weekdaily. Starts Mon.
        'value' => '2008-06-15T22:00:00',
        'end_value' => '2008-06-16T06:00:00',
        'rrule' => $rrule,
        // UTC+7.
        'timezone' => $timezone,
      ],
    ];
    $entity->save();

    $this->drupalGet('/entity_test/' . $entity->id() . '/date_occur/date_parent_field/2008-06-17T22:00:00Z--2008-06-18T06:00:00Z/edit-future');
    $this->assertSession()->statusCodeEquals(200);

    $this->assertSession()->fieldValueEquals('date_recur_field[0][value][date]', '2008-06-18');
    $this->assertSession()->fieldValueEquals('date_recur_field[0][value][time]', '05:00:00');
    $this->assertSession()->fieldValueEquals('date_recur_field[0][end_value][date]', '2008-06-18');
    $this->assertSession()->fieldValueEquals('date_recur_field[0][end_value][time]', '13:00:00');
    $this->assertSession()->fieldValueEquals('date_recur_field[0][timezone]', $timezone);

    $page = $this->getSession()->getPage();
    $page->fillField('date_recur_field[0][value][date]', '2008-06-18');
    $page->fillField('date_recur_field[0][value][time]', '06:00:00');
    $page->fillField('date_recur_field[0][end_value][date]', '2008-06-18');
    $page->fillField('date_recur_field[0][end_value][time]', '14:00:00');
    $page->pressButton('Save');
    $this->assertSession()->addressEquals('/entity_test/manage/2/edit');

    $original = EntityTest::load(1);
    $this->assertEquals('2008-06-15T22:00:00', $original->date_recur_field->value);
    $this->assertEquals('2008-06-16T06:00:00', $original->date_recur_field->end_value);
    $original_rrule = RfcParser::parseRRule($original->date_recur_field->rrule);
    $expected_rrule = RfcParser::parseRRule($rrule . ';UNTIL=20080617T215959Z');
    $this->assertEquals($expected_rrule, $original_rrule);
    $this->assertEquals($timezone, $original->date_recur_field->timezone);
    $this->assertTrue($original->date_parent_field->isEmpty());

    $future = EntityTest::load(2);
    $this->assertEquals('2008-06-17T23:00:00', $future->date_recur_field->value);
    $this->assertEquals('2008-06-18T07:00:00', $future->date_recur_field->end_value);
    $this->assertEquals($rrule, $future->date_recur_field->rrule);
    $this->assertEquals($timezone, $future->date_recur_field->timezone);
    $this->assertTrue($original->date_parent_field->isEmpty());
  }

  public function testUpdateOverrideFuture() {
    $entity = EntityTest::create([
      'name' => $this->randomString(),
      'user_id' => $this->edit_user->id(),
    ]);
    $rrule = 'FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR';
    $timezone = 'Indian/Christmas';
    $entity->date_recur_field = [
      [
        // 10am-4pm weekdaily. Starts Mon.
        'value' => '2008-06-15T22:00:00',
        'end_value' => '2008-06-16T06:00:00',
        'rrule' => $rrule,
        // UTC+7.
        'timezone' => $timezone,
      ],
    ];
    $entity->save();

    $override = EntityTest::create([
      'name' => $this->randomString(),
      'name_id' => $this->edit_user->id(),
    ]);
    $override->date_recur_field = [
      [
        // Fri.
        'value' => '2008-06-19T22:00:00',
        'end_value' => '2008-06-20T06:00:00',
        'timezone' => $timezone,
      ],
    ];
    $override->date_parent_field = [
      'target_id' => $entity->id(),
      'field_delta' => '',
      'value' => '2008-06-19T22:00:00',
      'end_value' => '2008-06-20T06:00:00',
    ];
    $override->save();

    $this->drupalGet('/entity_test/' . $entity->id() . '/date_occur/date_parent_field/2008-06-17T22:00:00Z--2008-06-18T06:00:00Z/edit-future');
    $this->assertSession()->statusCodeEquals(200);

    $this->assertSession()->fieldValueEquals('date_recur_field[0][value][date]', '2008-06-18');
    $this->assertSession()->fieldValueEquals('date_recur_field[0][value][time]', '05:00:00');
    $this->assertSession()->fieldValueEquals('date_recur_field[0][end_value][date]', '2008-06-18');
    $this->assertSession()->fieldValueEquals('date_recur_field[0][end_value][time]', '13:00:00');
    $this->assertSession()->fieldValueEquals('date_recur_field[0][timezone]', $timezone);

    $page = $this->getSession()->getPage();
    $page->pressButton('Save');
    $this->assertSession()->addressEquals('/entity_test/' . $entity->id() . '/date_occur/date_parent_field/' . urlencode('2008-06-17T22:00:00Z--2008-06-18T06:00:00Z') . '/edit-future');

    $this->assertSession()->pageTextNotContains('Orphaned overrides');
    $this->assertSession()->pageTextContains('Valid overrides');
    $page->pressButton('Confirm and save');

    // Original updated to stop before the new $future.
    // @todo should we be doing something to clear this cache on update?
    \Drupal::entityTypeManager()->getStorage('entity_test')->resetCache([1]);
    $original = EntityTest::load(1);
    $this->assertEquals('2008-06-15T22:00:00', $original->date_recur_field->value);
    $this->assertEquals('2008-06-16T06:00:00', $original->date_recur_field->end_value);
    $original_rrule = RfcParser::parseRRule($original->date_recur_field->rrule);
    $expected_rrule = RfcParser::parseRRule($rrule . ';UNTIL=20080617T215959Z');
    $this->assertEquals($expected_rrule, $original_rrule);
    $this->assertEquals($timezone, $original->date_recur_field->timezone);
    $this->assertTrue($original->date_parent_field->isEmpty());

    // Future continuing on.
    $future = EntityTest::load(3);
    $this->assertEquals('2008-06-17T22:00:00', $future->date_recur_field->value);
    $this->assertEquals('2008-06-18T06:00:00', $future->date_recur_field->end_value);
    $this->assertEquals($rrule, $future->date_recur_field->rrule);
    $this->assertEquals($timezone, $future->date_recur_field->timezone);
    $this->assertTrue($original->date_parent_field->isEmpty());

    // Override now for $future.
    $override = EntityTest::load(2);
    $this->assertEquals($future->id(), $override->date_parent_field->target_id);
    $this->assertEquals('2008-06-19T22:00:00', $override->date_parent_field->value);
  }

  public function testUpdateOrphanFuture() {
    $entity = EntityTest::create([
      'name' => $this->randomString(),
      'user_id' => $this->edit_user->id(),
    ]);
    $rrule = 'FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR';
    $timezone = 'Indian/Christmas';
    $entity->date_recur_field = [
      [
        // 10am-4pm weekdaily. Starts Mon.
        'value' => '2008-06-15T22:00:00',
        'end_value' => '2008-06-16T06:00:00',
        'rrule' => $rrule,
        // UTC+7.
        'timezone' => $timezone,
      ],
    ];
    $entity->save();

    $override = EntityTest::create([
      'name' => $this->randomString(),
      'name_id' => $this->edit_user->id(),
    ]);
    $override->date_recur_field = [
      [
        // Fri.
        'value' => '2008-06-19T22:00:00',
        'end_value' => '2008-06-20T06:00:00',
        'timezone' => $timezone,
      ],
    ];
    $override->date_parent_field = [
      'target_id' => $entity->id(),
      'field_delta' => '',
      'value' => '2008-06-19T22:00:00',
      'end_value' => '2008-06-20T06:00:00',
    ];
    $override->save();

    $this->drupalGet('/entity_test/' . $entity->id() . '/date_occur/date_parent_field/2008-06-17T22:00:00Z--2008-06-18T06:00:00Z/edit-future');
    $this->assertSession()->statusCodeEquals(200);

    $this->assertSession()->fieldValueEquals('date_recur_field[0][value][date]', '2008-06-18');
    $this->assertSession()->fieldValueEquals('date_recur_field[0][value][time]', '05:00:00');
    $this->assertSession()->fieldValueEquals('date_recur_field[0][end_value][date]', '2008-06-18');
    $this->assertSession()->fieldValueEquals('date_recur_field[0][end_value][time]', '13:00:00');
    $this->assertSession()->fieldValueEquals('date_recur_field[0][timezone]', $timezone);

    $page = $this->getSession()->getPage();
    $page->fillField('date_recur_field[0][value][date]', '2008-06-18');
    $page->fillField('date_recur_field[0][value][time]', '06:00:00');
    $page->fillField('date_recur_field[0][end_value][date]', '2008-06-18');
    $page->fillField('date_recur_field[0][end_value][time]', '14:00:00');
    $page->pressButton('Save');
    $this->assertSession()->addressEquals('/entity_test/' . $entity->id() . '/date_occur/date_parent_field/' . urlencode('2008-06-17T22:00:00Z--2008-06-18T06:00:00Z') . '/edit-future');

    $this->assertSession()->pageTextContains('Orphaned overrides');
    $this->assertSession()->pageTextNotContains('Valid overrides');
    $page->pressButton('Confirm and save');

    // Original updated to stop before the new $future.
    // @todo should we be doing something to clear this cache on update?
    \Drupal::entityTypeManager()->getStorage('entity_test')->resetCache([1]);
    $original = EntityTest::load(1);
    $this->assertEquals('2008-06-15T22:00:00', $original->date_recur_field->value);
    $this->assertEquals('2008-06-16T06:00:00', $original->date_recur_field->end_value);
    $original_rrule = RfcParser::parseRRule($original->date_recur_field->rrule);
    $expected_rrule = RfcParser::parseRRule($rrule . ';UNTIL=20080617T215959Z');
    $this->assertEquals($expected_rrule, $original_rrule);
    $this->assertEquals($timezone, $original->date_recur_field->timezone);
    $this->assertTrue($original->date_parent_field->isEmpty());

    // Future continuing on.
    $future = EntityTest::load(3);
    $this->assertEquals('2008-06-17T23:00:00', $future->date_recur_field->value);
    $this->assertEquals('2008-06-18T07:00:00', $future->date_recur_field->end_value);
    $this->assertEquals($rrule, $future->date_recur_field->rrule);
    $this->assertEquals($timezone, $future->date_recur_field->timezone);
    $this->assertTrue($original->date_parent_field->isEmpty());
  }

}
