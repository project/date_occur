<?php

namespace Drupal\Tests\date_occur_ui\Functional;

use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\date_recur\Plugin\Field\FieldType\DateRecurItem;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\BrowserTestBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Tests form route generation and access.
 *
 * @group date_occur
 */
class InstanceCreateRouteTest extends BrowserTestBase {

#  use FieldUiTestTrait;

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = [
    'entity_test',
    'field_ui',
    'path',
    'date_occur_ui',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The name of the content type created for testing purposes.
   *
   * @var string
   */
  protected $type;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $fieldStorage = FieldStorageConfig::create([
      'entity_type' => 'entity_test',
      'field_name' => 'date_recur_field',
      'type' => 'date_recur',
      'settings' => [
        'datetime_type' => DateRecurItem::DATETIME_TYPE_DATETIME,
      ],
    ]);
    $fieldStorage->save();

    $field = [
      'field_name' => 'date_recur_field',
      'entity_type' => 'entity_test',
      'bundle' => 'entity_test',
    ];
    $this->fieldConfig = FieldConfig::create($field);
    $this->fieldConfig->save();

  }

  /**
   * Check access to create form and form mode.
   */
  public function testRouteAndFormMode() {
    $routes = \Drupal::service('router.route_provider')->getRoutesByPattern('/entity_test/%/date_occur/%/create');
    assert($routes instanceof RouteCollection);
    $this->assertCount(0, $routes);

    $fieldStorage = FieldStorageConfig::create([
      'entity_type' => 'entity_test',
      'field_name' => 'date_parent_field',
      'type' => 'date_occur_parent',
      'settings' => [
        'target_type' => 'entity_test',
        'target_field' => 'date_recur_field',
      ],
    ]);
    $fieldStorage->save();

    $field = [
      'field_name' => 'date_parent_field',
      'entity_type' => 'entity_test',
      'bundle' => 'entity_test',
    ];
    $fieldConfig = FieldConfig::create($field);
    $fieldConfig->save();

    $routes = \Drupal::service('router.route_provider')->getRoutesByPattern('/entity_test/%/date_occur/date_parent_field/%/create');
    assert($routes instanceof RouteCollection);
    $this->assertCount(1, $routes);
    $route = \Drupal::service('router.route_provider')->getRouteByName('date_occur_ui.entity_test.date_parent_field.create');
    $this->assertEquals('/entity_test/{entity_test}/date_occur/date_parent_field/{date_occur}/create', $route->getPath());

    // Create user with access.
    $edit_user = $this->drupalCreateUser([
      'edit own entity_test content',
    ]);
    $this->drupalLogin($edit_user);

    $entity = EntityTest::create([
      'name' => $this->randomString(),
      'user_id' => $edit_user->id(),
    ]);
    $rrule = 'FREQ=DAILY;INTERVAL=2';
    $timezone = 'Indian/Christmas';
    $entity->date_recur_field = [
      [
        // 10am-4pm weekdaily.
        'value' => '2022-06-01T22:00:00',
        'end_value' => '2022-06-02T06:00:00',
        'rrule' => $rrule,
        // UTC+7.
        'timezone' => $timezone,
      ],
    ];
    $entity->save();

    $this->drupalGet('/entity_test/' . $entity->id() . '/date_occur/date_parent_field/2022-06-05T22:00:00+00:00--2022-06-06T06:00:00+00:00/create');
    $this->assertSession()->statusCodeEquals(200);

    // Create user without access.
    $other_user = $this->drupalCreateUser();
    $this->drupalLogin($other_user);

    $this->drupalGet('/entity_test/' . $entity->id() . '/date_occur/date_parent_field/2022-06-05T22:00:00+00:00--2022-06-06T06:00:00+00:00/create');
    $this->assertSession()->statusCodeEquals(403);

    // Edit user, but date not in sequence.
    $this->drupalLogin($edit_user);
    $this->drupalGet('/entity_test/' . $entity->id() . '/date_occur/date_parent_field/2022-06-06T22:00:00+00:00--2022-06-07T06:00:00+00:00/create');
    $this->assertSession()->statusCodeEquals(404);

    // Edit user, but date not in correct end.
    $this->drupalGet('/entity_test/' . $entity->id() . '/date_occur/date_parent_field/2022-06-05T22:00:00+00:00--2022-06-06T05:00:00+00:00/create');
    $this->assertSession()->statusCodeEquals(404);

    // Edit user, but invalid date.
    $this->drupalGet('/entity_test/' . $entity->id() . '/date_occur/date_parent_field/2022-06-05--22:00:00T00:00--2022-06-06T05:00:00+00:00/create');
    $this->assertSession()->statusCodeEquals(404);

    // Check form display mode switch.
    $entity_display = EntityFormDisplay::load('entity_test.entity_test.default')->createCopy('edit_instance');
    $entity_display->setComponent('date_recur_field');
    $entity_display->save();

    $this->drupalGet('/entity_test/' . $entity->id() . '/date_occur/date_parent_field/2022-06-05T22:00:00Z--2022-06-06T06:00:00Z/create');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementExists('css', 'input[name="date_recur_field[0][value][date]"]');

    $entity_display->removeComponent('date_recur_field');
    $entity_display->save();

    $this->drupalGet('/entity_test/' . $entity->id() . '/date_occur/date_parent_field/2022-06-05T22:00:00Z--2022-06-06T06:00:00Z/create');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementNotExists('css', 'input[name="date_recur_field[0][value][date]"]');
  }

}
