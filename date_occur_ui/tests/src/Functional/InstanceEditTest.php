<?php

namespace Drupal\Tests\date_occur_ui\Functional;

use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\date_recur\Plugin\Field\FieldType\DateRecurItem;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests form create and edit.
 *
 * @group date_occur
 */
class InstanceEditTest extends BrowserTestBase {

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = [
    'entity_test',
    'field_ui',
    'path',
    'date_occur_ui',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The name of the content type created for testing purposes.
   *
   * @var string
   */
  protected $type;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $fieldStorage = FieldStorageConfig::create([
      'entity_type' => 'entity_test',
      'field_name' => 'date_recur_field',
      'type' => 'date_recur',
      'settings' => [
        'datetime_type' => DateRecurItem::DATETIME_TYPE_DATETIME,
      ],
    ]);
    $fieldStorage->save();

    $field = [
      'field_name' => 'date_recur_field',
      'entity_type' => 'entity_test',
      'bundle' => 'entity_test',
    ];
    $this->fieldConfig = FieldConfig::create($field);
    $this->fieldConfig->save();

    $fieldStorage = FieldStorageConfig::create([
      'entity_type' => 'entity_test',
      'field_name' => 'date_parent_field',
      'type' => 'date_occur_parent',
      'settings' => [
        'target_type' => 'entity_test',
        'target_field' => 'date_recur_field',
      ],
    ]);
    $fieldStorage->save();

    $field = [
      'field_name' => 'date_parent_field',
      'entity_type' => 'entity_test',
      'bundle' => 'entity_test',
    ];
    $fieldConfig = FieldConfig::create($field);
    $fieldConfig->save();

    $entity_display = EntityFormDisplay::load('entity_test.entity_test.default')->createCopy('edit_instance');
    $entity_display->setComponent('date_recur_field', [
      'type' => 'date_recur_occur_instance',
      'settings' => [],
    ]);
    $entity_display->save();

    // Create user with access.
    $this->edit_user = $this->drupalCreateUser([
      'edit own entity_test content',
    ]);
    $this->drupalLogin($this->edit_user);
  }

  public function testCreateEdit() {

    $entity = EntityTest::create([
      'name' => $this->randomString(),
      'user_id' => $this->edit_user->id(),
    ]);
    $rrule = 'FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR';
    $timezone = 'Indian/Christmas';
    $entity->date_recur_field = [
      [
        // 10am-4pm weekdaily.
        'value' => '2008-06-15T22:00:00',
        'end_value' => '2008-06-16T06:00:00',
        'rrule' => $rrule,
        // UTC+7.
        'timezone' => $timezone,
      ],
    ];
    $entity->save();

    $this->drupalGet('/entity_test/' . $entity->id() . '/date_occur/date_parent_field/2008-06-16T22:00:00Z--2008-06-17T06:00:00Z/create');
    $this->assertSession()->statusCodeEquals(200);

    $this->assertSession()->elementNotExists('css', 'input[name="date_recur_field[0][rrule]"]');

    $this->assertSession()->fieldValueEquals('date_recur_field[0][value][date]', '2008-06-17');
    $this->assertSession()->fieldValueEquals('date_recur_field[0][value][time]', '05:00:00');
    $this->assertSession()->fieldValueEquals('date_recur_field[0][end_value][date]', '2008-06-17');
    $this->assertSession()->fieldValueEquals('date_recur_field[0][end_value][time]', '13:00:00');
    $this->assertSession()->fieldValueEquals('date_recur_field[0][timezone]', $timezone);

    $page = $this->getSession()->getPage();
    $page->fillField('date_recur_field[0][value][date]', '2008-06-18');
    $page->fillField('date_recur_field[0][value][time]', '06:00:00');
    $page->fillField('date_recur_field[0][end_value][date]', '2008-06-18');
    $page->fillField('date_recur_field[0][end_value][time]', '14:00:00');
    $page->pressButton('Save');

    $instance = EntityTest::load(2);
    $this->assertEquals('2008-06-17T23:00:00', $instance->date_recur_field->value);
    $this->assertEquals('2008-06-18T07:00:00', $instance->date_recur_field->end_value);
    $this->assertEquals('', $instance->date_recur_field->rrule);
    $this->assertEquals($timezone, $instance->date_recur_field->timezone);
    $this->assertEquals(1, $instance->date_parent_field->target_id);
    $this->assertEquals(0, $instance->date_parent_field->field_delta);
    $this->assertEquals('2008-06-16T22:00:00', $instance->date_parent_field->value);
    $this->assertEquals('2008-06-17T06:00:00', $instance->date_parent_field->end_value);
  }

}
