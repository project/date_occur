<?php

namespace Drupal\Tests\date_occur_ui\Functional;

use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Entity\Entity\EntityFormMode;
use Drupal\date_recur\Plugin\Field\FieldType\DateRecurItem;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\field_ui\Traits\FieldUiTestTrait;

/**
 * Tests for the administrative UI.
 *
 * @group date_occur
 */
class InstanceFormTest extends BrowserTestBase {

  use FieldUiTestTrait;

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = [
    'entity_test',
    'field_ui',
    'path',
    'date_occur_ui',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The name of the content type created for testing purposes.
   *
   * @var string
   */
  protected $type;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $fieldStorage = FieldStorageConfig::create([
      'entity_type' => 'entity_test',
      'field_name' => 'date_recur_field',
      'type' => 'date_recur',
      'settings' => [
        'datetime_type' => DateRecurItem::DATETIME_TYPE_DATETIME,
      ],
    ]);
    $fieldStorage->save();

    $field = [
      'field_name' => 'date_recur_field',
      'entity_type' => 'entity_test',
      'bundle' => 'entity_test',
    ];
    $this->fieldConfig = FieldConfig::create($field);
    $this->fieldConfig->save();

    $display = \Drupal::service('entity_display.repository')->getFormDisplay('entity_test', 'entity_test', 'default');
    $component = $display->getComponent('date_recur_field');
    $component['region'] = 'content';
    $component['type'] = 'date_recur_basic_widget';
    $component['settings'] = [];
    $display->setComponent('date_recur_field', $component);
    $display->save();

    $fieldStorage = FieldStorageConfig::create([
      'entity_type' => 'entity_test',
      'field_name' => 'date_parent_field',
      'type' => 'date_occur_parent',
      'settings' => [
        'target_type' => 'entity_test',
        'target_field' => 'date_recur_field',
      ],
    ]);
    $fieldStorage->save();

    $field = [
      'field_name' => 'date_parent_field',
      'entity_type' => 'entity_test',
      'bundle' => 'entity_test',
    ];
    $this->fieldConfig = FieldConfig::create($field);
    $this->fieldConfig->save();

    // Create test user.
    $admin_user = $this->drupalCreateUser([
      'administer entity_test content',
      'administer entity_test fields',
      'administer entity_test display',
    ]);
    $this->drupalLogin($admin_user);
  }

  /**
   * Switch form mode if available and field completed.
   *
   * @covers date_occur_entity_form_mode_alter().
   */
  public function testEntityFormModeAlter() {
    // The field should be available on default form mode.
    $entity1 = EntityTest::create([
      'name' => $this->randomString(),
    ]);
    $entity1->save();
    $this->drupalGet($entity1->toUrl('edit-form'));
    $this->assertSession()->elementExists('css', 'input[name="date_recur_field[0][value][date]"]');

    // Configure form mode.
    EntityFormDisplay::create([
      'targetEntityType' => 'entity_test',
      'bundle' => 'entity_test',
      'mode' => 'edit_instance',
      'status' => TRUE,
    ])->removeComponent('date_recur_field')->save();

    // The field should be available on default form mode.
    $entity2 = EntityTest::create([
      'name' => $this->randomString(),
    ]);
    $entity2->save();
    $this->drupalGet($entity2->toUrl('edit-form'));
    $this->assertSession()->elementExists('css', 'input[name="date_recur_field[0][value][date]"]');

    // The field should be hidden on compact form mode.
    // See: entity_test_entity_form_mode_alter().
    $entity3 = EntityTest::create([
      'name' => $this->randomString(),
      'date_parent_field' => [
        'target_id' => $entity2->id(),
        'field_delta' => 0,
        'recurrence_id' => '',
        'value' => '',
        'end_value' => '',
      ],
    ]);
    $entity3->save();
    $this->drupalGet($entity3->toUrl('edit-form'));
    $this->assertSession()->elementNotExists('css', 'input[name="date_recur_field[0][value][date]"]');
  }

}
