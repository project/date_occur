<?php

namespace Drupal\Tests\date_occur_ui\Functional;

use Drupal\date_recur\Plugin\Field\FieldType\DateRecurItem;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests redirection if an occurrence has an instance.
 *
 * @group date_occur
 */
class InstanceRedirectRouteTest extends BrowserTestBase {

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = [
    'entity_test',
    'field_ui',
    'path',
    'date_occur_ui',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The name of the content type created for testing purposes.
   *
   * @var string
   */
  protected $type;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $fieldStorage = FieldStorageConfig::create([
      'entity_type' => 'entity_test',
      'field_name' => 'date_recur_field',
      'type' => 'date_recur',
      'settings' => [
        'datetime_type' => DateRecurItem::DATETIME_TYPE_DATETIME,
      ],
    ]);
    $fieldStorage->save();

    $field = [
      'field_name' => 'date_recur_field',
      'entity_type' => 'entity_test',
      'bundle' => 'entity_test',
    ];
    $this->fieldConfig = FieldConfig::create($field);
    $this->fieldConfig->save();

    $fieldStorage = FieldStorageConfig::create([
      'entity_type' => 'entity_test',
      'field_name' => 'date_parent_field',
      'type' => 'date_occur_parent',
      'settings' => [
        'target_type' => 'entity_test',
        'target_field' => 'date_recur_field',
      ],
    ]);
    $fieldStorage->save();

    $field = [
      'field_name' => 'date_parent_field',
      'entity_type' => 'entity_test',
      'bundle' => 'entity_test',
    ];
    $fieldConfig = FieldConfig::create($field);
    $fieldConfig->save();

    // Create user with access.
    $edit_user = $this->drupalCreateUser([
      'view test entity',
    ]);
    $this->drupalLogin($edit_user);
  }

  /**
   * Access occurrences.
   */
  public function testRouteOccurrences() {
    $parent_entity = EntityTest::create([
      'name' => $this->randomString(),
    ]);
    $rrule = 'FREQ=DAILY;INTERVAL=2';
    $timezone = 'Indian/Christmas';
    $parent_entity->date_recur_field = [
      [
        'value' => '2022-06-01T22:00:00',
        'end_value' => '2022-06-02T06:00:00',
        'rrule' => $rrule,
        // UTC+7.
        'timezone' => $timezone,
      ],
    ];
    $parent_entity->save();

    $this->drupalGet($parent_entity->toUrl());
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals($parent_entity->toUrl());
    $this->assertSession()->pageTextContains($parent_entity->label());

    // Date that does not exist in the recurrence rule.
    $this->drupalGet($parent_entity->toUrl()->toString() . '/date_occur/date_parent_field/2022-06-02T22:00:00Z--2022-06-03T06:00:00Z');
    $this->assertSession()->statusCodeEquals(404);

    // Date that does exist in the recurrence rule.
    $this->drupalGet($parent_entity->toUrl()->toString() . '/date_occur/date_parent_field/2022-06-03T22:00:00Z--2022-06-04T06:00:00Z');
    $this->assertSession()->statusCodeEquals(200);
    $occurrence_url = $parent_entity->toUrl()->toString() . '/date_occur/date_parent_field/' . urlencode('2022-06-03T22:00:00Z--2022-06-04T06:00:00Z');
    $this->assertSession()->addressEquals($occurrence_url);
    $this->assertSession()->pageTextContains($parent_entity->label());

    // Override the occurrence with an instance.
    $instance_entity = EntityTest::create([
      'name' => $this->randomString(),
    ]);
    $timezone = 'Indian/Christmas';
    $instance_entity->date_recur_field = [
      [
        'value' => '2022-06-03T22:00:00',
        'end_value' => '2022-06-04T06:00:00',
        // UTC+7.
        'timezone' => $timezone,
      ],
    ];
    $instance_entity->date_parent_field = [
      [
        'target_id' => $parent_entity->id(),
        'recurrence_id' => '2022-06-03T22:00:00Z--2033-06-04T06:00:00Z',
        'field_delta' => 0,
        'value' => '2022-06-03T22:00:00',
        'end_value' => '2022-06-04T06:00:00',
      ],
    ];
    $instance_entity->save();

    // Check redirected to the instance now.
    $this->drupalGet($parent_entity->toUrl()->toString() . '/date_occur/date_parent_field/2022-06-03T22:00:00Z--2022-06-04T06:00:00Z');
    $this->assertSession()->addressEquals($instance_entity->toUrl());
    $this->assertSession()->pageTextContains($instance_entity->label());
    $this->assertSession()->statusCodeEquals(200);

    // Delete the instance, should be back to the occurrence.
    $instance_entity->delete();
    $this->drupalGet($parent_entity->toUrl()->toString() . '/date_occur/date_parent_field/2022-06-03T22:00:00Z--2022-06-04T06:00:00Z');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals($occurrence_url);
    $this->assertSession()->pageTextContains($parent_entity->label());
  }

}
