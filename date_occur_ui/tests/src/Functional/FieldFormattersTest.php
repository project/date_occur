<?php

namespace Drupal\Tests\date_occur_ui\Functional;

use Drupal\Tests\date_occur\Functional\FieldFormatterTest;

/**
 * Recur and Occur field formatters.
 *
 * @group date_occur
 */
class FieldFormattersTest extends FieldFormatterTest {

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = [
    'entity_test',
    'field_ui',
    'path',
    'date_occur_ui',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $recur_options = [
      'type' => 'date_occur_ui_recur_formatter',
      'label' => 'hidden',
      'settings' => [
        'timezone_override' => '',
        'format_type' => 'html_datetime',
        'occurrence_format_type' => 'html_datetime',
        'same_end_date_format_type' => 'html_datetime',
      ],
    ];
    $occur_options = [
      'type' => 'date_occur_occurrences_formatter',
      'label' => 'hidden',
      'settings' => [
        'show_parent_title' => 'always',
        'interpreter' => 'default_interpreter',
        'timezone_override' => '',
        'occurrence_format_type' => 'html_datetime',
        'same_end_date_format_type' => 'html_datetime',
      ],
    ];

    $this->viewDisplay->setComponent('date_recur_field', $recur_options);
    $this->viewDisplay->setComponent('date_parent_field', $occur_options);
    $this->viewDisplay->save();
  }

  /**
   * Date occur recur formatter timezones.
   */
  public function testTimezoneFormatter() {
    $this->drupalGet('/entity_test/' . $this->entity->id() . '/date_occur/date_parent_field/2008-06-17T22:00:00Z--2008-06-18T06:00:00Z');
    // Occurrence.
    $time = $this->getSession()->getPage()->find('xpath', "//div[@class='date-recur-date']/time[@datetime='2008-06-18T05:00:00+07:00']");
    $this->assertEquals('2008-06-18T08:00:00+1000', $time->getHtml());
    $time = $this->getSession()->getPage()->find('xpath', "//div[@class='date-recur-date']/time[@datetime='2008-06-18T13:00:00+07:00']");
    $this->assertEquals('2008-06-18T16:00:00+1000', $time->getHtml());
    // Next occurrence.
    $time = $this->getSession()->getPage()->find('xpath', "//time[@datetime='2008-06-19T05:00:00+07:00']");
    $this->assertEquals('2008-06-19T08:00:00+1000', $time->getHtml());
    $time = $this->getSession()->getPage()->find('xpath', "//time[@datetime='2008-06-19T13:00:00+07:00']");
    $this->assertEquals('2008-06-19T16:00:00+1000', $time->getHtml());

    // Switch to display in original timezone.
    $display_options = $this->viewDisplay->getComponent('date_recur_field');
    $display_options['settings']['timezone_override'] = 'date_timezone';
    $this->viewDisplay->setComponent('date_recur_field', $display_options)->save();
    $this->drupalGet('/entity_test/' . $this->entity->id() . '/date_occur/date_parent_field/2008-06-17T22:00:00Z--2008-06-18T06:00:00Z');
    // Occurrence.
    $time = $this->getSession()->getPage()->find('xpath', "//div[@class='date-recur-date']/time[@datetime='2008-06-18T05:00:00+07:00']");
    $this->assertEquals('2008-06-18T05:00:00+0700', $time->getHtml());
    $time = $this->getSession()->getPage()->find('xpath', "//div[@class='date-recur-date']/time[@datetime='2008-06-18T13:00:00+07:00']");
    $this->assertEquals('2008-06-18T13:00:00+0700', $time->getHtml());
    // Next occurrence.
    $time = $this->getSession()->getPage()->find('xpath', "//time[@datetime='2008-06-19T05:00:00+07:00']");
    $this->assertEquals('2008-06-19T05:00:00+0700', $time->getHtml());
    $time = $this->getSession()->getPage()->find('xpath', "//time[@datetime='2008-06-19T13:00:00+07:00']");
    $this->assertEquals('2008-06-19T13:00:00+0700', $time->getHtml());
  }

  /**
   * Date occur recur formatter occurrences.
   */
  public function testRecurOccurrenceOverride() {
    // Test with several links.
    $display_options = $this->viewDisplay->getComponent('date_recur_field');
    $display_options['settings']['show_next'] = 2;
    $this->viewDisplay->setComponent('date_recur_field', $display_options)->save();

    // Cache the list on a date.
    $this->drupalGet('/entity_test/' . $this->entity->id() . '/date_occur/date_parent_field/2008-06-15T22:00:00Z--2008-06-16T06:00:00Z');
    // Date.
    $time = $this->getSession()->getPage()->find('xpath', "//time[@datetime='2008-06-16T05:00:00+07:00']");
    $this->assertEquals('2008-06-16T08:00:00+1000', $time->getHtml());
    $time = $this->getSession()->getPage()->find('xpath', "//time[@datetime='2008-06-16T13:00:00+07:00']");
    $this->assertEquals('2008-06-16T16:00:00+1000', $time->getHtml());
    // Occurrences. Neither overriden.
    $time = $this->getSession()->getPage()->find('xpath', "//time[@datetime='2008-06-17T05:00:00+07:00']");
    $this->assertEquals('2008-06-17T08:00:00+1000', $time->getHtml());
    $time = $this->getSession()->getPage()->find('xpath', "//time[@datetime='2008-06-17T13:00:00+07:00']");
    $this->assertEquals('2008-06-17T16:00:00+1000', $time->getHtml());
    $this->assertSession()->linkByHrefExists('/entity_test/1/date_occur/date_parent_field/2008-06-16T22%3A00%3A00Z--2008-06-17T06%3A00%3A00Z');
    $time = $this->getSession()->getPage()->find('xpath', "//time[@datetime='2008-06-18T05:00:00+07:00']");
    $this->assertEquals('2008-06-18T08:00:00+1000', $time->getHtml());
    $time = $this->getSession()->getPage()->find('xpath', "//time[@datetime='2008-06-18T13:00:00+07:00']");
    $this->assertEquals('2008-06-18T16:00:00+1000', $time->getHtml());
    $this->assertSession()->linkByHrefExists('/entity_test/1/date_occur/date_parent_field/2008-06-17T22%3A00%3A00Z--2008-06-18T06%3A00%3A00Z');

    // Change date in the argument.
    $this->drupalGet('/entity_test/' . $this->entity->id() . '/date_occur/date_parent_field/2008-06-17T22:00:00Z--2008-06-18T06:00:00Z');
    // Date.
    $time = $this->getSession()->getPage()->find('xpath', "//time[@datetime='2008-06-18T05:00:00+07:00']");
    $this->assertEquals('2008-06-18T08:00:00+1000', $time->getHtml());
    $time = $this->getSession()->getPage()->find('xpath', "//time[@datetime='2008-06-18T13:00:00+07:00']");
    $this->assertEquals('2008-06-18T16:00:00+1000', $time->getHtml());
    // Neither overriden.
    $time = $this->getSession()->getPage()->find('xpath', "//time[@datetime='2008-06-19T05:00:00+07:00']");
    $this->assertEquals('2008-06-19T08:00:00+1000', $time->getHtml());
    $time = $this->getSession()->getPage()->find('xpath', "//time[@datetime='2008-06-19T13:00:00+07:00']");
    $this->assertEquals('2008-06-19T16:00:00+1000', $time->getHtml());
    $this->assertSession()->linkByHrefExists('/entity_test/1/date_occur/date_parent_field/2008-06-18T22%3A00%3A00Z--2008-06-19T06%3A00%3A00Z');
    $time = $this->getSession()->getPage()->find('xpath', "//time[@datetime='2008-06-20T05:00:00+07:00']");
    $this->assertEquals('2008-06-20T08:00:00+1000', $time->getHtml());
    $time = $this->getSession()->getPage()->find('xpath', "//time[@datetime='2008-06-20T13:00:00+07:00']");
    $this->assertEquals('2008-06-20T16:00:00+1000', $time->getHtml());
    $this->assertSession()->linkByHrefExists('/entity_test/1/date_occur/date_parent_field/2008-06-19T22%3A00%3A00Z--2008-06-20T06%3A00%3A00Z');

    $override = $this->createOverride();

    $this->drupalGet('/entity_test/' . $this->entity->id() . '/date_occur/date_parent_field/2008-06-17T22:00:00Z--2008-06-18T06:00:00Z');
    // Not overriden occurrence.
    $time = $this->getSession()->getPage()->find('xpath', "//time[@datetime='2008-06-19T05:00:00+07:00']");
    $this->assertEquals('2008-06-19T08:00:00+1000', $time->getHtml());
    $time = $this->getSession()->getPage()->find('xpath', "//time[@datetime='2008-06-19T13:00:00+07:00']");
    $this->assertEquals('2008-06-19T16:00:00+1000', $time->getHtml());
    $this->assertSession()->linkByHrefExists('/entity_test/1/date_occur/date_parent_field/2008-06-18T22%3A00%3A00Z--2008-06-19T06%3A00%3A00Z');
    // Overriden occurrence.
    $time = $this->getSession()->getPage()->find('xpath', "//time[@datetime='2008-06-20T05:00:00+07:00']");
    $this->assertEquals('2008-06-20T08:00:00+1000', $time->getHtml());
    $time = $this->getSession()->getPage()->find('xpath', "//time[@datetime='2008-06-20T13:00:00+07:00']");
    $this->assertEquals('2008-06-20T16:00:00+1000', $time->getHtml());
    // As mentioned in the code changing the URL is less important. But it does
    // show the the cache is updated when override is added.
    $this->assertSession()->linkByHrefNotExists('/entity_test/1/date_occur/date_parent_field/2008-06-19T22%3A00%3A00Z--2008-06-20T06%3A00%3A00Z');
    $this->assertSession()->linkByHrefExists('/entity_test/2');

    // Change time on override check cache.
    $override->date_recur_field = [
      'value' => '2008-06-19T23:00:00',
      'end_value' => '2008-06-20T07:00:00',
      'timezone' => $this->timezone,
    ];
    $override->save();
    $this->drupalGet('/entity_test/' . $this->entity->id() . '/date_occur/date_parent_field/2008-06-18T22:00:00Z--2008-06-19T06:00:00Z');
    // Not overriden occurrence.
    $time = $this->getSession()->getPage()->find('xpath', "//time[@datetime='2008-06-19T05:00:00+07:00']");
    $this->assertEquals('2008-06-19T08:00:00+1000', $time->getHtml());
    $time = $this->getSession()->getPage()->find('xpath', "//time[@datetime='2008-06-19T13:00:00+07:00']");
    $this->assertEquals('2008-06-19T16:00:00+1000', $time->getHtml());
    // Overriden occurrence.
    $time = $this->getSession()->getPage()->find('xpath', "//time[@datetime='2008-06-20T06:00:00+07:00']");
    $this->assertEquals('2008-06-20T09:00:00+1000', $time->getHtml());
    $time = $this->getSession()->getPage()->find('xpath', "//time[@datetime='2008-06-20T14:00:00+07:00']");
    $this->assertEquals('2008-06-20T17:00:00+1000', $time->getHtml());
  }

  public function testOccurOccurrences() {
    $override = $this->createOverride();
    $display_options = $this->viewDisplay->getComponent('date_parent_field');

    $this->drupalGet($override->toUrl()->toString());
    // Default single future occurrence.
    $time = $this->getSession()->getPage()->find('xpath', "//time[@datetime='2008-06-23T05:00:00+07:00']");
    $this->assertEquals('2008-06-23T08:00:00+1000', $time->getHtml());
    $time = $this->getSession()->getPage()->find('xpath', "//time[@datetime='2008-06-23T13:00:00+07:00']");
    $this->assertEquals('2008-06-23T16:00:00+1000', $time->getHtml());

    // Switch to display in original timezone.
    $display_options['settings']['timezone_override'] = 'date_timezone';
    $this->viewDisplay->setComponent('date_parent_field', $display_options)->save();
    $this->drupalGet($override->toUrl()->toString());
    $time = $this->getSession()->getPage()->find('xpath', "//time[@datetime='2008-06-23T05:00:00+07:00']");
    $this->assertEquals('2008-06-23T05:00:00+0700', $time->getHtml());
    $time = $this->getSession()->getPage()->find('xpath', "//time[@datetime='2008-06-23T13:00:00+07:00']");
    $this->assertEquals('2008-06-23T13:00:00+0700', $time->getHtml());

    // Increase number listed.
    $this->assertSession()->elementsCount('xpath', "//ul[@class='date-recur-occurrences']/li", 1);
    $display_options['settings']['show_next'] = 5;
    $this->viewDisplay->setComponent('date_parent_field', $display_options)->save();
    $this->drupalGet($override->toUrl()->toString());
    $this->assertSession()->elementsCount('xpath', "//ul[@class='date-recur-occurrences']/li", 5);
  }

  /**
   * Test routes with different parameters.
   *
   * @todo move this somewhere more appropriate.
   */
  public function testUpcasting() {
    $this->drupalGet('/entity_test/' . $this->entity->id() . '/date_occur/date_parent_field/2008-06-17T22:00:00Z--2008-06-18T06:00:00Z');
    $this->assertSession()->statusCodeEquals(200);
    // No existing.
    $this->drupalGet('/entity_test/' . $this->entity->id() . '/date_occur/date_parent_field/2008-06-17T21:00:00Z--2008-06-18T06:00:00Z');
    $this->assertSession()->statusCodeEquals(404);
    // Misformed.
    $this->drupalGet('/entity_test/' . $this->entity->id() . '/date_occur/date_parent_field/2008-06-17T22:00:00--2008-06-18T06:00:00');
    $this->assertSession()->statusCodeEquals(404);
  }

}
