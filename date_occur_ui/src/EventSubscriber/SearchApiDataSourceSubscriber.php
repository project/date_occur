<?php

namespace Drupal\date_occur_ui\EventSubscriber;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\date_occur_ui\Plugin\search_api\datasource\DateOccurInstances;
use Drupal\search_api\Event\GatheringPluginInfoEvent;
use Drupal\search_api\Event\SearchApiEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Date Recur Recurrence event subscriber.
 *
 * Replace DateOccur datasource with our extended DateOccurInstance one.
 */
class SearchApiDataSourceSubscriber implements EventSubscriberInterface {

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs event subscriber.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   */
  public function __construct(MessengerInterface $messenger) {
    $this->messenger = $messenger;
  }

  /**
   * Kernel response event handler.
   *
   * @param \Drupal\search_api\Event\GatheringPluginInfoEvent $event
   *   Search API gathering datasources event.
   */
  public function alterDataSources(GatheringPluginInfoEvent $event) {
    $definitions =& $event->getDefinitions();
    foreach ($definitions as $id => $definition) {
      if (strstr($id, ':', true) == 'date_occur') {
        $definitions[$id]['class'] = DateOccurInstances::class;
        $definitions[$id]['provider'] = 'date_occur_ui';
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      SearchApiEvents::GATHERING_DATA_SOURCES => ['alterDataSources'],
    ];
  }

}
