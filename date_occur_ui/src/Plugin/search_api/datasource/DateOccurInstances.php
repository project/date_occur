<?php

namespace Drupal\date_occur_ui\Plugin\search_api\datasource;

use Drupal\Core\TypedData\ComplexDataInterface;
use Drupal\Core\Url;
use Drupal\date_occur\OccurReferenceHelperInterface;
use Drupal\date_occur\Plugin\DataType\DateOccurrenceInterface;
use Drupal\date_occur_sapi\Plugin\search_api\datasource\DateOccur;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A datasource for occurrences of a recurring date including overriden.
 */
class DateOccurInstances extends DateOccur {

  /**
   * The occurrence reference helper.
   *
   * @var \Drupal\date_occur\OccurReferenceHelperInterface|null
   */
  protected ?OccurReferenceHelperInterface $occurrenceHelper;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var static $datasource */
    $datasource = parent::create($container, $configuration, $plugin_id, $plugin_definition);

    $datasource->setOccurrenceHelper($container->get('date_occur.occur_reference_helper'));
    return $datasource;
  }

  /**
   * Set the occurrence reference helper.
   *
   * @param \Drupal\date_occur\OccurReferenceHelperInterface $occurrence_helper
   *   The occurrence reference helper.
   *
   * @return $this
   */
  public function setOccurrenceHelper(OccurReferenceHelperInterface $occurrence_helper): self {
    $this->occurrenceHelper = $occurrence_helper;
    return $this;
  }

  /**
   * Get the occurrence reference helper.
   *
   * @return \Drupal\date_occur\OccurReferenceHelperInterface
   *   The occurrence reference helper.
   */
  public function getOccurrenceHelper(): OccurReferenceHelperInterface {
    return $this->occurrenceHelper ?: \Drupal::service('date_occur.occur_reference_helper');
  }

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions() {
    $property_definitions = parent::getPropertyDefinitions(); 
    $property_definitions['parent_entity']->setEntityTypeId($this->getEntityTypeId());
    return $property_definitions;
  }

  /**
   * {@inheritdoc}
   */
  public function loadMultiple(array $ids) {
    $loaded_items = parent::loadMultiple($ids);

    // Update the occurrence for any that have instances.
    if ($occur_field_id = $this->getInstanceReferenceField()) {
      list(, $field_name) = explode('.', $occur_field_id);
      foreach ($loaded_items as $id => $item) {
        // @todo why, or does, this get called outside of a test with a value
        // that's not yet indexed (and doesn't want to be).
        if (!$item->getValue()['entity']->get($field_name)->isEmpty()) {
          unset($loaded_items[$id]);
        }
        else {
          $loaded_items[$id] = $this->getOccurrenceHelper()->setOccurrenceInstance($item, $this->getFieldName());
        }
      }
    }

    return $loaded_items;
  }

  /**
   * {@inheritdoc}
   */
  public function getItemUrl(ComplexDataInterface $item) {
    assert($item instanceof DateOccurrenceInterface);
    if (!$item->getParent() && ($entity = $item->getEntity())) {
      return Url::fromRoute('date_occur_ui.' . $this->getInstanceReferenceField() . '.view', [
        $entity->getEntityTypeId() => $entity->id(),
        'date_occur' => $item->getRangeId(),
      ]);
    }

    return parent::getItemUrl($item);

  }

  /**
   * {@inheritdoc}
   */
  protected function getEntitiesQuery() {
    $query = parent::getEntitiesQuery();

    if ($occur_field_id = $this->getInstanceReferenceField()) {
      list($entity_type, $field_name) = $occur_field_id;
      if (
        ($entity_type == $this->getEntityType()->id())
      ) {
        // Don't index instance entities of a recurring date.
        // These are indexed via the parent occurrences.
        $query->notExists($field_name);
      }
    }

    return $query;
  }

  private function getInstanceReferenceField() {
    $recur_field_name = $this->getFieldName();
    $entity_type = $this->getEntityType();
    $recur_field_storage = $this->getEntityFieldManager()->getFieldStorageDefinitions($entity_type->id())[$recur_field_name];
    return $this->getOccurrenceHelper()->getInstanceReferenceFieldForRecurField($recur_field_storage);
  }

}
