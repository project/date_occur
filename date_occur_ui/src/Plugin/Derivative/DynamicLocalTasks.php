<?php

namespace Drupal\date_occur_ui\Plugin\Derivative;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\field\FieldStorageConfigInterface;

/**
 * Defines dynamic local tasks.
 */
class DynamicLocalTasks extends DeriverBase implements ContainerDeriverInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The base plugin ID.
   *
   * @var string
   */
  protected $basePluginId;

  /**
   * Constructs a new ConfigTranslationLocalTasks.
   *
   * @param string $base_plugin_id
   *   The base plugin ID.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $$entity_type_manager
   *   The entity type manager service.
   */
  public function __construct($base_plugin_id, EntityTypeManagerInterface $entity_type_manager) {
    $this->basePluginId = $base_plugin_id;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static($base_plugin_id,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $fields = $this->entityTypeManager
      ->getStorage('field_storage_config')
      ->loadByProperties(['type' => 'date_occur_parent']);

    $entity_types = [];
    foreach ($fields as $field) {
      assert($field instanceof FieldStorageConfigInterface);
      $entity_types[$field->id()] = $field->getTargetEntityTypeId();
    }

    foreach ($entity_types as $field_id => $entity_type) {
      $view_id = 'date_occur_ui.' . $field_id . '.view'; 
      $this->derivatives[$view_id]['route_name'] = 'date_occur_ui.' . $field_id . '.view';
      $this->derivatives[$view_id]['base_route'] = 'date_occur_ui.' . $field_id . '.view';
      $this->derivatives[$view_id]['title'] = 'View';

      $create_id = 'date_occur_ui.' . $field_id . '.create';
      $this->derivatives[$create_id]['route_name'] = 'date_occur_ui.' . $field_id . '.create';
      $this->derivatives[$create_id]['base_route'] = 'date_occur_ui.' . $field_id . '.view';
      $this->derivatives[$create_id]['title'] = 'Override occurrence';

      $edit_id = 'date_occur_ui.' . $field_id . '.edit_all';
      $this->derivatives[$edit_id]['route_name'] = 'date_occur_ui.' . $field_id . '.edit_all';
      $this->derivatives[$edit_id]['base_route'] = 'date_occur_ui.' . $field_id . '.view';
      $this->derivatives[$edit_id]['title'] = 'Edit all';

      $future_id = 'date_occur_ui.' . $field_id . '.edit';
      $this->derivatives[$future_id]['route_name'] = 'date_occur_ui.' . $field_id . '.edit_future';
      $this->derivatives[$future_id]['base_route'] = 'date_occur_ui.' . $field_id . '.view';
      $this->derivatives[$future_id]['title'] = 'Edit future';
    }

    return parent::getDerivativeDefinitions($base_plugin_definition);
  }

}
