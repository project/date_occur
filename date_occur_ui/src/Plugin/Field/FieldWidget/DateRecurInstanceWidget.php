<?php

declare(strict_types = 1);

namespace Drupal\date_occur_ui\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\date_recur\Plugin\Field\FieldWidget\DateRecurBasicWidget;

/**
 * Widget for date_recur without recurrence.
 *
 * Allows reusing field for instances to override an occurance of a recurring
 * date. As it adds TimeZone we're using that rather than the
 * DateRangeDefaultWidget.
 *
 * @FieldWidget(
 *   id = "date_recur_occur_instance",
 *   label = @Translation("Instance overriding an recurring date occurance"),
 *   field_types = {
 *     "date_recur"
 *   }
 * )
 */
class DateRecurInstanceWidget extends DateRecurBasicWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $element['first_occurrence']['#title'] = $this->t('This occurrence');
    // Most of the widgets don't use a field set. The basic one does.
    // This is because of potential end date / rrrule confusion we don't have.
    // But assuming consistency with other widgets is likely to be wanted
    // default to without.
    $element['first_occurrence']['#type'] = 'container';
    $element['rrule'] = [
      '#type' => 'value',
      '#value' => '',
    ];

    return $element;
  }

}
