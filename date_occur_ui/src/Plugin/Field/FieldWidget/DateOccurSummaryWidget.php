<?php

declare(strict_types = 1);

namespace Drupal\date_occur_ui\Plugin\Field\FieldWidget;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\date_occur\Plugin\Field\FieldType\DateOccurParentInterface;
use Drupal\date_occur_ui\Plugin\Field\FieldFormatter\OccurrenceListFormatterTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Widget giving summary of which occurrence is being edited.
 *
 * @FieldWidget(
 *   id = "date_occur_summary",
 *   label = @Translation("Summary of which occurrence"),
 *   field_types = {
 *     "date_occur_parent"
 *   }
 * )
 */
class DateOccurSummaryWidget extends WidgetBase {

  use OccurrenceListFormatterTrait;

  /**
   * Constructs a WidgetBase object.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Entity\EntityStorageInterface $date_format_storage
   *   The date format entity storage.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, protected DateFormatterInterface $dateFormatter, protected EntityStorageInterface $dateFormatStorage) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('date.formatter'),
      $container->get('entity_type.manager')->getStorage('date_format')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    $item = $items->first();
    assert($item instanceof DateOccurParentInterface);
    $date = $item->getParentDateRecur();
    $date->set('value', $item->value);
    $date->set('end_value', $item->end_value);
    $start_date = $date->start_date;
    $this->setTimeZone($start_date);
    $end_date = $date->end_date;
    $this->setTimeZone($end_date);
    $element['#type'] = 'value';
    $element['#value'] = $item->getValue();
    $element['date_occur_parent_description'] = [
      '#title' => $this->t('Overriding occurrence'),
      '#description' => [
        'title' => [
          '#type' => 'processed_text',
          // @todo getTranslation
          '#text' => $items->entity->label(),
        ],
        'description' => $this->buildDateRangeValue($start_date, $end_date, TRUE),
      ],
      '#type' => 'item',
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'timezone_override' => '',
      'occurrence_format_type' => 'medium',
      'same_end_date_format_type' => 'medium',
      'separator' => '-',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);

    $time = new DrupalDateTime();
    $format_types = $this->dateFormatStorage->loadMultiple();
    $options = [];
    foreach ($format_types as $type => $type_info) {
      $format = $this->dateFormatter->format($time->getTimestamp(), $type);
      $options[$type] = $type_info->label() . ' (' . $format . ')';
    }
    $format_type_select = [
      '#type' => 'select',
      '#title' => $this->t('Date format'),
      '#options' => $options,
      '#default_value' => $this->getSetting('format_type'),
    ];

    $element['occurrence_format_type'] = $format_type_select;
    $element['occurrence_format_type']['#title'] = $this->t('Start and end date format');
    $element['occurrence_format_type']['#default_value'] = $this->getSetting('occurrence_format_type');
    $element['occurrence_format_type']['#description'] = $this->t('Date format used for field values with repeat rules.');
    $element['same_end_date_format_type'] = $format_type_select;
    $element['same_end_date_format_type']['#title'] = $this->t('Same day end date format');
    $element['same_end_date_format_type']['#description'] = $this->t('Date format used for end date if field value has repeat rule. Used only if occurs on same calendar day as start date.');
    $element['same_end_date_format_type']['#default_value'] = $this->getSetting('same_end_date_format_type');

    $element['separator'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Date separator'),
      '#description' => $this->t('The string to separate the start and end dates'),
      '#default_value' => $this->getSetting('separator'),
    ];

    // Redefine timezone to change the natural order of form fields.
    $element['timezone_override'] = [
      '#type' => 'select',
      '#title' => $this->t('Time zone override'),
      '#description' => $this->t('Timezone used for displaying dates.'),
      '#options' => \system_time_zones(TRUE, TRUE),
      '#default_value' => $this->getSetting('timezone_override'),
      '#empty_option' => $this->t('Use user timezone'),
    ];
    $options = $element['timezone_override']['#options'];
    $options = ['date_timezone' => $this->t('Use timezone on the date')] + $options;
    $element['timezone_override']['#options'] = $options;

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    #$date = new DrupalDateTime();
    #$summary[] = $this->t('Format: @display', ['@display' => $this->formatDate($date)]);

    if ($separator = $this->getSetting('separator')) {
      $summary[] = $this->t('Separator: %separator', ['%separator' => $separator]);
    }

    if ($override = $this->getSetting('timezone_override')) {
      $summary[] = $this->t('Time zone: @timezone', ['@timezone' => $override]);
    }

    return $summary;
  }

}
