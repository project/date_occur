<?php

declare(strict_types = 1);

namespace Drupal\date_occur_ui\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\DependencyTrait;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\TypedData\TypedDataManagerInterface;
use Drupal\date_occur\OccurReferenceHelperInterface;
use Drupal\date_occur\Plugin\DataType\DateOccurrence;
use Drupal\date_recur\Entity\DateRecurInterpreterInterface;
use Drupal\date_recur\Plugin\Field\FieldType\DateRecurItem;
use Drupal\datetime_range\Plugin\Field\FieldFormatter\DateRangeDefaultFormatter;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Recurring date field formatter with occurence option.
 *
 * @FieldFormatter(
 *   id = "date_occur_ui_recur_formatter",
 *   label = @Translation("Occurrence formatter"),
 *   field_types = {
 *     "date_recur"
 *   }
 * )
 */
class DateRecurOccurFormatter extends DateRangeDefaultFormatter {

  use DependencyTrait;
  use OccurrenceListFormatterTrait;

  /**
   * Date format config ID.
   *
   * @var string|null
   */
  protected ?string $formatType;

  /**
   * Constructs a new DateRecurOccurFormatter.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Third party settings.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $dateFormatter
   *   The date formatter service.
   * @param \Drupal\Core\Entity\EntityStorageInterface $dateFormatStorage
   *   The date format entity storage.
   * @param \Drupal\Core\Entity\EntityStorageInterface $dateRecurInterpreterStorage
   *   The date recur interpreter entity storage.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $currentRouteMatch
   *   The current route match.
   * @param \Drupal\Core\TypedData\TypedDataManagerInterface $typedDataManager
   *   Typed Data Manager.
   * @param \Drupal\date_occur\OccurReferenceHelperInterface $occurReferenceHelper
   *   Occurrence reference helper service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, DateFormatterInterface $dateFormatter, EntityStorageInterface $dateFormatStorage, EntityTypeManagerInterface $entityTypeManager, protected EntityStorageInterface $dateRecurInterpreterStorage, protected CurrentRouteMatch $currentRouteMatch, protected TypedDataManagerInterface $typedDataManager, protected OccurReferenceHelperInterface $occurReferenceHelper) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings, $dateFormatter, $dateFormatStorage);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('date.formatter'),
      $container->get('entity_type.manager')->getStorage('date_format'),
      $container->get('entity_type.manager'),
      $container->get('entity_type.manager')->getStorage('date_recur_interpreter'),
      $container->get('current_route_match'),
      $container->get('typed_data_manager'),
      $container->get('date_occur.occur_reference_helper'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return [
      // Show number of occurrences.
      'show_next' => 1,
      // Date format for occurrences.
      'occurrence_format_type' => 'medium',
      // Date format for end date, if same day as start date.
      'same_end_date_format_type' => 'medium',
      'interpreter' => NULL,
      'show_occurrence_title' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies(): array {
    $this->dependencies = parent::calculateDependencies();

    /** @var string|null $dateFormatId */
    $interpreterId = $this->getSetting('interpreter');
    if ($interpreterId && ($interpreter = $this->dateRecurInterpreterStorage->load($interpreterId))) {
      $this->addDependency('config', $interpreter->getConfigDependencyName());
    }

    $dateFormatDependencies = [
      'format_type',
      'occurrence_format_type',
      'same_end_date_format_type',
    ];
    foreach ($dateFormatDependencies as $dateFormatId) {
      $id = $this->getSetting($dateFormatId);
      $dateFormat = $this->dateFormatStorage->load($id);
      if (!$dateFormat) {
        continue;
      }
      $this->addDependency('config', $dateFormat->getConfigDependencyName());
    }

    return $this->dependencies;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $form = parent::settingsForm($form, $form_state);

    $originalFormatType = $form['format_type'];
    unset($form['format_type']);

    // Redefine format type to change the natural order of form fields.
    $form['format_type'] = $originalFormatType;
    $form['format_type']['#title'] = $this->t('Non-Repeating Date format');
    $form['format_type']['#description'] = $this->t('Date format used for field values without repeat rules.');
    $form['occurrence_format_type'] = $originalFormatType;
    $form['occurrence_format_type']['#title'] = $this->t('Start and end date format');
    $form['occurrence_format_type']['#default_value'] = $this->getSetting('occurrence_format_type');
    $form['occurrence_format_type']['#description'] = $this->t('Date format used for field values with repeat rules.');
    $form['same_end_date_format_type'] = $originalFormatType;
    $form['same_end_date_format_type']['#title'] = $this->t('Same day end date format');
    $form['same_end_date_format_type']['#description'] = $this->t('Date format used for end date if field value has repeat rule. Used only if occurs on same calendar day as start date.');
    $form['same_end_date_format_type']['#default_value'] = $this->getSetting('same_end_date_format_type');

    // Redefine separator to change the natural order of form fields.
    $originalSeparator = $form['separator'];
    unset($form['separator']);
    $form['separator'] = $originalSeparator;
    // Change the width of the field if not already set. (Not set by default)
    $form['separator']['#size'] ??= 5;

    // Redefine timezone to change the natural order of form fields.
    $originalTimezoneOverride = $form['timezone_override'];
    unset($form['timezone_override']);
    $form['timezone_override'] = $originalTimezoneOverride;
    $form['timezone_override']['#empty_option'] = $this->t('Use user timezone');
    $options = $form['timezone_override']['#options'];
    $options = ['date_timezone' => $this->t('Use timezone on the date')] + $options;
    $form['timezone_override']['#options'] = $options;
    $form['timezone_override']['#description'] = $this->t('Timezone used for displaying dates.');

    $interpreterOptions = array_map(
      fn (DateRecurInterpreterInterface $interpreter): string => $interpreter->label() ?? (string) $this->t('- Missing label -'),
      $this->dateRecurInterpreterStorage->loadMultiple()
    );
    $form['interpreter'] = [
      '#type' => 'select',
      '#title' => $this->t('Recurring date interpreter'),
      '#description' => $this->t('Choose a plugin for converting rules into a human readable description.'),
      '#default_value' => $this->getSetting('interpreter'),
      '#options' => $interpreterOptions,
      '#required' => FALSE,
      '#empty_option' => $this->t('- Do not show interpreted rule -'),
    ];

    $form['show_next'] = [
      '#field_prefix' => $this->t('Show maximum of'),
      '#field_suffix' => $this->t('occurrences'),
      '#type' => 'number',
      '#min' => 0,
      '#default_value' => $this->getSetting('show_next'),
      '#attributes' => ['size' => 4],
    ];

    $form['show_occurrence_title'] = [
      '#type' => 'select',
      '#title' => $this->t('Show title'),
      '#description' => $this->t('Display the title alongside occurrence date'),
      '#options' => [
        '' => $this->t('Never show title'),
        'different' => $this->t('Show if different'),
        'always' => $this->t('Always show title'),
      ],
      '#default_value' => $this->getSetting('show_occurrence_title'),
    ];


    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    $this->formatType = $this->getSetting('format_type');
    $summary = parent::settingsSummary();

    $start = new DrupalDateTime('today 9am');
    $endSameDay = clone $start;
    $endSameDay->setTime(17, 0, 0);
    $summary['sample_same_day'] = [
      '#type' => 'inline_template',
      '#template' => '{{ label }}: {{ sample }}',
      '#context' => [
        'label' => $this->t('Same day range'),
        'sample' => $this->buildDateRangeValue($start, $endSameDay, TRUE),
      ],
    ];
    $endDifferentDay = clone $endSameDay;
    $endDifferentDay->modify('+1 day');
    $summary['sample_different_day'] = [
      '#type' => 'inline_template',
      '#template' => '{{ label }}: {{ sample }}',
      '#context' => [
        'label' => $this->t('Different day range'),
        'sample' => $this->buildDateRangeValue($start, $endDifferentDay, TRUE),
      ],
    ];

    $show_next = $this->getSetting('show_next');
    if ($show_next > 0) {
      $summary[] = $this->formatPlural(
        $show_next,
        'Show maximum of @count occurrence',
        'Show maximum of @count occurrences',
      );
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $occurrence = $this->currentRouteMatch->getParameter('date_occur');
    $cacheability = new CacheableMetadata();
    // Can't vary on a specific parameter?
    $cacheability->addCacheContexts(['url.path']);
    $elements = [];
    foreach ($items as $delta => $item) {
      $elements[$delta] = $this->viewItem($item, $occurrence);
    }
    $cacheability->applyTo($elements);
    return $elements;
  }

  /**
   * Generate the output appropriate for a field item.
   *
   * @param \Drupal\date_recur\Plugin\Field\FieldType\DateRecurItem $item
   *   A field item.
   *
   * @return array
   *   A render array for a field item.
   */
  protected function viewItem(DateRecurItem $item, ?DateOccurrence $current_occurrence): array {
    $cacheability = new CacheableMetadata();
    $build = [
      '#theme' => 'date_occur_recur_basic_formatter',
      '#is_recurring' => $item->isRecurring(),
    ];

    $startDate = $item->start_date;
    /** @var \Drupal\Core\Datetime\DrupalDateTime|null $endDate */
    $endDate = $item->end_date ?? $startDate;
    if (!$startDate || !$endDate) {
      return $build;
    }
    if ($current_occurrence) {
      $startDate = $current_occurrence->get('start_date')->getDateTime();
      $startDate->setTimezone($item->start_date->getTimezone());
      $endDate = $current_occurrence->get('end_date')->getDateTime();
      $endDate->setTimezone($item->end_date->getTimezone());
    }
    $build['#date'] = $this->buildDateRangeValue($startDate, $endDate, FALSE);

    // Render the rule.
    if ($item->isRecurring() && $this->getSetting('interpreter')) {
      /** @var string|null $interpreterId */
      $interpreterId = $this->getSetting('interpreter');
      if ($interpreterId && ($interpreter = $this->dateRecurInterpreterStorage->load($interpreterId))) {
        assert($interpreter instanceof DateRecurInterpreterInterface);
        $rules = $item->getHelper()->getRules();
        $plugin = $interpreter->getPlugin();
        $cacheability->addCacheableDependency($interpreter);
        $build['#interpretation'] = $plugin->interpret($rules, 'en');
      }
    }

    // Occurrences are generated even if the item is not recurring.
    $occur_field_id = $this->occurReferenceHelper->getInstanceReferenceFieldForRecurField($item->getFieldDefinition()->getFieldStorageDefinition());
    list($occur_entity_type, $occur_field_name) = explode('.', $occur_field_id);
    $occurrences = $this->occurReferenceHelper->getNextOccurrences($item, $occur_entity_type, $occur_field_name, $this->fieldDefinition->getName(), $this->getSetting('show_next'), $current_occurrence);
    $build['#occurrences'] = $this->buildOccurrenceRangeValues($occur_field_id, $item->getEntity()->id(), $occurrences, $cacheability, $this->getSetting('show_occurrence_title'));

    $cacheability->applyTo($build);
    return $build;
  }

}
