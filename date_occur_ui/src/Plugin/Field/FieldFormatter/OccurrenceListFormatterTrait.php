<?php

declare(strict_types = 1);

namespace Drupal\date_occur_ui\Plugin\Field\FieldFormatter;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Url;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItem;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * Field formatter methods for listing next occurrences.
 */
trait OccurrenceListFormatterTrait {

  protected function buildOccurrenceRangeValues(string $field_name, $entity_id, $occurrences, CacheableMetadata $cacheability, $show_title): array {
    $links = [];
    foreach ($occurrences as $occurrence) {
      $start = $occurrence->getStart();
      $end = $occurrence->getEnd();
      $entity = $occurrence->getEntity();
      if ($entity->id() == $entity_id) {
        $url = Url::fromRoute('date_occur_ui.' . $field_name . '.view', [
          $entity->getEntityTypeId() => $entity->id(),
          'date_occur' => $occurrence->getRangeId(),
        ]);
      }
      else {
        $url = $entity->toUrl();
        $cacheability->addCacheableDependency($entity);
      }
      $link_text = [$this->buildDateRangeValue(
        $start,
        $end,
        TRUE
      )];
      $different_title = ($parent = $occurrence->getParent()) && ($parent->label() != $entity->label());
      if ($show_title == 'always' ||
        ($show_title == 'different' && $different_title)
      ) {
        $link_text[] = ['#plain_text' => $entity->label()];
      }
      $links[] = [
        '#type' => 'link',
        '#title' =>$link_text,
        '#url' => $url,
      ];
    }
    return $links;
  }

  /**
   * Builds a date range suitable for rendering.
   *
   * @param \Drupal\Core\Datetime\DrupalDateTime $startDate
   *   The start date.
   * @param \Drupal\Core\Datetime\DrupalDateTime $endDate
   *   The end date.
   * @param bool $isOccurrence
   *   Whether the range is an occurrence of a repeating value.
   *
   * @return array
   *   A render array.
   */
  protected function buildDateRangeValue(DrupalDateTime $startDate, DrupalDateTime $endDate, $isOccurrence): array {
    $this->formatType = $isOccurrence ? $this->getSetting('occurrence_format_type') : $this->getSetting('format_type');
    $startDateString = $this->buildDateWithIsoAttribute($startDate);

    // Show the range if start and end are different, otherwise only start date.
    if ($startDate->getTimestamp() === $endDate->getTimestamp()) {
      return $startDateString;
    }
    else {
      // Start date and end date are different.
      $this->formatType = $startDate->format('Ymd') == $endDate->format('Ymd') ?
        $this->getSetting('same_end_date_format_type') :
        $this->getSetting('occurrence_format_type');
      $endDateString = $this->buildDateWithIsoAttribute($endDate);
      return [
        '#theme' => 'date_occur_date_range',
        '#start_date' => $startDateString,
        '#separator' => ['#plain_text' => $this->getSetting('separator')],
        '#end_date' => $endDateString,
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function formatDate($date): string {
    assert($date instanceof DrupalDateTime);
    is_string($this->formatType) ?: throw new \LogicException('Date format must be set.');
    // This should be set by a call to setTimeZone() before formatDate().
    $timezone = $date->getTimezone()->getName();
    return $this->dateFormatter->format($date->getTimestamp(), $this->formatType, '', $timezone);
  }

  /**
   * Sets timezone, including accounting for timezone override setting.
   */
  protected function setTimeZone(DrupalDateTime $date) {
    $timezone = '';
    if ($this->getFieldSetting('datetime_type') === DateTimeItem::DATETIME_TYPE_DATE) {
      // A date without time has no timezone conversion.
      $timezone = DateTimeItemInterface::STORAGE_TIMEZONE;
    }
    elseif ($this->getSetting('timezone_override') == 'date_timezone') {
      // Don't set a different timezone, use the one attached to the date.
      $timezone = '';
    }
    elseif ($this->getSetting('timezone_override') == '') {
      // Change the date to the users timezone.
      $timezone = \date_default_timezone_get();
    }
    else {
      // Use the overriden timezone.
      $timezone = $this->getSetting('timezone_override');
    }
    if ($timezone != '') {
      $date->setTimeZone(new \DateTimeZone($timezone));
    }
  }

  /**
   * Creates a render array from a date object with ISO date attribute.
   *
   * @param \Drupal\Core\Datetime\DrupalDateTime $date
   *   A date object.
   *
   * @return array
   *   A render array.
   */
  protected function buildDateWithIsoAttribute(DrupalDateTime $date) {
    // Create the ISO date.
    // Don't hide the offset of the date.
    $iso_date = $date->format('c');

    // Display for the user in the desired timezone.
    $this->setTimeZone($date);

    $build = [
      '#theme' => 'time',
      '#text' => $this->formatDate($date),
      '#attributes' => [
        'datetime' => $iso_date,
      ],
      '#cache' => [
        'contexts' => [
          'timezone',
        ],
      ],
    ];

    return $build;
  }

}
