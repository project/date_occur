<?php

declare(strict_types = 1);

namespace Drupal\date_occur_ui\Plugin\Field\FieldFormatter;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\DependencyTrait;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\date_occur\OccurReferenceHelperInterface;
use Drupal\date_occur\Plugin\Field\FieldFormatter\DateOccurParentBasicFormatter;

/**
 * Recurring date occurrence basic field formatter.
 *
 * @FieldFormatter(
 *   id = "date_occur_occurrences_formatter",
 *   label = @Translation("Formatter with occurrences"),
 *   field_types = {
 *     "date_occur_parent"
 *   }
 * )
 */
class DateOccurParentOccurFormatter extends DateOccurParentBasicFormatter {

  use DependencyTrait;
  use OccurrenceListFormatterTrait;

  /**
   * Constructs a new DateOccurParentBasicFormatter.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Third party settings.
   * @param \Drupal\Core\Entity\EntityStorageInterface $dateRecurInterpreterStorage
   *   The date recur interpreter entity storage.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $dateFormatter
   *   The date formatter service.
   * @param \Drupal\Core\Entity\EntityStorageInterface $dateFormatStorage
   *   The date format entity storage.
   * @param \Drupal\date_occur\OccurReferenceHelperInterface $occurReferenceHelper
   *   Occurrence reference helper service.
   */ 
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, EntityStorageInterface $dateRecurInterpreterStorage, protected DateFormatterInterface $dateFormatter, protected EntityStorageInterface $dateFormatStorage, protected OccurReferenceHelperInterface $occurReferenceHelper) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings, $dateRecurInterpreterStorage);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager')->getStorage('date_recur_interpreter'),
      $container->get('date.formatter'),
      $container->get('entity_type.manager')->getStorage('date_format'),
      $container->get('date_occur.occur_reference_helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return [
      // Show number of occurrences.
      'show_next' => 1,
      'separator' => '-',
      'timezone_override' => '',
      'occurrence_format_type' => 'medium',
      'same_end_date_format_type' => 'medium',
      'show_occurrence_title' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies(): array {
    $this->dependencies = parent::calculateDependencies();

    /** @var string|null $dateFormatId */
    $interpreterId = $this->getSetting('interpreter');
    if ($interpreterId && ($interpreter = $this->dateRecurInterpreterStorage->load($interpreterId))) {
      $this->addDependency('config', $interpreter->getConfigDependencyName());
    }

    $dateFormatDependencies = [
      'occurrence_format_type',
      'same_end_date_format_type',
    ];
    foreach ($dateFormatDependencies as $dateFormatId) {
      $id = $this->getSetting($dateFormatId);
      $dateFormat = $this->dateFormatStorage->load($id);
      if (!$dateFormat) {
        continue;
      }
      $this->addDependency('config', $dateFormat->getConfigDependencyName());
    }

    return $this->dependencies;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $form = parent::settingsForm($form, $form_state);

    $form['show_next'] = [
      '#field_prefix' => $this->t('Show maximum of'),
      '#field_suffix' => $this->t('occurrences'),
      '#type' => 'number',
      '#min' => 0,
      '#default_value' => $this->getSetting('show_next'),
      '#attributes' => ['size' => 4],
    ];

    $form['show_occurrence_title'] = [
      '#type' => 'select',
      '#title' => $this->t('Show occurrence title'),
      '#description' => $this->t('Display the title alongside occurrence date'),
      '#options' => [
        '' => $this->t('Never show title'),
        'different' => $this->t('Show if different'),
        'always' => $this->t('Always show title'),
      ],
      '#default_value' => $this->getSetting('show_occurrence_title'),
    ];

    $time = new DrupalDateTime();
    $format_types = $this->dateFormatStorage->loadMultiple();
    $options = [];
    foreach ($format_types as $type => $type_info) {
      $format = $this->dateFormatter->format($time->getTimestamp(), $type);
      $options[$type] = $type_info->label() . ' (' . $format . ')';
    }
    $format_type_select = [
      '#type' => 'select',
      '#title' => $this->t('Date format'),
      '#options' => $options,
      '#default_value' => $this->getSetting('format_type'),
    ];

    $form['occurrence_format_type'] = $format_type_select;
    $form['occurrence_format_type']['#title'] = $this->t('Start and end date format');
    $form['occurrence_format_type']['#default_value'] = $this->getSetting('occurrence_format_type');
    $form['occurrence_format_type']['#description'] = $this->t('Date format used for field values with repeat rules.');
    $form['same_end_date_format_type'] = $format_type_select;
    $form['same_end_date_format_type']['#title'] = $this->t('Same day end date format');
    $form['same_end_date_format_type']['#description'] = $this->t('Date format used for end date if field value has repeat rule. Used only if occurs on same calendar day as start date.');
    $form['same_end_date_format_type']['#default_value'] = $this->getSetting('same_end_date_format_type');

    // Change the width of the field if not already set. (Not set by default)
    $form['separator'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Date separator'),
      '#description' => $this->t('The string to separate the start and end dates'),
      '#default_value' => $this->getSetting('separator'),
      '#size' => 5,
    ];

    // Redefine timezone to change the natural order of form fields.
    $form['timezone_override'] = [
      '#type' => 'select',
      '#title' => $this->t('Time zone override'),
      '#description' => $this->t('Timezone used for displaying dates.'),
      '#options' => \system_time_zones(TRUE, TRUE),
      '#default_value' => $this->getSetting('timezone_override'),
      '#empty_option' => $this->t('Use user timezone'),
    ];
    $options = $form['timezone_override']['#options'];
    $options = ['date_timezone' => $this->t('Use timezone on the date')] + $options;
    $form['timezone_override']['#options'] = $options;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    $this->formatType = $this->getSetting('format_type');
    $summary = parent::settingsSummary();

    if ($separator = $this->getSetting('separator')) {
      $summary[] = $this->t('Separator: %separator', ['%separator' => $separator]);
    }

    $start = new DrupalDateTime('today 9am');
    $endSameDay = clone $start;
    $endSameDay->setTime(17, 0, 0);
    $summary['sample_same_day'] = [
      '#type' => 'inline_template',
      '#template' => '{{ label }}: {{ sample }}',
      '#context' => [
        'label' => $this->t('Same day range'),
        'sample' => $this->buildDateRangeValue($start, $endSameDay, TRUE),
      ],
    ];
    $endDifferentDay = clone $endSameDay;
    $endDifferentDay->modify('+1 day');
    $summary['sample_different_day'] = [
      '#type' => 'inline_template',
      '#template' => '{{ label }}: {{ sample }}',
      '#context' => [
        'label' => $this->t('Different day range'),
        'sample' => $this->buildDateRangeValue($start, $endDifferentDay, TRUE),
      ],
    ];

    $show_next = $this->getSetting('show_next');
    if ($show_next > 0) {
      $summary[] = $this->formatPlural(
        $show_next,
        'Show maximum of @count occurrence',
        'Show maximum of @count occurrences',
      );
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    if (!$items->isEmpty()) {
      $entity = $items->first()->getEntity();
    }
    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $parent) {
      if ($parent->id()) {
        $elements[$delta] = $this->viewItemEntity($parent, $entity);
      }
    }

    return $elements;
  }

  protected function viewItemEntity(EntityInterface $parent, EntityInterface $entity): array {
    $build = parent::viewItemEntity($parent, $entity);
    $build['#theme'] = 'date_occur_occurrences_formatter';

    $cacheability = CacheableMetadata::createFromRenderArray($build);

    $recur_field_name = $this->fieldDefinition->getSetting('target_field');
    $occur_entity_type = $this->fieldDefinition->getTargetEntityTypeId();
    $occur_field_name = $this->fieldDefinition->getName();
    $date_field_name = $this->fieldDefinition->getSettings()['target_field'];
    $occurrences = $this->occurReferenceHelper->getNextOccurrences($parent->$recur_field_name->first(), $occur_entity_type, $occur_field_name, $date_field_name, $this->getSetting('show_next'), $entity->$occur_field_name->first()->getOccurrence());
    $build['#occurrences'] = $this->buildOccurrenceRangeValues($occur_entity_type . '.' . $occur_field_name, $parent->id(), $occurrences, $cacheability, $this->getSetting('show_occurrence_title'));
    
    $cacheability->applyTo($build);

    return $build;
  }

}
