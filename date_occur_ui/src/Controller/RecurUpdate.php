<?php

namespace Drupal\date_occur_ui\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityFormInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\date_occur\Plugin\DataType\DateOccurrence;
use Drupal\date_occur_ui\Form\RecurUpdateConfirmForm;
use Drupal\date_recur\Rl\RlHelper;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\replicate\Replicator;
use RRule\RfcParser;
use RRule\RRule;

/**
 * Entity with recur rule edit form controller.
 *
 * @todo the code is working it out code. It really needs a tidy up.
 */
class RecurUpdate extends ControllerBase {

  /**
   * Constructs a new enhanced entity edit form with recurring date.
   *
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $privateTempStoreFactory
   *   The private store factory.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Drupal\replicate\Replicator $replicator
   *   The entity replicator service.
   */
  public function __construct(protected PrivateTempStoreFactory $privateTempStoreFactory, protected RendererInterface $renderer, protected Replicator $replicator) {
  }

  /**
   * {@inheritdoc}
   */
  public function editFutureForm(RouteMatchInterface $route_match, DateOccurrence $date_occur) {
    $route = $route_match->getRouteObject();
    $field_id = substr(strstr($route->getOptions()['parameters']['date_occur']['type'], ':'), 1);
    list($entity_type, $occur_field_name) = explode('.', $field_id);
    $original = $route_match->getParameter($entity_type);

    $wizard_id = 'date_occur_ui_recur_future';
    $store = $this->privateTempStoreFactory->get($wizard_id);
    $store_id = $field_id . ':' . $original->id();
    $extra['date_occur_wizard_id'] = $wizard_id;
    $extra['store_id'] = $store_id;
    $extra['date_occur_occurrence'] = $date_occur;

    // See if we are on the second step of the form.
    $step = $store->get("$store_id:step");
    // Entity edit form, potentially as wizard step 1.
    if (empty($step) || $step == 1) {
      // Only clone entity if we have nothing stored.
      if (!$copy = $store->get("$store_id:entity")) {
        $copy = $this->cloneEntity($original);
        // Change the copy to start the date recurrence from the occurrence.
        $start_date = $date_occur->get('start_date')->getDateTime();
        $end_date = $date_occur->get('end_date')->getDateTime();
        $occur_field = $this->entityTypeManager()
          ->getStorage('field_storage_config')
          ->load($field_id);
        $date_field_name = $occur_field->getSetting('target_field');
        $copy_date = $copy->get($date_field_name)->get(0)->getValue();
        $copy_date['value'] = $start_date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
        $copy_date['end_value'] = $end_date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
        $copy->get($date_field_name)->setValue([$copy_date]);
      }
      $form = $this->entityFormBuilder()->getForm($copy, 'default', $extra);
    }
    else {
      $copy = $store->get("$store_id:entity");
      $form = $this->formBuilder()->getForm(RecurUpdateConfirmForm::class, $copy, $extra);
    }

    return $form;
  }

  /**
   * Alters from submit to store. Submit used later after confirmation.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @see date_occur_ui_form_alter()
   */
  static public function formAlter(array &$form, FormStateInterface $form_state) {
    $form['actions']['submit']['#access'] = FALSE;
    $form['actions']['store'] = [
      '#submit' => ['::submitForm', [static::class, 'store']],
      '#type' => 'submit',
      '#value' => \t('Save confirm'),
    ];
  }

  /**
   * If there are overrides store for confirmation form, otherwise save.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  static public function store(array &$form, FormStateInterface $form_state) {
    $new_entity = $form_state->getFormObject()->getEntity();
    $store_id = $form_state->get('store_id');
    list($occur_field_id, $original_id) = explode(':', $store_id);
    list($entity_type, $occur_field_name) = explode('.', $occur_field_id);

    $storage = \Drupal::entityTypeManager()
      ->getStorage($new_entity->getEntityTypeId());
    $original_entity = $storage->load($original_id);
    $occur_field = \Drupal::entityTypeManager()
      ->getStorage('field_storage_config')
      ->load($occur_field_id);
    $date_field_name = $occur_field->getSetting('target_field');

    // Are there any overrides.
    $overrides = \Drupal::entityQuery($new_entity->getEntityTypeId())
      ->condition($occur_field_name . '.target_id', $original_id)
      ->accessCheck(FALSE)
      ->execute();
    $start_set = RlHelper::createInstance($new_entity->$date_field_name->rrule, $new_entity->$date_field_name->start_date->getPhpDateTime())->getRlRuleset();
    $end_set = RlHelper::createInstance($new_entity->$date_field_name->rrule, $new_entity->$date_field_name->end_date->getPhpDateTime())->getRlRuleset();

    $affected_overrides = [];
    foreach ($storage->loadMultiple($overrides) as $override) {
      if ($override->$date_field_name->start_date >= $new_entity->$date_field_name->start_date) {
        $affected_overrides[] = $override->id();
      }
    }

    if (count($affected_overrides)) {
      // If so do the match with the dates in the sequence.
      // Matches need updating to reference the new entity.
      //
      // If some no longer match warn the user - potentially allow for updating.
      // Store the unsaved entity in the temp store.
      $store = \Drupal::service('tempstore.private')->get($form_state->get('date_occur_wizard_id'));
      $store_id = $form_state->get('store_id');
      $store->set("$store_id:entity", $new_entity);
      $store->set("$store_id:step", 2);

      // Disable any URL-based redirect until the final step.
      $request = \Drupal::service('request_stack')->getCurrentRequest();
      $form_state->setRedirect('<current>', [], ['query' => $request->query->all()]);
      $request->query->remove('destination');
    }
    else {
      $occurrence = $form_state->get('date_occur_occurrence');
      static::updateOriginal($original_entity, $occurrence, $date_field_name);
      static::saveNew($form, $form_state, $new_entity);
      static::clearStore($form_state);
      return;
    }
  }

  /**
   * Confirm saving the new entity, update the orginal and the overrides.
   */
  public static function confirm(array $form, FormStateInterface $form_state) {
    $store = \Drupal::service('tempstore.private')->get($form_state->get('date_occur_wizard_id'));
    $store_id = $form_state->get('store_id');
    $new_entity = $form_state instanceof EntityFormInterface ? $form_state->getFormObject()->getEntity(): $store->get("$store_id:entity");
    list($occur_field_id, $original_id) = explode(':', $store_id);
    list($entity_type, $occur_field_name) = explode('.', $occur_field_id);
    $storage = \Drupal::entityTypeManager()
      ->getStorage($new_entity->getEntityTypeId());
    $original_entity = $storage->load($original_id);
    $occur_field = \Drupal::entityTypeManager()
      ->getStorage('field_storage_config')
      ->load($occur_field_id);
    $date_field_name = $occur_field->getSetting('target_field');
 
    $occurrence = $form_state->get('date_occur_occurrence');
    static::updateOriginal($original_entity, $occurrence, $date_field_name);
    static::saveNew($form, $form_state, $new_entity);
    static::updateFuture($new_entity->getEntityTypeId(), $original_entity->id(), $new_entity, $occurrence, $date_field_name, $occur_field_name);
    static::clearStore($form_state);
  }

  /**
   * Cancel don't save, remove the store so can start anew.
   */
  public static function cancel(array $form, FormStateInterface $form_state) {
    static::clearStore($form_state);
  }

  /**
   * Update future overrides to point to new entity.
   */
  public static function updateFuture($entity_type_id, $original_id, EntityInterface $new_entity, DateOccurrence $date_occur, $date_field_name, $occur_field_name) {
    $storage = \Drupal::entityTypeManager()
      ->getStorage($entity_type_id);
    $overrides = \Drupal::entityQuery($entity_type_id)
      ->condition($occur_field_name . '.target_id', $original_id)
      ->condition($date_field_name . '.value', $date_occur->get('start_date')->getValue(), '>')
      ->accessCheck(FALSE)
      ->execute();
    $start_set = RlHelper::createInstance($new_entity->$date_field_name->rrule, $new_entity->$date_field_name->start_date->getPhpDateTime())->getRlRuleset();
    $end_set = RlHelper::createInstance($new_entity->$date_field_name->rrule, $new_entity->$date_field_name->end_date->getPhpDateTime())->getRlRuleset();
    
    foreach ($storage->loadMultiple($overrides) as $override) {
      if ($override->$date_field_name->start_date >= $new_entity->$date_field_name->start_date) {
        if ($start_set->occursAt($override->$date_field_name->start_date->format('c'))
          && $end_set->occursAt($override->$date_field_name->end_date->format('c'))
        ) {
          $occur_reference = $override->get($occur_field_name)->first()->getValue();
          $occur_reference['target_id'] = $new_entity->id();
          $override->set($occur_field_name, $occur_reference);
          $override->save();
        }
        else {
          // Just unpublish for now. Configurable? Clean up cron?
          if ($override instanceof EntityPublishedInterface) {
            $override->setUnpublished();
            $override->save();
          }
        }
      }
    }

  }

  /**
   * Update the original to stop before the instance that starts new sequence.
   */
  public static function updateOriginal(EntityInterface $original_entity, DateOccurrence $date_occur, string $date_field_name) {
    $new_end = $date_occur->get('start_date')->getDateTime();
    $new_end->sub(new \DateInterval('PT1S'));
    $new_end->setTimezone(new \DateTimeZone('UTC'));
    $rrule_array = RfcParser::parseRRule($original_entity->$date_field_name->rrule);
    unset($rrule_array['count']);
    $rrule_array['until'] = $new_end;
    $rrule = new RRule($rrule_array);
    $date = $original_entity->$date_field_name->first();
    $original_entity->$date_field_name->setValue([
      'value' => $date->value,
      'end_value' => $date->end_value,
      'rrule' => $rrule->rfcString(),
      'timezone' => $date->timezone,
      'infinite' => FALSE,
    ]);
    $original_entity->save();
  }

  /**
   * Save the new entity.
   */
  public static function saveNew(array &$form, FormStateInterface $form_state, EntityInterface $entity) {
    $form_object = \Drupal::entityTypeManager()->getFormObject($entity->getEntityTypeId(), 'default');
    $form_object->setEntity($entity);
    $form_object->save($form, $form_state);
    return $form_object;
  }

  /**
   * Remove the stored values.
   */
  public static function clearStore(FormStateInterface $form_state) {
    $store = \Drupal::service('tempstore.private')->get($form_state->get('date_occur_wizard_id'));
    $store_id = $form_state->get('store_id');
    $store->delete("$store_id:step");
    $store->delete("$store_id:entity");
  }

  /**
   * Generate copy of the entity.
   *
   * Seperated to allow any classes inheriting to just alter this action.
   * Replicator also includes plenty of events to change what is cloned.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The group for which to generate the content.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   A clone of the entity.
   */
  public function cloneEntity(EntityInterface $entity) {
    $copy = $this->replicator->cloneEntity($entity);

    return $copy;
  }

}
