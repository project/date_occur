<?php

namespace Drupal\date_occur_ui\Controller;

use Drupal\Core\Controller\FormController;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Symfony\Component\HttpKernel\Controller\ArgumentResolverInterface;
use Drupal\replicate\Replicator;

/**
 * Instance page controller.
 */
class InstanceCreate extends FormController {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  
  /**
   * The entity replicator service.
   *
   * @var \Drupal\replicate\Replicator
   */
  protected $replicator;

  /**
   * Constructs a new \Drupal\Core\Routing\Enhancer\FormEnhancer object.
   *
   * @param \Symfony\Component\HttpKernel\Controller\ArgumentResolverInterface $argument_resolver
   *   The argument resolver.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\replicate\Replicator $replicator
   *   The entity replicator service.
   */
  public function __construct(ArgumentResolverInterface $argument_resolver, FormBuilderInterface $form_builder, EntityTypeManagerInterface $entity_type_manager, Replicator $replicator) {
    parent::__construct($argument_resolver, $form_builder);
    $this->entityTypeManager = $entity_type_manager;
    $this->replicator = $replicator;
  }

  /**
   * {@inheritdoc}
   */
  protected function getFormArgument(RouteMatchInterface $route_match) {
    $route = $route_match->getRouteObject();
    return $route->getOptions()['parameters']['date_occur']['entity_param'] . '.edit';
  }

  /**
   * {@inheritdoc}
   */
  protected function getFormObject(RouteMatchInterface $route_match, $form_arg) {
    $occurrence = $route_match->getParameter('date_occur');
    $original = $occurrence->getEntity();
    $copy = $this->cloneEntity($original);

    $route = $route_match->getRouteObject();
    $field_id = substr(strstr($route->getOptions()['parameters']['date_occur']['type'], ':'), 1);
    list($entity_type, $occur_field_name) = explode('.', $field_id);
    $form_object = $this->entityTypeManager
      ->getFormObject($entity_type, 'default');
    $occur_field = $this->entityTypeManager
      ->getStorage('field_storage_config')
      ->load($field_id);

    $start_date = $occurrence->get('start_date')->getDateTime();
    $end_date = $occurrence->get('end_date')->getDateTime();

    $copy->get($occur_field_name)->setValue([
      'entity' => $original,
      'field_delta' => $occurrence->get('field_delta')->getValue(),
      'value' => $start_date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
      'end_value' => $end_date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
    ]);
    $date_field_name = $occur_field->getSetting('target_field');
    $copy_date = $copy->get($date_field_name)->get(0)->getValue();
    $copy_date['value'] = $start_date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
    $copy_date['end_value'] = $end_date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
    $copy_date['rrule'] = '';
    $copy->get($date_field_name)->setValue([$copy_date]);
    $form_object->setEntity($copy);
    return $form_object;
  }

  /**
   * Generate default content for group.
   *
   * Seperated to allow any classes inheriting to just alter this action.
   * Replicator also includes plenty of events to change what is cloned.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The group for which to generate the content.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   A clone of the entity.
   */
  public function cloneEntity(EntityInterface $entity) {
    $copy = $this->replicator->cloneEntity($entity);

    return $copy;
  }

}
