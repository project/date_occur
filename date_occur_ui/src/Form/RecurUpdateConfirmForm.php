<?php

namespace Drupal\date_occur_ui\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\date_occur_ui\Controller\RecurUpdate;
use Drupal\date_recur\Rl\RlHelper;

/**
 * Builds a confirmation form for saving updates to recurring date.
 */
class RecurUpdateConfirmForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'date_occur_ui_recur_update_confirm_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $new_entity = func_get_arg(2);
    $extras = func_get_arg(3) ?? [];

    if (!empty($extras['date_occur_wizard_id'])) {
      $store_id = $extras['store_id'];
      $form_state->set('date_occur_wizard_id', $extras['date_occur_wizard_id']);
      $form_state->set('store_id', $store_id);
      $form_state->set('date_occur_occurrence', $extras['date_occur_occurrence']);
    }

    // This is almost identical code to RecurUpdate::store.
    // We could store the override ids?
    // We could make reusable methods - but that's a lot of variables needed
    // elsewhere.
    // STARTS.
    list($occur_field_id, $original_id) = explode(':', $store_id);
    list($entity_type, $occur_field_name) = explode('.', $occur_field_id);

    $storage = \Drupal::entityTypeManager()
      ->getStorage($new_entity->getEntityTypeId());
    $original_entity = $storage->load($original_id);
    $occur_field = \Drupal::entityTypeManager()
      ->getStorage('field_storage_config')
      ->load($occur_field_id);
    $date_field_name = $occur_field->getSetting('target_field');

    // Are there any overrides.
    $overrides = \Drupal::entityQuery($new_entity->getEntityTypeId())
      ->condition($occur_field_name . '.target_id', $original_id)
      ->accessCheck(FALSE)
      ->execute();
    $start_set = RlHelper::createInstance($new_entity->$date_field_name->rrule, $new_entity->$date_field_name->start_date->getPhpDateTime())->getRlRuleset();
    $end_set = RlHelper::createInstance($new_entity->$date_field_name->rrule, $new_entity->$date_field_name->end_date->getPhpDateTime())->getRlRuleset();

    $orphan_overrides = [];
    $valid_overrides = [];
    foreach ($storage->loadMultiple($overrides) as $override) {
      if ($override->$date_field_name->start_date >= $new_entity->$date_field_name->start_date) {
        if ($start_set->occursAt($override->$date_field_name->start_date->format('c'))
          && $end_set->occursAt($override->$date_field_name->end_date->format('c'))
        ) {
          $valid_overrides[] = $override;
        }
        else {
          $orphan_overrides[] = $override;
        }
      }
    }
    // ENDS.

    $form['#title'] = $this->t('The repeating date has some overrides');

    $form['description'] = [
      '#markup' => '<p>' . $this->t('The updated repeating date instance has overrides.') . '</p>',
    ];

    if (!empty($valid_overrides)) {
      $form['valid_title'] = [
        '#markup' => '<h2>' . $this->t('Valid overrides') . '</h2>',
      ];
      $form['valid_description'] = [
        '#markup' => '<p>' . $this->t('These overrides will continue to exist, but will not be updated with any changes to the original.') . '</p>',
      ];
      $valid_list = [
        '#theme' => 'item_list',
        '#items' => [],
      ];
      foreach ($valid_overrides as $override) {
        if ($override->access('view', $this->currentUser())) {
          $valid_list['#items'] = [
            'value' => [
              '#type' => 'link',
              '#title' => $override->label(),
              '#url' => $override->toUrl(),
            ],
          ];
        }
        else {
          // No idea why this might be the case, but the query and the
          // information should be given even if the user does not have access.
          $valid_list['#items'] = [
            'value' => [
              '#type' => 'link',
              '#title' => $this->t('id: %id', ['%id' => $override->id()]),
              '#url' => $override->toUrl(),
            ],
          ];
        }
      }
      $form['valid_list'] = $valid_list;
    }
    if (!empty($orphan_overrides)) {
      $form['orphan_title'] = [
        '#markup' => '<h2>' . $this->t('Orphaned overrides') . '</h2>',
      ];
      $form['orphan_description'] = [
        '#markup' => '<p>' . $this->t('These overrides will be removed as they no longer align with a date in the repeating sequence.') . '</p>',
      ];
      $orphan_list = [
        '#theme' => 'item_list',
        '#items' => [],
      ];
      foreach ($orphan_overrides as $override) {
        if ($override->access('view', $this->currentUser())) {
          $orphan_list['#items'] = [
            'value' => [
              '#type' => 'link',
              '#title' => $override->label(),
              '#url' => $override->toUrl(),
            ],
          ];
        }
        else {
          // No idea why this might be the case, but the query and the
          // information should be given even if the user does not have access.
          $orphan_list['#items'] = [
            'value' => [
              '#type' => 'link',
              '#title' => $this->t('id: %id', ['%id' => $override->id()]),
              '#url' => $override->toUrl(),
            ],
          ];
        }
      }
      $form['orphan_list'] = $orphan_list;
    }

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Confirm and save'),
      '#button_type' => 'primary',
      '#submit' => [[RecurUpdate::class, 'confirm']],
    ];

    $form['actions']['cancel'] = [ 
      '#type' => 'submit',
      '#value' => $this->t('Cancel'),
      '#button_type' => 'secondary',
      '#submit' => [[RecurUpdate::class, 'cancel']],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Submission handled in RecurUpdate to keep logic together.
  }

}
