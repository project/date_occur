<?php

namespace Drupal\date_occur_ui\Routing;

use Drupal\Core\Cache\CacheableRedirectResponse;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Listens for HttpKernel Request event.
 */
class RequestSubscriber implements EventSubscriberInterface {

  /**
   * Redirects if date_occur parameter has an instance entity.
   *
   * @todo is this the appropriate place to do this; or should it be later in
   * the controller events?
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The RequestEvent to process.
   */
  public function onKernelRequest(RequestEvent $event) {
    if ($event->isMainRequest()) {
      $attributes = $event->getRequest()->attributes;
      if ($date = $attributes->get('date_occur')) {
        $parameters = $attributes->get('_route_object')->getOption('parameters');
        // ParamConverter should already have checked this exists.
        $entity_param = $parameters['date_occur']['entity_param'];
        $parent_entity = $attributes->get($entity_param);
        $occur_entity = $date->getValue()['entity'];
        if ($parent_entity->id() != $occur_entity->id()) {
          $redirect = new CacheableRedirectResponse($occur_entity->toUrl()->toString());
          $redirect->addCacheableDependency($parent_entity);
          $redirect->addCacheableDependency($occur_entity);
          $event->setResponse($redirect);
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[KernelEvents::REQUEST] = ['onKernelRequest', 31];
    return $events;
  }

}
