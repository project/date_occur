<?php

namespace Drupal\date_occur_ui\Routing;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\field\FieldStorageConfigInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

class Routes {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs Routes class.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Return dynamic routes.
   *
   * @return \Symfony\Component\Routing\RouteCollection
   *   A route collection.
   */
  public function getRoutes() {
    $collection = new RouteCollection();

    $fields = $this->entityTypeManager
      ->getStorage('field_storage_config')
      ->loadByProperties(['type' => 'date_occur_parent']);

    foreach ($fields as $field) {
      assert($field instanceof FieldStorageConfigInterface);
      $entity_type_id = $field->getTargetEntityTypeId();
      $field_machine_name = $field->getName();
      $field_id = $field->id();

      $route = new Route(
        '/' . $entity_type_id . '/{' . $entity_type_id . '}/date_occur/' . $field_machine_name . '/{date_occur}/create',
        [
          '_controller' => 'date_occur_ui.create_form:getContentResult',
          // @todo _title_callback
        ],
        [
          '_entity_access' => $entity_type_id . '.update',
        ],
        [
          'parameters' => [
            $entity_type_id => ['type' => 'entity:' . $entity_type_id],
            'date_occur' => [
              'type' => 'date_occur:' . $field_id,
              'entity_param' => $entity_type_id,
            ],
          ],
          '_date_occur_operation_route' => TRUE,
        ],
      );
      $collection->add(
        'date_occur_ui.' . $field_id . '.create',
        $route
      );

      $route = new Route(
        '/' . $entity_type_id . '/{' . $entity_type_id . '}/date_occur/' . $field_machine_name . '/{date_occur}/edit-future',
        [
          '_controller' => 'date_occur_ui.recur_edit_form:editFutureForm',
          // @todo _title_callback
        ],
        [
          '_entity_access' => $entity_type_id . '.update',
        ],
        [
          'parameters' => [
            $entity_type_id => ['type' => 'entity:' . $entity_type_id],
            'date_occur' => [
              'type' => 'date_occur:' . $field_id,
              'entity_param' => $entity_type_id,
            ],
          ],
          '_date_occur_operation_route' => TRUE,
        ]
      );
      $collection->add(
        'date_occur_ui.' . $field_id . '.edit_future',
        $route
      );
    }

    return $collection;
  }

}
