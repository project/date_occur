<?php

namespace Drupal\date_occur_ui\Routing;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\ParamConverter\ParamConverterInterface;
use Drupal\Core\ParamConverter\ParamNotConvertedException;
use Drupal\Core\TypedData\TypedDataManagerInterface;
use Drupal\date_occur\OccurReferenceHelperInterface;
use Drupal\date_occur\Plugin\DataType\DateOccurrenceInterface;
use Drupal\date_recur\Rl\RlHelper;
use Symfony\Component\Routing\Route;

/**
 * Converts parameters for upcasting date occur id to data type.
 *
 * Needs the EntityConverter to have run on the entity beforehand.
 * @todo Worth working out handling when not?
 */
class ParamConverter implements ParamConverterInterface {

  /**
   * Occurrence reference helper service.
   *
   * @var \Drupal\date_occur\OccurReferenceHelperInterface
   */
  protected $occurReferenceHelper;

  /**
   * Typed data manager.
   *
   * @var \Drupal\Core\TypedData\TypedDataManagerInterface
   */
  protected $typedDataManager;

  /**
   * Constructs a new ParamConverter.
   *
   * @param \Drupal\date_occur\OccurReferenceHelperInterface $occur_reference_helper
   *   The occurrence reference helper service.
   * @param \Drupal\Core\TypedData\TypedDataManagerInterface $typed_data_manager
   *   The typed data manager.
   */
  public function __construct(OccurReferenceHelperInterface $occur_reference_helper, TypedDataManagerInterface $typed_data_manager) {
    $this->occurReferenceHelper = $occur_reference_helper;
    $this->typedDataManager = $typed_data_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function convert($value, $definition, $name, array $defaults) {
    $type_part = strstr($definition['type'], ':');
    if (!$type_part) {
      throw new ParamNotConvertedException(sprintf('The type definition "%s" is invalid. The expected format is "date_occur:<field_id>".', $definition['type']));
    }
    list($entity_type, $occur_field_name) = explode('.', substr($type_part, 1));
    if (!$occur_field_name) {
      throw new ParamNotConvertedException(sprintf('The type definition "%s" is invalid. The expected format is "date_occur:<field_id>". Where field_id is <entity_type>.<field_name>.', $definition['type']));
    }
    $entity_param = $definition['entity_param'];
    if (!$entity_param) {
      throw new ParamNotConvertedException('The entity_param is required.');
    }
    if (empty($defaults[$entity_param])) {
      // The entity parameter should be converted, upcast, first. Not ideal
      // relying on two parameters, and probably not intended, but works.
      // The weight of \Drupal\date_occur_ui\Routing\RouteSubscriber event makes
      // sure it adds this after
      // \Drupal\Core\EventSubscriber\EntityRouteAlterSubscriber.
      throw new ParamNotConvertedException(sprintf('The entity parameter "%s" has not yet been converted.', $entity_param));
    }

    $item_definition = $this->typedDataManager->createDataDefinition('date_occurrence');
    if (!empty($value)) {
      list($start_date, $end_date) = explode(DateOccurrenceInterface::INTERVAL_DESIGNATOR, $value);
      // Check it is a valid recurrence. This could be done by querying the
      // date_recur table, or calculated as here. Maybe could make
      // switchable. occursAt requires rlanvin/php-rrule.
      $recur_entity = $defaults[$entity_param];
      $date_field_name = $recur_entity->$occur_field_name->getSetting('target_field');
      // End date same period from start date.
      $original_start_datetime = $recur_entity->$date_field_name->start_date->getPhpDateTime();
      $original_interval = $original_start_datetime->diff($recur_entity->$date_field_name->end_date->getPhpDateTime());
      $argument_start_datetime = new DrupalDateTime($start_date);
      $argument_end_datetime = new DrupalDateTime($end_date);
      if ($argument_start_datetime->hasErrors() || $argument_end_datetime->hasErrors()) {
        return NULL;
      }
      if ($argument_start_datetime->add($original_interval) != $argument_end_datetime) {
        return NULL;
      }
      if (empty($recur_entity->$date_field_name->rrule)) {
        return NULL;
      }
      // Start is in sequence.
      $rset = RlHelper::createInstance($recur_entity->$date_field_name->rrule, $recur_entity->$date_field_name->start_date->getPhpDateTime())->getRlRuleset();
      if ($rset->occursAt($start_date)) {
        $occurrence = $this->typedDataManager->create($item_definition, [
          'start_date' => $start_date,
          'end_date' => $end_date,
          'entity' => $recur_entity,
          'field_delta' => 0,
        ]);
        // Update with occurrence if there is an instance.
        return $this->occurReferenceHelper->setOccurrenceInstance($occurrence, $date_field_name);
      }
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function applies($definition, $name, Route $route) {
    if (!empty($definition['type']) && strpos($definition['type'], 'date_occur:') === 0) {
      // @todo additional check the field?
      return TRUE;
    }
    return FALSE;
  }

}
