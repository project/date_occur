<?php

namespace Drupal\date_occur_ui\Routing;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteBuildEvent;
use Drupal\Core\Routing\RoutingEvents;
use Drupal\field\FieldStorageConfigInterface;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber implements EventSubscriberInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs Routes class.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    $fields = $this->entityTypeManager
      ->getStorage('field_storage_config')
      ->loadByProperties(['type' => 'date_occur_parent']);

    foreach ($fields as $field) {
      assert($field instanceof FieldStorageConfigInterface);
      $entity_type_id = $field->getTargetEntityTypeId();
      $field_machine_name = $field->getName();
      $field_id = $field->id();

      if ($route = $collection->get('entity.' . $entity_type_id . '.canonical')) {
        $path = $route->getPath();
        $date_route = clone $route;
        $date_route->setPath($path . '/date_occur/' . $field_machine_name . '/{date_occur}');
        $parameters = (array) $date_route->getOption('parameters');
        $parameters += [
          'date_occur' => [
            'type' => 'date_occur:' . $field_id,
            'entity_param' => $entity_type_id,
          ],
        ];
        $date_route->setOption('parameters', $parameters);
        $collection->add('date_occur_ui.' . $field_id . '.view', $date_route);
      }
      // For convenience and not to confuse UI duplicate edit form.
      if ($route = $collection->get('entity.' . $entity_type_id . '.edit_form')) {
        $path = $route->getPath();
        $date_route = clone $route;
        $date_route->setPath($path . '/date_occur/' . $field_machine_name . '/{date_occur}/edit-all');
        $parameters = (array) $date_route->getOption('parameters');
        $parameters += [
          'date_occur' => [
            'type' => 'date_occur:' . $field_id,
            'entity_param' => $entity_type_id,
          ],
        ];
        $date_route->setOption('parameters', $parameters);
        $collection->add('date_occur_ui.' . $field_id . '.edit_all', $date_route);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    // Core\EventSubscriber\EntityRouteAlterSubscriber -150.
    // As long as we can come after then don't need to worry about upcasting the
    // entity parameter.
    $events[RoutingEvents::ALTER] = ['onAlterRoutes', -170];
    return $events;
  }

  /**
   * Delegates the route altering to self::alterRoutes().
   *
   * @param \Drupal\Core\Routing\RouteBuildEvent $event
   *   The route build event.
   */
  public function onAlterRoutes(RouteBuildEvent $event) {
    $collection = $event->getRouteCollection();
    $this->alterRoutes($collection);
  }

}
