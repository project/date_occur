<?php

namespace Drupal\date_occur_ui;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\date_occur\Event\DateOccurInstanceEvents;
use Drupal\date_occur\Event\DateOccurParentValueEvent;
use Drupal\date_occur\OccurReferenceHelperInterface;
use Drupal\date_occur_sapi\OccurrencesSearchApiTrackerSubscriber as OriginalOccurrencesSearchApiTrackerSubscriber;
use Drupal\date_recur\Event\DateRecurValueEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Decorates \Drupal\date_occur_sapi\OccurrencesSearchApiTrackerSubscriber.
 *
 * Adds functionality to replace recurrences with occurrence instances.
 */
class OccurrencesSearchApiTrackerSubscriber implements EventSubscriberInterface {

  /**
   * The decorated Tracker Subscriber.
   *
   * @var \Drupal\date_occur_sapi\OccurrencesSearchApiTrackerSubscriber
   */
  protected OriginalOccurrencesSearchApiTrackerSubscriber $tracker;

  /**
   * The occurrence reference helper.
   *
   * @var \Drupal\date_occur\OccurReferenceHelperInterface
   */
  protected OccurReferenceHelperInterface $referenceHelper;

  /**
   * Entity Field Manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * Decorator constructor.
   */
  public function __construct(OriginalOccurrencesSearchApiTrackerSubscriber $tracker, OccurReferenceHelperInterface $reference_helper, EntityFieldManagerInterface $entity_field_manager) {
    $this->tracker = $tracker;
    $this->referenceHelper = $reference_helper;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * Respond to a field value insertion or update.
   *
   * @param \Drupal\date_recur\Event\DateRecurValueEvent $event
   *   The date recur event.
   */
  public function onSave(DateRecurValueEvent $event): void {
    if (!$this->isOverridenRecurrenceField($event)) {
      $this->tracker->onSave($event);
    }
  }

  /**
   * Respond to a entity deletion.
   *
   * @param \Drupal\date_recur\Event\DateRecurValueEvent $event
   *   The date recur event.
   */
  public function onEntityDelete(DateRecurValueEvent $event): void {
    if (!$this->isOverridenRecurrenceField($event)) {
      $this->tracker->onEntityDelete($event);
    }
  }

  /**
   * Respond to an instance insertion, update, or delete.
   *
   * The occurrence should exist from the date_recur recurrence. Even removal
   * should be precipitated by removal from the rule, otherwise it is just the
   * exception being removed, and returning to the parent.
   *
   * @todo should this also check that the occurrence is already indexed? Is it
   * easier to debug if it gets indexed when something went wrong elsewhere or
   * not?
   *
   * @todo order of changes, particularly of deletion. Can we ensure the parent
   * gets removed after the instance.
   *
   * @param \Drupal\date_occur\Event\DateOccurParentValueEvent $event
   *   The instance date event.
   */
  public function onInstanceEvent(DateOccurParentValueEvent $event): void {
    $instance_parent_field = $event->getField();
    if (empty($instance_parent_field->first())) {
      return;
    }

    $instance_parent_storage_settings = $instance_parent_field->getFieldDefinition()->getFieldStorageDefinition()->getSettings();
    $recur_field_storage_definition = $this->entityFieldManager->getFieldStorageDefinitions($instance_parent_storage_settings['target_type'])[$instance_parent_storage_settings['target_field']];
    $indexes = $this->tracker->getIndexesForFieldStorageDefinition($recur_field_storage_definition);

    if (empty($indexes)) {
      return;
    }

    $datasource_plugin_id = $this->tracker->getFullyQualifiedDatasourcePluginId($recur_field_storage_definition);
    foreach ($indexes as $index) {
      $occurrence = $instance_parent_field->first()->getOccurrence();
      $occurence_identifier = sprintf('%s:%s:%s', $occurrence->getParent()->id(), 0, $occurrence->getRangeId());
      $index->trackItemsUpdated($datasource_plugin_id, [$occurence_identifier]);
    }
  }

  /**
   * Is overridden date instance.
   *
   * Is this an occurrence instance.
   * Does the occurrence instance re-use the Recurring Date field for the
   * instance date.
   *
   * @param \Drupal\date_recur\Event\DateRecurValueEvent $event
   *   The date recur event.
   *
   * @return bool
   *   TRUE if this is an overriden reused date field.
   */
  protected function isOverridenRecurrenceField(DateRecurValueEvent $event) {
    $recur_field = $event->getField();
    $entity = $recur_field->getEntity();

    // Does the recur_field have an instance_reference_field?
    // Is this instance_reference_field on the same entity type?
    // Does the instance_reference_field have a value?
    if ($occur_field_id = $this->referenceHelper->getInstanceReferenceFieldForRecurField($recur_field->getFieldDefinition()->getFieldStorageDefinition())) {
      list($occur_entity_type, $occur_field_name) = explode('.', $occur_field_id);
      if (
        ($occur_entity_type == $entity->getEntityTypeId()) &&
        (!$entity->get($occur_field_name)->isEmpty())
      ) {
        // A reused entity type for both recurrence and occurrence.
        // Is the date field for the occurrence also the recurrence field?
        $occurrence_date_field = $entity->get($occur_field_name)->getSetting('target_field');
        if ($occurrence_date_field == $recur_field->getName()) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = OriginalOccurrencesSearchApiTrackerSubscriber::getSubscribedEvents();
    $events[DateOccurInstanceEvents::PARENT_FIELD_VALUE_SAVE] = ['onInstanceEvent'];
    $events[DateOccurInstanceEvents::PARENT_FIELD_ENTITY_DELETE] = ['onInstanceEvent'];

    return $events;
  }

}
