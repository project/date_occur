# Recurring Date Occurrences

Recurring Date Field Occurrences (`date_occur`) module and related modules 
(`date_occur_*`) that depend on it extend the functionality of Recurring Date
Field (`date_recur`) for occurrences in the recurring date.

## Recurring Date Occurrences Example (`date_occur_example`)

Probably the place to start. This provides an example Event node configured
to ...

This is not the only way to use the Recurring Date Occurrences Field, but is
how Recurring Date Occurrences UI was designed.

## Recurring Date Occurrences Field (`date_occur`)

Provides a field to reference a single Occurrence in a parent Recurring Date
Field. Intended to be used to override specific occurrences.

## Recurring Date Occurrences UI

An implementation using the Recurring Date Field Occcurrences field. This used
the same entity as the Recurring Date Field is on to be able to create
Occerrence Instances to override all values of the original Recurrence.

The module provides a route to view occurrences and a formatter for Recurring
Date field to show the date for that occurrence. Rather than the showing the
date from 'now' as the default foratter does. If the occurrence is overridden
it will redirect to the the occurrence instance.
The module also provides a formatter for the Recurring Date Occurrencs field
that will display information about the parent Recurring Date field, its
repeat rule and upcoming occurrences.

## Recurring Date Occurrences Search API

To provide lists of Occurrences, including any overriden Occurrence Instances
a Search API datasource is provided.

## Naming

The `date_occur` prefix is a contraction of `date_recur_occurrences` for
readable, but not too long final 'namespaced' strings. In line with Recurring
Date Field naming protocol the modules become Recurring Date Occurrences.

An `instance` is a specific Recurring Date field occurrence that has been
overriden.
