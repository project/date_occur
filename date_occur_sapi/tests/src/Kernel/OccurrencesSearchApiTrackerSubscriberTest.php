<?php

namespace Drupal\Tests\date_occur_sapi\Kernel;

use Prophecy\PhpUnit\ProphecyTrait;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * @coversDefaultClass \Drupal\date_occur_sapi\OccurrencesSearchApiTrackerSubscriber
 */
class OccurrencesSearchApiTrackerSubscriberTest extends DateOccurSapiKernelTestBase {

  use ProphecyTrait;
  /**
   * A mock tracker.
   *
   * @var \Drupal\search_api\Tracker\TrackerInterface
   */
  protected $mockTracker;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $index = $this->createTestIndex();
    $this->mockTracker = new LoggingTrackerDecorator([], 'default', [], $index->getTrackerInstance());
    $index->setTracker($this->mockTracker);

    $index_storage = $this->prophesize(EntityStorageInterface::class);
    $index_storage->loadMultiple()->willReturn([$index]);

    // Ensure the loadMultiple call to the SAPI index storage will return the
    // version with the mocked tracker.
    $entity_type_manager = $this->container->get('entity_type.manager');
    $manager_reflection = new \ReflectionObject($entity_type_manager);
    $handlers = $manager_reflection->getProperty('handlers');
    $handlers->setAccessible(TRUE);
    $handlers->setValue($entity_type_manager, [
      'storage' => [
        'search_api_index' => $index_storage->reveal(),
      ],
    ]);
  }

  /**
   * @covers ::onSave
   * @covers ::onEntityDelete
   */
  public function testOnSaveFullEntityLifecycle() {
    // Creating the entity will result in a series of initial inserts into the
    // tracker.
    $entity = $this->createTestEntity();
    $this->assertTrackerState([
      'trackItemsInserted',
      [
        'date_occur:entity_test__date_recur_field/1:0:2014-06-15T23:00:00Z--2014-06-16T07:00:00Z',
        'date_occur:entity_test__date_recur_field/1:0:2014-06-16T23:00:00Z--2014-06-17T07:00:00Z',
        'date_occur:entity_test__date_recur_field/1:0:2014-06-17T23:00:00Z--2014-06-18T07:00:00Z',
        'date_occur:entity_test__date_recur_field/1:1:2011-12-14T23:00:00Z--2011-12-14T23:30:00Z',
        'date_occur:entity_test__date_recur_field/1:1:2011-12-15T23:00:00Z--2011-12-15T23:30:00Z',
        'date_occur:entity_test__date_recur_field/1:1:2011-12-20T23:00:00Z--2011-12-20T23:30:00Z',
        'date_occur:entity_test__date_recur_field/1:1:2011-12-21T23:00:00Z--2011-12-21T23:30:00Z',
        'date_occur:entity_test__date_recur_field/1:1:2011-12-22T23:00:00Z--2011-12-22T23:30:00Z',
      ],
    ]);

    // Increase the count of the rrule for the second field item from 5 to 10,
    // this will result in a series of inserts and updates into the tracker.
    $entity->date_recur_field[1]->rrule = 'FREQ=WEEKLY;BYDAY=WE,TH,FR;COUNT=10';
    $entity->save();
    $this->assertTrackerState([
      [
        'trackItemsInserted',
        [
          'date_occur:entity_test__date_recur_field/1:1:2011-12-27T23:00:00Z--2011-12-27T23:30:00Z',
          'date_occur:entity_test__date_recur_field/1:1:2011-12-28T23:00:00Z--2011-12-28T23:30:00Z',
          'date_occur:entity_test__date_recur_field/1:1:2011-12-29T23:00:00Z--2011-12-29T23:30:00Z',
          'date_occur:entity_test__date_recur_field/1:1:2012-01-03T23:00:00Z--2012-01-03T23:30:00Z',
          'date_occur:entity_test__date_recur_field/1:1:2012-01-04T23:00:00Z--2012-01-04T23:30:00Z',
        ],
      ],
      [
        'trackItemsUpdated',
        [
          'date_occur:entity_test__date_recur_field/1:0:2014-06-15T23:00:00Z--2014-06-16T07:00:00Z',
          'date_occur:entity_test__date_recur_field/1:0:2014-06-16T23:00:00Z--2014-06-17T07:00:00Z',
          'date_occur:entity_test__date_recur_field/1:0:2014-06-17T23:00:00Z--2014-06-18T07:00:00Z',
          'date_occur:entity_test__date_recur_field/1:1:2011-12-14T23:00:00Z--2011-12-14T23:30:00Z',
          'date_occur:entity_test__date_recur_field/1:1:2011-12-15T23:00:00Z--2011-12-15T23:30:00Z',
          'date_occur:entity_test__date_recur_field/1:1:2011-12-20T23:00:00Z--2011-12-20T23:30:00Z',
          'date_occur:entity_test__date_recur_field/1:1:2011-12-21T23:00:00Z--2011-12-21T23:30:00Z',
          'date_occur:entity_test__date_recur_field/1:1:2011-12-22T23:00:00Z--2011-12-22T23:30:00Z',
        ],
      ],
    ]);

    // Remove the first field item, which will result in a series of deletions
    // and insertions. These are tracked as insertaions and not updates, since
    // the ID of the recurrences for delta 1 are rekeyed to delta 0.
    $entity->date_recur_field->removeItem(0);
    $entity->save();
    $this->assertTrackerState([
      [
        'trackItemsDeleted',
        [
          'date_occur:entity_test__date_recur_field/1:0:2014-06-15T23:00:00Z--2014-06-16T07:00:00Z',
          'date_occur:entity_test__date_recur_field/1:0:2014-06-16T23:00:00Z--2014-06-17T07:00:00Z',
          'date_occur:entity_test__date_recur_field/1:0:2014-06-17T23:00:00Z--2014-06-18T07:00:00Z',
          'date_occur:entity_test__date_recur_field/1:1:2011-12-14T23:00:00Z--2011-12-14T23:30:00Z',
          'date_occur:entity_test__date_recur_field/1:1:2011-12-15T23:00:00Z--2011-12-15T23:30:00Z',
          'date_occur:entity_test__date_recur_field/1:1:2011-12-20T23:00:00Z--2011-12-20T23:30:00Z',
          'date_occur:entity_test__date_recur_field/1:1:2011-12-21T23:00:00Z--2011-12-21T23:30:00Z',
          'date_occur:entity_test__date_recur_field/1:1:2011-12-22T23:00:00Z--2011-12-22T23:30:00Z',
          'date_occur:entity_test__date_recur_field/1:1:2011-12-27T23:00:00Z--2011-12-27T23:30:00Z',
          'date_occur:entity_test__date_recur_field/1:1:2011-12-28T23:00:00Z--2011-12-28T23:30:00Z',
          'date_occur:entity_test__date_recur_field/1:1:2011-12-29T23:00:00Z--2011-12-29T23:30:00Z',
          'date_occur:entity_test__date_recur_field/1:1:2012-01-03T23:00:00Z--2012-01-03T23:30:00Z',
          'date_occur:entity_test__date_recur_field/1:1:2012-01-04T23:00:00Z--2012-01-04T23:30:00Z',
        ],
      ],
      [
        'trackItemsInserted',
        [
          'date_occur:entity_test__date_recur_field/1:0:2011-12-14T23:00:00Z--2011-12-14T23:30:00Z',
          'date_occur:entity_test__date_recur_field/1:0:2011-12-15T23:00:00Z--2011-12-15T23:30:00Z',
          'date_occur:entity_test__date_recur_field/1:0:2011-12-20T23:00:00Z--2011-12-20T23:30:00Z',
          'date_occur:entity_test__date_recur_field/1:0:2011-12-21T23:00:00Z--2011-12-21T23:30:00Z',
          'date_occur:entity_test__date_recur_field/1:0:2011-12-22T23:00:00Z--2011-12-22T23:30:00Z',
          'date_occur:entity_test__date_recur_field/1:0:2011-12-27T23:00:00Z--2011-12-27T23:30:00Z',
          'date_occur:entity_test__date_recur_field/1:0:2011-12-28T23:00:00Z--2011-12-28T23:30:00Z',
          'date_occur:entity_test__date_recur_field/1:0:2011-12-29T23:00:00Z--2011-12-29T23:30:00Z',
          'date_occur:entity_test__date_recur_field/1:0:2012-01-03T23:00:00Z--2012-01-03T23:30:00Z',
          'date_occur:entity_test__date_recur_field/1:0:2012-01-04T23:00:00Z--2012-01-04T23:30:00Z',
        ],
      ],
    ]);

    // Delete the entity, which will result in a delete for the remaining items.
    $entity->delete();
    $this->assertTrackerState([
      'trackItemsDeleted',
      [
        'date_occur:entity_test__date_recur_field/1:0:2011-12-14T23:00:00Z--2011-12-14T23:30:00Z',
        'date_occur:entity_test__date_recur_field/1:0:2011-12-15T23:00:00Z--2011-12-15T23:30:00Z',
        'date_occur:entity_test__date_recur_field/1:0:2011-12-20T23:00:00Z--2011-12-20T23:30:00Z',
        'date_occur:entity_test__date_recur_field/1:0:2011-12-21T23:00:00Z--2011-12-21T23:30:00Z',
        'date_occur:entity_test__date_recur_field/1:0:2011-12-22T23:00:00Z--2011-12-22T23:30:00Z',
        'date_occur:entity_test__date_recur_field/1:0:2011-12-27T23:00:00Z--2011-12-27T23:30:00Z',
        'date_occur:entity_test__date_recur_field/1:0:2011-12-28T23:00:00Z--2011-12-28T23:30:00Z',
        'date_occur:entity_test__date_recur_field/1:0:2011-12-29T23:00:00Z--2011-12-29T23:30:00Z',
        'date_occur:entity_test__date_recur_field/1:0:2012-01-03T23:00:00Z--2012-01-03T23:30:00Z',
        'date_occur:entity_test__date_recur_field/1:0:2012-01-04T23:00:00Z--2012-01-04T23:30:00Z',
      ],
    ]);
  }

  /**
   * Assert calls expected to the tracker and reset it's internal state.
   */
  protected function assertTrackerState(array $expectedCalls): void {
    // Unroll the format of expected calls in the case where there is only one
    // call to simplify the assertion format.
    $actual_calls = count($this->mockTracker->calls) === 1 ? $this->mockTracker->calls[0] : $this->mockTracker->calls;
    try {
      $this->assertEquals($expectedCalls, $actual_calls);
    }
    catch (\Exception $e) {
      $this->fail(sprintf("Calls to the tracker did not match the expected calls, \n\n %s \n\n actual calls were: \n\n %s", var_export($expectedCalls, TRUE), var_export($actual_calls, TRUE)));
    }
    $this->mockTracker->reset();
  }

}
