<?php

namespace Drupal\Tests\date_occur_sapi\Kernel;

use Drupal\entity_test\Entity\EntityTest;

/**
 * @coversDefaultClass \Drupal\date_occur_sapi\Plugin\search_api\datasource\DateOccur
 * @covers \Drupal\date_occur_sapi\Plugin\search_api\datasource\DateOccurDeriver
 * @covers \Drupal\date_occur_sapi\Plugin\DataType\DateRecurOccurrence
 * @covers \Drupal\date_occur_sapi\TypedData\DateRecurOccurrenceDefinition
 */
class DateOccurDatasourceTest extends DateOccurSapiKernelTestBase {

  /**
   * The test datasource.
   *
   * @var \Drupal\search_api\Datasource\DatasourceInterface
   */
  protected $datasource;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->datasource = $this->createTestIndex()->getDatasource('date_occur:entity_test__date_recur_field');
  }

  /**
   * @covers ::getItemIds
   */
  public function testGetItemIdsAll() {
    $this->createTestEntity();
    $occurrence_ids = $this->datasource->getItemIds();
    $this->assertEquals([
      '1:0:2014-06-15T23:00:00Z--2014-06-16T07:00:00Z',
      '1:0:2014-06-16T23:00:00Z--2014-06-17T07:00:00Z',
      '1:0:2014-06-17T23:00:00Z--2014-06-18T07:00:00Z',
      '1:1:2011-12-14T23:00:00Z--2011-12-14T23:30:00Z',
      '1:1:2011-12-15T23:00:00Z--2011-12-15T23:30:00Z',
      '1:1:2011-12-20T23:00:00Z--2011-12-20T23:30:00Z',
      '1:1:2011-12-21T23:00:00Z--2011-12-21T23:30:00Z',
      '1:1:2011-12-22T23:00:00Z--2011-12-22T23:30:00Z',
    ], $occurrence_ids);
  }

  /**
   * @covers ::getItemIds
   */
  public function testGetItemIdsPaged() {
    array_map(function () {
      return $this->createTestEntity();
    }, range(0, 6));

    $pages = [
      [
        '1:0:2014-06-15T23:00:00Z--2014-06-16T07:00:00Z',
        '1:0:2014-06-16T23:00:00Z--2014-06-17T07:00:00Z',
        '1:0:2014-06-17T23:00:00Z--2014-06-18T07:00:00Z',
        '1:1:2011-12-14T23:00:00Z--2011-12-14T23:30:00Z',
        '1:1:2011-12-15T23:00:00Z--2011-12-15T23:30:00Z',
        '1:1:2011-12-20T23:00:00Z--2011-12-20T23:30:00Z',
        '1:1:2011-12-21T23:00:00Z--2011-12-21T23:30:00Z',
        '1:1:2011-12-22T23:00:00Z--2011-12-22T23:30:00Z',
        '2:0:2014-06-15T23:00:00Z--2014-06-16T07:00:00Z',
        '2:0:2014-06-16T23:00:00Z--2014-06-17T07:00:00Z',
        '2:0:2014-06-17T23:00:00Z--2014-06-18T07:00:00Z',
        '2:1:2011-12-14T23:00:00Z--2011-12-14T23:30:00Z',
        '2:1:2011-12-15T23:00:00Z--2011-12-15T23:30:00Z',
        '2:1:2011-12-20T23:00:00Z--2011-12-20T23:30:00Z',
        '2:1:2011-12-21T23:00:00Z--2011-12-21T23:30:00Z',
        '2:1:2011-12-22T23:00:00Z--2011-12-22T23:30:00Z',
        '3:0:2014-06-15T23:00:00Z--2014-06-16T07:00:00Z',
        '3:0:2014-06-16T23:00:00Z--2014-06-17T07:00:00Z',
        '3:0:2014-06-17T23:00:00Z--2014-06-18T07:00:00Z',
        '3:1:2011-12-14T23:00:00Z--2011-12-14T23:30:00Z',
        '3:1:2011-12-15T23:00:00Z--2011-12-15T23:30:00Z',
        '3:1:2011-12-20T23:00:00Z--2011-12-20T23:30:00Z',
        '3:1:2011-12-21T23:00:00Z--2011-12-21T23:30:00Z',
        '3:1:2011-12-22T23:00:00Z--2011-12-22T23:30:00Z',
        '4:0:2014-06-15T23:00:00Z--2014-06-16T07:00:00Z',
        '4:0:2014-06-16T23:00:00Z--2014-06-17T07:00:00Z',
        '4:0:2014-06-17T23:00:00Z--2014-06-18T07:00:00Z',
        '4:1:2011-12-14T23:00:00Z--2011-12-14T23:30:00Z',
        '4:1:2011-12-15T23:00:00Z--2011-12-15T23:30:00Z',
        '4:1:2011-12-20T23:00:00Z--2011-12-20T23:30:00Z',
        '4:1:2011-12-21T23:00:00Z--2011-12-21T23:30:00Z',
        '4:1:2011-12-22T23:00:00Z--2011-12-22T23:30:00Z',
        '5:0:2014-06-15T23:00:00Z--2014-06-16T07:00:00Z',
        '5:0:2014-06-16T23:00:00Z--2014-06-17T07:00:00Z',
        '5:0:2014-06-17T23:00:00Z--2014-06-18T07:00:00Z',
        '5:1:2011-12-14T23:00:00Z--2011-12-14T23:30:00Z',
        '5:1:2011-12-15T23:00:00Z--2011-12-15T23:30:00Z',
        '5:1:2011-12-20T23:00:00Z--2011-12-20T23:30:00Z',
        '5:1:2011-12-21T23:00:00Z--2011-12-21T23:30:00Z',
        '5:1:2011-12-22T23:00:00Z--2011-12-22T23:30:00Z',
      ],
      [
        '6:0:2014-06-15T23:00:00Z--2014-06-16T07:00:00Z',
        '6:0:2014-06-16T23:00:00Z--2014-06-17T07:00:00Z',
        '6:0:2014-06-17T23:00:00Z--2014-06-18T07:00:00Z',
        '6:1:2011-12-14T23:00:00Z--2011-12-14T23:30:00Z',
        '6:1:2011-12-15T23:00:00Z--2011-12-15T23:30:00Z',
        '6:1:2011-12-20T23:00:00Z--2011-12-20T23:30:00Z',
        '6:1:2011-12-21T23:00:00Z--2011-12-21T23:30:00Z',
        '6:1:2011-12-22T23:00:00Z--2011-12-22T23:30:00Z',
        '7:0:2014-06-15T23:00:00Z--2014-06-16T07:00:00Z',
        '7:0:2014-06-16T23:00:00Z--2014-06-17T07:00:00Z',
        '7:0:2014-06-17T23:00:00Z--2014-06-18T07:00:00Z',
        '7:1:2011-12-14T23:00:00Z--2011-12-14T23:30:00Z',
        '7:1:2011-12-15T23:00:00Z--2011-12-15T23:30:00Z',
        '7:1:2011-12-20T23:00:00Z--2011-12-20T23:30:00Z',
        '7:1:2011-12-21T23:00:00Z--2011-12-21T23:30:00Z',
        '7:1:2011-12-22T23:00:00Z--2011-12-22T23:30:00Z',
      ],
    ];

    foreach ($pages as $page_id => $expected_ids) {
      $occurrence_ids = $this->datasource->getItemIds($page_id);
      $this->assertEquals($expected_ids, $occurrence_ids);
    }
  }

  /**
   * @covers ::getItemIds
   */
  public function testGetItemIdsNoEntity() {
    $this->assertNull($this->datasource->getItemIds());
  }

  /**
   * @covers ::loadMultiple
   */
  public function testLoadMultiple() {
    $item_definition = \Drupal::typedDataManager()->createDataDefinition('date_occurrence');
    $entity = $this->createTestEntity();

    $expected_items = [
      '1:0:2014-06-15T23:00:00Z--2014-06-16T07:00:00Z' => \Drupal::typedDataManager()
        ->create($item_definition, [
          'start_date' => '2014-06-15T23:00:00+00:00',
          'end_date' => '2014-06-16T07:00:00+00:00',
          'entity' => EntityTest::load($entity->id()),
          'field_delta' => 0,
        ]),
      '1:1:2011-12-14T23:00:00Z--2011-12-14T23:30:00Z' => \Drupal::typedDataManager()
        ->create($item_definition, [
          'start_date' => '2011-12-14T23:00:00+00:00',
          'end_date' => '2011-12-14T23:30:00+00:00',
          'entity' => EntityTest::load($entity->id()),
          'field_delta' => 1,
        ]),
    ];

    $items = $this->datasource->loadMultiple(array_keys($expected_items));
    $this->assertEquals($expected_items, $items);
  }

  /**
   * @covers ::getItemId
   */
  public function testGetItemId1() {
    $item_definition = \Drupal::typedDataManager()->createDataDefinition('date_occurrence');
    $entity = $this->createTestEntity();
    $item = \Drupal::typedDataManager()->create($item_definition, [
      'start_date' => '2011-12-14T23:00:00+00:00',
      'end_date' => '2011-12-14T23:30:00+00:00',
      'entity' => $entity,
      'field_delta' => 1,
    ]);
    $this->assertSame('1:1:2011-12-14T23:00:00Z--2011-12-14T23:30:00Z', $this->datasource->getItemId($item));
  }

}
