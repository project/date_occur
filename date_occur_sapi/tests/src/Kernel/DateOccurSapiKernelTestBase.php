<?php

namespace Drupal\Tests\date_occur_sapi\Kernel;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\date_recur\Plugin\Field\FieldType\DateRecurItem;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\field\FieldConfigInterface;
use Drupal\field\FieldStorageConfigInterface;
use Drupal\KernelTests\KernelTestBase;
use Drupal\search_api\Entity\Index;
use Drupal\search_api\Entity\Server;
use Drupal\search_api\IndexInterface;

/**
 * A base class for Kernel tests.
 */
class DateOccurSapiKernelTestBase extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'entity_test',
    'search_api',
    'search_api_db',
    'datetime',
    'datetime_range',
    'date_recur',
    'date_occur',
    'date_occur_sapi',
    'field',
    'user',
  ];

  /**
   * Recurring date field storge.
   *
   * @var \Drupal\field\FieldStorageConfigInterface
   */
  protected FieldStorageConfigInterface $recurFieldStorage;

  /**
   * Recurring date field configuration.
   *
   * @var \Drupal\field\FieldConfigInterface
   */
  protected FieldConfigInterface $recurFieldConfig;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('entity_test');
    $this->installEntitySchema('search_api_task');
    $this->installSchema('search_api', ['search_api_item']);
    $this->installEntitySchema('user');
    $this->installConfig('date_recur');

    // Add a date recur field to test entity.
    $this->recurFieldStorage = FieldStorageConfig::create([
      'entity_type' => 'entity_test',
      'field_name' => 'date_recur_field',
      'type' => 'date_recur',
      'cardinality' => FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED,
      'settings' => [
        'datetime_type' => DateRecurItem::DATETIME_TYPE_DATETIME,
      ],
    ]);
    $this->recurFieldStorage->save();

    $field = [
      'field_name' => 'date_recur_field',
      'entity_type' => 'entity_test',
      'bundle' => 'entity_test',
    ];
    $this->recurFieldConfig = FieldConfig::create($field);
    $this->recurFieldConfig->save();
  }

  /**
   * Get a test entity with some rules.
   */
  protected function createTestEntity(): EntityTest {
    $entity = EntityTest::create([
      'date_recur_field' => [
        [
          'value' => '2014-06-15T23:00:00',
          'end_value' => '2014-06-16T07:00:00',
          'rrule' => 'FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR;COUNT=3',
          'infinite' => '0',
          'timezone' => 'Australia/Sydney',
        ],
        [
          'value' => '2011-12-14T23:00:00',
          'end_value' => '2011-12-14T23:30:00',
          'rrule' => 'FREQ=WEEKLY;BYDAY=WE,TH,FR;COUNT=5',
          'infinite' => '0',
          'timezone' => 'Australia/Sydney',
        ],
      ],
    ]);
    $entity->save();
    return $entity;
  }

  /**
   * Create a test index.
   */
  protected function createTestIndex(): IndexInterface {
    $server = Server::create([
      'id' => 'test_server',
      'name' => 'Test server',
      'status' => TRUE,
      'backend' => 'search_api_db',
      'backend_config' => [
        'min_chars' => 3,
        'database' => 'default:default',
      ],
    ]);
    $server->save();
    $index = Index::create([
      'name' => 'Test Index',
      'id' => 'test_index',
      'status' => TRUE,
      'server' => 'test_server',
      'datasource_settings' => [
        'date_occur:entity_test__date_recur_field' => [],
      ],
      'tracker_settings' => [
        'default' => [],
      ],
    ]);
    $index->save();
    return $index;
  }

}
