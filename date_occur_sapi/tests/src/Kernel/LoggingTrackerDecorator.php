<?php

namespace Drupal\Tests\date_occur_sapi\Kernel;

use Drupal\search_api\Tracker\TrackerInterface;
use Drupal\search_api\Tracker\TrackerPluginBase;

/**
 * A tracker decorator that logs calls.
 */
class LoggingTrackerDecorator extends TrackerPluginBase implements TrackerInterface {

  /**
   * The original tracker.
   *
   * @var \Drupal\search_api\Tracker\TrackerInterface
   */
  protected $original;

  /**
   * A list of calls to the mock tracker.
   *
   * @var array
   */
  public $calls = [];

  /**
   * LoggingTrackerDecorator constructor.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, TrackerInterface $originalTracker) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->original = $originalTracker;
  }

  /**
   * Reset calls on the mock.
   */
  public function reset(): void {
    $this->calls = [];
  }

  /**
   * {@inheritdoc}
   */
  public function __call($name, $arguments) {
    $this->calls[] = [$name, count($arguments) === 1 ? $arguments[0] : $arguments];
    return call_user_func_array([$this->original, $name], $arguments);
  }

  /**
   * {@inheritdoc}
   */
  public function trackItemsInserted(array $ids) {
    return static::__call(__FUNCTION__, func_get_args());
  }

  /**
   * {@inheritdoc}
   */
  public function trackItemsUpdated(array $ids) {
    return static::__call(__FUNCTION__, func_get_args());
  }

  /**
   * {@inheritdoc}
   */
  public function trackAllItemsUpdated($datasource_id = NULL) {
    return static::__call(__FUNCTION__, func_get_args());
  }

  /**
   * {@inheritdoc}
   */
  public function trackItemsIndexed(array $ids) {
    return static::__call(__FUNCTION__, func_get_args());
  }

  /**
   * {@inheritdoc}
   */
  public function trackItemsDeleted(array $ids = NULL) {
    return static::__call(__FUNCTION__, func_get_args());
  }

  /**
   * {@inheritdoc}
   */
  public function trackAllItemsDeleted($datasource_id = NULL) {
    return static::__call(__FUNCTION__, func_get_args());
  }

  /**
   * {@inheritdoc}
   */
  public function getRemainingItems($limit = -1, $datasource_id = NULL) {
    return static::__call(__FUNCTION__, func_get_args());
  }

  /**
   * {@inheritdoc}
   */
  public function getTotalItemsCount($datasource_id = NULL) {
    return static::__call(__FUNCTION__, func_get_args());
  }

  /**
   * {@inheritdoc}
   */
  public function getIndexedItemsCount($datasource_id = NULL) {
    return static::__call(__FUNCTION__, func_get_args());
  }

  /**
   * {@inheritdoc}
   */
  public function getRemainingItemsCount($datasource_id = NULL) {
    return static::__call(__FUNCTION__, func_get_args());
  }

}
