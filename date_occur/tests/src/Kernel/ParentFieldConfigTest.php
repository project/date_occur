<?php

namespace Drupal\Tests\date_occur\Kernel;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\date_recur\Plugin\Field\FieldType\DateRecurItem;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\KernelTests\KernelTestBase;

/**
 * Instance reference field configuration test.
 *
 * @group date_occur
 */
class ParentFieldConfigTest extends KernelTestBase {

  /**
   * Entity Field Manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface|null
   */
  protected ?EntityFieldManagerInterface $entityFieldManager;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'date_occur',
    'entity_test',
    'datetime',
    'datetime_range',
    'date_recur',
    'field',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('entity_test');
    $this->installConfig('date_recur');
    $this->installConfig('date_occur');
    $this->entityFieldManager = \Drupal::service('entity_field.manager');

    // Add a date recur field to test entity.
    $fieldStorage = FieldStorageConfig::create([
      'entity_type' => 'entity_test',
      'field_name' => 'date_recur_field',
      'type' => 'date_recur',
      'settings' => [
        'datetime_type' => DateRecurItem::DATETIME_TYPE_DATETIME,
      ],
    ]);
    $fieldStorage->save();

    $field = [
      'field_name' => 'date_recur_field',
      'entity_type' => 'entity_test',
      'bundle' => 'entity_test',
    ];
    $this->fieldConfig = FieldConfig::create($field);
    $this->fieldConfig->save();
  }

  public function testFieldConfig() {
    // Add a recurrence parent reference field to the test entity.
    $field_storage = FieldStorageConfig::create([
      'entity_type' => 'entity_test',
      'field_name' => 'occur_parent_reference',
      'type' => 'date_occur_parent',
      'settings' => [
        'target_type' => 'entity_test',
        'target_field' => 'date_recur_field',
      ],
    ]);
    $field_storage->save();

    $field = [
      'field_name' => 'occur_parent_reference',
      'entity_type' => 'entity_test',
      'bundle' => 'entity_test',
    ];
    $field_config = FieldConfig::create($field);
    $field_config->save();

    // Ensure recurrence target date recur field correct.
    $reference_field = FieldConfig::loadByName('entity_test', 'entity_test', 'occur_parent_reference');
    $this->assertEquals('date_recur_field', $reference_field->getSetting('target_field'));
    $this->assertEquals('entity_test', $reference_field->getSetting('target_type'));

    // Ensure target date recur field has recurrence field third party setting.
    $target_field = FieldStorageConfig::load('entity_test.date_recur_field');
    $this->assertEquals('entity_test.occur_parent_reference', $target_field->getThirdPartySetting('date_occur', 'instance_reference_field'));

    // Add a second field to reference.
    FieldStorageConfig::create([
      'entity_type' => 'entity_test',
      'field_name' => 'second_recur_field',
      'type' => 'date_recur',
      'settings' => [
        'datetime_type' => DateRecurItem::DATETIME_TYPE_DATETIME,
      ],
    ])->save();
    FieldConfig::create([
      'field_name' => 'second_recur_field',
      'entity_type' => 'entity_test',
      'bundle' => 'entity_test',
    ])->save();

    // Update recurrence field.
    $field_storage = FieldStorageConfig::loadByName('entity_test', 'occur_parent_reference');
    $field_storage->setSetting('target_field', 'second_recur_field');
    $field_storage->save();

    // Check third party settings.
    $old_target_field = FieldStorageConfig::load('entity_test.date_recur_field');
    $this->assertEmpty($old_target_field->getThirdPartySetting('date_occur', 'instance_reference_field'));
    $new_target_field = FieldStorageConfig::load('entity_test.second_recur_field');
    $this->assertEquals('entity_test.occur_parent_reference', $new_target_field->getThirdPartySetting('date_occur', 'instance_reference_field'));

    // And remove the field.
    $field_storage->delete();
    // Check third party settings.
    $old_target_field = FieldStorageConfig::load('entity_test.date_recur_field');
    $this->assertEmpty($old_target_field->getThirdPartySetting('date_occur', 'instance_reference_field'));
    $new_target_field = FieldStorageConfig::load('entity_test.second_recur_field');
    $this->assertEmpty($new_target_field->getThirdPartySetting('date_occur', 'instance_reference_field'));
  }

}
