<?php

namespace Drupal\Tests\date_occur\Kernel;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\date_occur\Plugin\DataType\DateOccurrenceInterface;
use Drupal\date_recur\Plugin\Field\FieldType\DateRecurItem;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\KernelTests\KernelTestBase;

/**
 * Instance reference field test.
 *
 * @group date_occur
 */
class ParentFieldTest extends KernelTestBase {

  /**
   * Entity Field Manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface|null
   */
  protected ?EntityFieldManagerInterface $entityFieldManager;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'date_occur',
    'entity_test',
    'datetime',
    'datetime_range',
    'date_recur',
    'field',
    'user',
  ];

  private FieldStorageConfig $recur_field_storage;

  private FieldConfig $recur_field_config;

  private FieldStorageConfig $parent_reference_field_storage;

  private FieldConfig $parent_reference_field_config;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('entity_test');
    $this->installConfig('date_recur');
    $this->installConfig('date_occur');
    $this->entityFieldManager = \Drupal::service('entity_field.manager');

    // Add a date recur field to test entity.
    $this->recur_field_storage = FieldStorageConfig::create([
      'entity_type' => 'entity_test',
      'field_name' => 'date_recur_field',
      'type' => 'date_recur',
      'settings' => [
        'datetime_type' => DateRecurItem::DATETIME_TYPE_DATETIME,
      ],
    ]);
    $this->recur_field_storage->save();

    $field = [
      'field_name' => 'date_recur_field',
      'entity_type' => 'entity_test',
      'bundle' => 'entity_test',
    ];
    $this->recur_field_config = FieldConfig::create($field);
    $this->recur_field_config->save();

    // Add a recurrence field to the test entity.
    $this->parent_reference_field_storage = FieldStorageConfig::create([
      'entity_type' => 'entity_test',
      'field_name' => 'occur_parent_reference',
      'type' => 'date_occur_parent',
      'settings' => [
        'target_type' => 'entity_test',
        'target_field' => 'date_recur_field',
      ],
    ]);
    $this->parent_reference_field_storage->save();

    $field = [
      'field_name' => 'occur_parent_reference',
      'entity_type' => 'entity_test',
      'bundle' => 'entity_test',
    ];
    $this->parent_reference_field_config = FieldConfig::create($field);
    $this->parent_reference_field_config->save();
  }

  public function testValues() {
    $recur_instance = EntityTest::create();
    $recur_instance->date_recur_field = [
      [
        'value' => '2008-06-16T00:00:00',
        'end_value' => '2008-06-16T06:00:00',
        'rrule' => 'FREQ=DAILY',
        'timezone' => 'Australia/Sydney',
      ],
    ];
    $recur_instance->save();
    $recur_instance = EntityTest::load($recur_instance->id());

    $occurrence_instance = $recur_instance->createDuplicate();
    // @todo should we know what the date field is, it logically can't have a
    // repeat value.
    $occurrence_instance->date_recur_field = [
      [
        'value' => '2008-06-17T01:00:00',
        'end_value' => '2008-06-17T07:00:00',
        'timezone' => 'Australia/Sydney',
      ],
    ];
    $occurrence_instance->occur_parent_reference = [
      [
        'target_id' => $recur_instance->id(),
        'field_delta' => 0,
        'value' => '2008-06-17T01:00:00',
        'end_value' => '2008-06-17T07:00:00',
      ],
    ];
    $occurrence_instance->save();
    $occurrence_instance = EntityTest::load($occurrence_instance->id());

    $this->assertEquals($recur_instance->id(), $occurrence_instance->occur_parent_reference->target_id);
    $this->assertEquals($recur_instance, $occurrence_instance->occur_parent_reference->entity);
    $this->assertEquals('2008-06-17T01:00:00', $occurrence_instance->occur_parent_reference->value);
    $this->assertEquals('2008-06-17T07:00:00', $occurrence_instance->occur_parent_reference->end_value);

    $occurrence = $occurrence_instance->occur_parent_reference->first()->getOccurrence();
    assert($occurrence instanceof DateOccurrenceInterface);
    $this->assertEquals($occurrence_instance->id(), $occurrence->getEntity()->id());
    $this->assertEquals($recur_instance->id(), $occurrence->getParent()->id());
    $this->assertEquals('2008-06-17T01:00:00', $occurrence->getStart()->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT));
    $this->assertEquals('2008-06-17T07:00:00', $occurrence->getEnd()->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT));
  }

}
