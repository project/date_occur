<?php

namespace Drupal\Tests\date_occur\Functional;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\Core\Entity\EntityInterface;
use Drupal\date_recur\Plugin\Field\FieldType\DateRecurItem;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Field formatter with multiple recur & occur fields on different bundles.
 *
 * @group date_occur
 */
class FieldFormatterMultipleTest extends FieldFormatterTest {

  /**
   * Test a second parent recurring date field entity.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected EntityInterface $secondEntity;

  /**
   * Entity view display for test entity type.
   *
   * @var \Drupal\Core\Entity\Display\EntityViewDisplayInterface
   */
  protected EntityViewDisplayInterface $secondViewDisplay;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    entity_test_create_bundle('second_entity_test');

    $fieldStorage = FieldStorageConfig::create([
      'entity_type' => 'entity_test',
      'field_name' => 'second_date_recur_field',
      'type' => 'date_recur',
      'settings' => [
        'datetime_type' => DateRecurItem::DATETIME_TYPE_DATETIME,
      ],
    ]);
    $fieldStorage->save();

    $field = [
      'field_name' => 'second_date_recur_field',
      'entity_type' => 'entity_test',
      'bundle' => 'second_entity_test',
    ];
    $fieldConfig = FieldConfig::create($field);
    $fieldConfig->save();

    $fieldStorage = FieldStorageConfig::create([
      'entity_type' => 'entity_test',
      'field_name' => 'second_date_parent_field',
      'type' => 'date_occur_parent',
      'settings' => [
        'target_type' => 'entity_test',
        'target_field' => 'second_date_recur_field',
      ],
    ]);
    $fieldStorage->save();

    $field = [
      'field_name' => 'second_date_parent_field',
      'entity_type' => 'entity_test',
      'bundle' => 'second_entity_test',
    ];
    $fieldConfig = FieldConfig::create($field);
    $fieldConfig->save();

    $recur_options = [
      'type' => 'date_recur_basic_formatter',
      'label' => 'hidden',
      'settings' => [
        'timezone_override' => '',
        'format_type' => 'html_datetime',
        'occurrence_format_type' => 'html_datetime',
        'same_end_date_format_type' => 'html_datetime',
      ],
    ];
    $occur_options = [
      'type' => 'date_occur_basic_formatter',
      'label' => 'hidden',
      'settings' => [
        'show_parent_title' => 'always',
        'interpreter' => 'default_interpreter',
      ],
    ];
    $this->secondViewDisplay = EntityViewDisplay::create([
      'targetEntityType' => 'entity_test',
      'bundle' => 'second_entity_test',
      'mode' => 'full',
      'status' => TRUE,
    ]);
    $this->secondViewDisplay->save();
    $this->secondViewDisplay->setComponent('second_date_recur_field', $recur_options);
    $this->secondViewDisplay->setComponent('second_date_parent_field', $occur_options);
    $this->secondViewDisplay->save();

    $this->secondEntity = EntityTest::create([
      'name' => $this->randomString(),
      'user_id' => $this->edit_user->id(),
      'type' => 'second_entity_test',
    ]);
    $rrule = 'FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR';
    $this->secondEntity->second_date_recur_field = [
      [
        'value' => '2008-06-15T22:00:00',
        'end_value' => '2008-06-16T06:00:00',
        'rrule' => $rrule,
        'timezone' => $this->timezone,
      ],
    ];
    $this->secondEntity->save();
  }

  /**
   * Test formatter on occurrence parent field.
   */
  public function testSecondOccurrenceFormatter() {
    $override = $this->createSecondOverride();

    // Parent title shown.
    $this->drupalGet($override->toUrl()->toString());
    $parent_title = $this->getSession()->getPage()->find('xpath', "//div[@class='parent-title']");
    $this->assertEquals($this->secondEntity->label(), $parent_title->getText());
    // Parent title cache context.
    $this->secondEntity->set('name', $override->label());
    $this->secondEntity->save();
    $this->drupalGet($override->toUrl()->toString());
    $parent_title = $this->getSession()->getPage()->find('xpath', "//div[@class='parent-title']");
    $this->assertEquals($this->secondEntity->label(), $parent_title->getText());

    // Switch to only show when different.
    $occur_options = $this->secondViewDisplay->getComponent('second_date_parent_field');
    $occur_options['settings']['show_parent_title'] = 'different';
    $this->secondViewDisplay->setComponent('second_date_parent_field', $occur_options);
    $this->secondViewDisplay->save();
    // Not shown.
    $this->drupalGet($override->toUrl()->toString());
    $parent_title = $this->getSession()->getPage()->find('xpath', "//div[@class='parent-title']");
    $this->assertEmpty($parent_title);
    // Change name. Shown.
    $override->set('name', $this->randomString());
    $override->save();
    $this->drupalGet($override->toUrl()->toString());
    $parent_title = $this->getSession()->getPage()->find('xpath', "//div[@class='parent-title']");
    $this->assertEquals($this->secondEntity->label(), $parent_title->getText());

    // Switch to never show.
    $occur_options['settings']['show_parent_title'] = '';
    $this->secondViewDisplay->setComponent('second_date_parent_field', $occur_options);
    $this->secondViewDisplay->save();
    $this->drupalGet($override->toUrl()->toString());
    $parent_title = $this->getSession()->getPage()->find('xpath', "//div[@class='parent-title']");
    $this->assertEmpty($parent_title);
  }

  /**
   * Create an overrided occurrence on the first Friday of the sequence.
   */
  protected function createSecondOverride() {
    $override = EntityTest::create([
      'name' => $this->randomString(),
      'name_id' => $this->edit_user->id(),
      'type' => 'second_entity_test',
    ]);
    $override->second_date_recur_field = [
      [
        // Fri.
        'value' => '2008-06-19T22:00:00',
        'end_value' => '2008-06-20T06:00:00',
        'timezone' => $this->timezone,
      ],
    ];
    $override->second_date_parent_field = [
      'target_id' => $this->secondEntity->id(),
      'field_delta' => '',
      'value' => '2008-06-19T22:00:00',
      'end_value' => '2008-06-20T06:00:00',
    ];
    $override->save();

    return $override;
  }

}
