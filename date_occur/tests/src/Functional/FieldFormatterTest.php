<?php

namespace Drupal\Tests\date_occur\Functional;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\Core\Entity\EntityInterface;
use Drupal\date_recur\Plugin\Field\FieldType\DateRecurItem;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\BrowserTestBase;

/**
 * Occur field formatter.
 *
 * @group date_occur
 */
class FieldFormatterTest extends BrowserTestBase {

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = [
    'entity_test',
    'field_ui',
    'path',
    'date_occur',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Test parent recurring date field entity.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected EntityInterface $entity;

  /**
   * Entity view display for test entity type.
   *
   * @var \Drupal\Core\Entity\Display\EntityViewDisplayInterface
   */
  protected EntityViewDisplayInterface $viewDisplay;

  /**
   * The timezone used.
   *
   * @var string
   */
  protected string $timezone = 'Indian/Christmas';

  /**
   * The name of the content type created for testing purposes.
   *
   * @var string
   */
  protected $type;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $fieldStorage = FieldStorageConfig::create([
      'entity_type' => 'entity_test',
      'field_name' => 'date_recur_field',
      'type' => 'date_recur',
      'settings' => [
        'datetime_type' => DateRecurItem::DATETIME_TYPE_DATETIME,
      ],
    ]);
    $fieldStorage->save();

    $field = [
      'field_name' => 'date_recur_field',
      'entity_type' => 'entity_test',
      'bundle' => 'entity_test',
    ];
    $fieldConfig = FieldConfig::create($field);
    $fieldConfig->save();

    $fieldStorage = FieldStorageConfig::create([
      'entity_type' => 'entity_test',
      'field_name' => 'date_parent_field',
      'type' => 'date_occur_parent',
      'settings' => [
        'target_type' => 'entity_test',
        'target_field' => 'date_recur_field',
      ],
    ]);
    $fieldStorage->save();

    $field = [
      'field_name' => 'date_parent_field',
      'entity_type' => 'entity_test',
      'bundle' => 'entity_test',
    ];
    $fieldConfig = FieldConfig::create($field);
    $fieldConfig->save();

    $recur_options = [
      'type' => 'date_recur_basic_formatter',
      'label' => 'hidden',
      'settings' => [
        'timezone_override' => '',
        'format_type' => 'html_datetime',
        'occurrence_format_type' => 'html_datetime',
        'same_end_date_format_type' => 'html_datetime',
      ],
    ];
    $occur_options = [
      'type' => 'date_occur_basic_formatter',
      'label' => 'hidden',
      'settings' => [
        'show_parent_title' => 'always',
        'interpreter' => 'default_interpreter',
      ],
    ];
    $this->viewDisplay = EntityViewDisplay::create([
      'targetEntityType' => 'entity_test',
      'bundle' => 'entity_test',
      'mode' => 'full',
      'status' => TRUE,
    ]);
    $this->viewDisplay->save();
    $this->viewDisplay->setComponent('date_recur_field', $recur_options);
    $this->viewDisplay->setComponent('date_parent_field', $occur_options);
    $this->viewDisplay->save();

    // Create user with access.
    $this->edit_user = $this->drupalCreateUser([
      'view test entity',
    ]);
    $this->drupalLogin($this->edit_user);

    $this->entity = EntityTest::create([
      'name' => $this->randomString(),
      'user_id' => $this->edit_user->id(),
    ]);
    $rrule = 'FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR';
    $this->entity->date_recur_field = [
      [
        'value' => '2008-06-15T22:00:00',
        'end_value' => '2008-06-16T06:00:00',
        'rrule' => $rrule,
        'timezone' => $this->timezone,
      ],
    ];
    $this->entity->save();
  }

  /**
   * Test formatter on occurrence parent field.
   */
  public function testOccurrenceFormatter() {
    $override = $this->createOverride();

    // Parent title shown.
    $this->drupalGet($override->toUrl()->toString());
    $parent_title = $this->getSession()->getPage()->find('xpath', "//div[@class='parent-title']");
    $this->assertEquals($this->entity->label(), $parent_title->getText());
    // Parent title cache context.
    $this->entity->set('name', $override->label());
    $this->entity->save();
    $this->drupalGet($override->toUrl()->toString());
    $parent_title = $this->getSession()->getPage()->find('xpath', "//div[@class='parent-title']");
    $this->assertEquals($this->entity->label(), $parent_title->getText());

    // Switch to only show when different.
    $occur_options = $this->viewDisplay->getComponent('date_parent_field');
    $occur_options['settings']['show_parent_title'] = 'different';
    $this->viewDisplay->setComponent('date_parent_field', $occur_options);
    $this->viewDisplay->save();
    // Not shown.
    $this->drupalGet($override->toUrl()->toString());
    $parent_title = $this->getSession()->getPage()->find('xpath', "//div[@class='parent-title']");
    $this->assertEmpty($parent_title);
    // Change name. Shown.
    $override->set('name', $this->randomString());
    $override->save();
    $this->drupalGet($override->toUrl()->toString());
    $parent_title = $this->getSession()->getPage()->find('xpath', "//div[@class='parent-title']");
    $this->assertEquals($this->entity->label(), $parent_title->getText());

    // Switch to never show.
    $occur_options['settings']['show_parent_title'] = '';
    $this->viewDisplay->setComponent('date_parent_field', $occur_options);
    $this->viewDisplay->save();
    $this->drupalGet($override->toUrl()->toString());
    $parent_title = $this->getSession()->getPage()->find('xpath', "//div[@class='parent-title']");
    $this->assertEmpty($parent_title);
  }

  /**
   * Create an overrided occurrence on the first Friday of the sequence.
   */
  protected function createOverride() {
    $override = EntityTest::create([
      'name' => $this->randomString(),
      'name_id' => $this->edit_user->id(),
    ]);
    $override->date_recur_field = [
      [
        // Fri.
        'value' => '2008-06-19T22:00:00',
        'end_value' => '2008-06-20T06:00:00',
        'timezone' => $this->timezone,
      ],
    ];
    $override->date_parent_field = [
      'target_id' => $this->entity->id(),
      'field_delta' => '',
      'value' => '2008-06-19T22:00:00',
      'end_value' => '2008-06-20T06:00:00',
    ];
    $override->save();

    return $override;
  }

}
