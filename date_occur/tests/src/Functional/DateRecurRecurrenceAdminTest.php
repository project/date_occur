<?php

namespace Drupal\Tests\date_occur\Functional;

use Drupal\date_recur\Plugin\Field\FieldType\DateRecurItem;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\field_ui\Traits\FieldUiTestTrait;

/**
 * Tests for the administrative UI.
 *
 * @group date_recur_occur
 */
class DateRecurRecurrenceAdminTest extends BrowserTestBase {

  use FieldUiTestTrait;

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = [
    'entity_test',
    'field_ui',
    'path',
    'block',
    'date_occur',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The name of the content type created for testing purposes.
   *
   * @var string
   */
  protected $type;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->drupalPlaceBlock('system_breadcrumb_block');
    $this->drupalPlaceBlock('local_actions_block');
    $this->drupalPlaceBlock('local_tasks_block');
    $this->drupalPlaceBlock('page_title_block');

    $fieldStorage = FieldStorageConfig::create([
      'entity_type' => 'entity_test',
      'field_name' => 'date_recur_field',
      'type' => 'date_recur',
      'settings' => [
        'datetime_type' => DateRecurItem::DATETIME_TYPE_DATETIME,
      ],
    ]);
    $fieldStorage->save();

    $field = [
      'field_name' => 'date_recur_field',
      'entity_type' => 'entity_test',
      'bundle' => 'entity_test',
    ];
    $this->fieldConfig = FieldConfig::create($field);
    $this->fieldConfig->save();

    // Create test user.
    $admin_user = $this->drupalCreateUser([
      'administer entity_test content',
      'administer entity_test fields',
      'administer entity_test display',
    ]);
    $this->drupalLogin($admin_user);
  }

  public function testStorage() {
    $this->fieldUIAddNewField('entity_test/structure/entity_test', 'date_occur_parent', 'Date recurr occurrence', 'date_occur_parent', ['settings[target_field]' => 'entity_test.date_recur_field']);
    $field_storage = FieldStorageConfig::loadByName('entity_test', 'field_date_occur_parent');
    $this->assertSame('entity_test', $field_storage->getSetting('target_type'));
    $this->assertSame('date_recur_field', $field_storage->getSetting('target_field'));
    $field = FieldConfig::loadByName('entity_test', 'entity_test', 'field_date_occur_parent');
    $this->assertSame('default:entity_test', $field->getSetting('handler'));
  }

}
