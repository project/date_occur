<?php

namespace Drupal\date_occur;

use Drupal\Core\Entity\EntityInterface;
use Drupal\date_occur\Plugin\DataType\DateOccurrenceInterface;
use Drupal\date_recur\Plugin\Field\FieldType\DateRecurItem;

/**
 * Recurrence reference helper service interface.
 */
interface OccurReferenceHelperInterface {

  /**
   * Retrive occurrence instance entity if it exists.
   *
   * @param string $instance_entity_type
   *   The entity type id of occurrence instances.
   * @param string $instance_reference_field
   *   The field name of the reference field to the parent occurrence.
   * @param string $parent_id
   *   The entity id of the recurring parent.
   * @param string $start_value
   *   The start date as stored by the parent field value.
   * @param string $end_value
   *   The start date as stored by the parent field value_end.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The occurrence entity if overriden, or the parent if not.
   */
  public function retrieveInstanceEntity($instance_entity_type, $instance_reference_field, $parent_id, $start_value, $end_value): ?EntityInterface;

  /**
   * Set occurrence instance.
   *
   * The occurrence will either be the original recurring instance, or an
   * overridden instance if there is one.
   *
   * @param \Drupal\date_occur\Plugin\DataType\DateOccurrence $occurrence
   *   Original date occurrence.
   * @param string $field_name
   *   Recurring date reference field name on parent entity.
   *
   * @return \Drupal\date_occur\Plugin\DataType\DateOccurrenceInterface
   *   The occourrence, either original if not overriden, or an instance.
   */
  public function setOccurrenceInstance(DateOccurrenceInterface $occurrence, $field_name): DateOccurrenceInterface;

  /**
   * Get next occurrences from now or start occurrence.
   *
   * @param \Drupal\date_recur\Plugin\Field\FieldType\DateRecurItem $item
   *   Recurring date field item.
   * @param string $occur_entity_type
   *   Entity type ID for the occurrence entity.
   * @param string $occur_field_name
   *   Occurrence entity occur field name.
   * @param string $date_field_name
   *   Field name of the Recurring Date field.
   * @param int $max_occurrences
   *   The maximum number of occurrences to return.
   * @param \Drupal\date_occur\Plugin\DataType\DateOccurrenceInterface $start_occurrence
   *   The occurrence to start generating occurrences from.
   *   Optional default 'now'.
   *
   * @return \Drupal\date_occur\Plugin\DataType\DateOccurrenceInterface[]
   *   The occurrences.
   */
  public function getNextOccurrences(DateRecurItem $item, string $occur_entity_type, string $occur_field_name, string $date_field_name, int $max_occurrences, ?DateOccurrenceInterface $start_occurrence): array;

}
