<?php

declare(strict_types = 1);

namespace Drupal\date_occur\Event;

/**
 * Reacts to changes on entity types.
 */
final class DateOccurInstanceEvents {

  /**
   * An entity containing an instance parent occurrence reference is saved.
   */
  public const PARENT_FIELD_VALUE_SAVE = 'date_occur_parent_field_value_save';

  /**
   * An entity containing an instance parent occurrence reference is saved.
   */
  public const PARENT_FIELD_ENTITY_DELETE = 'date_occur_parent_field_entity_delete';

  /**
   * An entity revision an instance parent occurrence reference is saved.
   */
  public const PARENT_FIELD_REVISION_DELETE = 'date_occur_parent_field_entity_revision_delete';

}
