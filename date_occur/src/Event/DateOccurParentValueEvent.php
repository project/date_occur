<?php

declare(strict_types = 1);

namespace Drupal\date_occur\Event;

use Drupal\date_occur\Plugin\Field\FieldType\DateOccurParentList;
use Drupal\Component\EventDispatcher\Event;

/**
 * Event dispatched when an entity containing a date recur field is modified.
 */
class DateOccurParentValueEvent extends Event {

  /**
   * The field list.
   *
   * @var \Drupal\date_occur\Plugin\Field\FieldType\DateOccurParentList
   */
  protected DateOccurParentList $field;

  /**
   * Whether the entity was created.
   *
   * @var bool
   */
  protected bool $insert;

  /**
   * DateOccurParentValueEvent constructor.
   *
   * @param \Drupal\date_occur\Plugin\Field\FieldType\DateOccurParentList $field
   *   The date instance parent reference item list.
   * @param bool $insert
   *   Specifies whether the entity was created.
   */
  public function __construct(DateOccurParentList $field, $insert) {
    $this->field = $field;
    $this->insert = $insert;
  }

  /**
   * Get the field list.
   *
   * The field cannot be changed because the entity has already been saved.
   *
   * @return \Drupal\date_occur\Plugin\Field\FieldType\DateOccurParentList
   *   The date instance parent reference field item list.
   */
  public function getField(): DateOccurParentList {
    return $this->field;
  }

  /**
   * Get whether the entity was created.
   *
   * @return bool
   *   Whether the entity was created.
   */
  public function isInsert(): bool {
    return $this->insert;
  }

}
