<?php

namespace Drupal\date_occur;

use Drupal\Core\Config\Entity\ThirdPartySettingsInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\TypedDataManagerInterface;
use Drupal\date_occur\Plugin\DataType\DateOccurrenceInterface;
use Drupal\date_recur\Plugin\Field\FieldType\DateRecurItem;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * Recurrence reference helper service.
 */
class OccurReferenceHelper implements OccurReferenceHelperInterface {

  /**
   * Constructs a RecurrenceReference object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\TypedData\TypedDataManagerInterface $typedDataManager
   *   Typed Data Manager.
   */
  public function __construct(protected EntityTypeManagerInterface $entityTypeManager, protected TypedDataManagerInterface $typedDataManager) {
  }

  /**
   * {@inheritdoc}
   */
  public function retrieveInstanceEntity($occurrence_entity, $recurrence_reference_field, $parent_id, $start_value, $end_value): ?EntityInterface {
    $query = $this->entityTypeManager->getStorage($occurrence_entity)->getQuery();
    $query->condition($recurrence_reference_field . '.target_id', $parent_id);
    $query->condition($recurrence_reference_field . '.value', $start_value);
    $query->condition($recurrence_reference_field . '.end_value', $end_value);
    $query->accessCheck(FALSE);
    $result = $query->execute();
    if ($result) {
      return $this->entityTypeManager->getStorage($occurrence_entity)->load(reset($result));
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setOccurrenceInstance(DateOccurrenceInterface $occurrence, $date_field_name): DateOccurrenceInterface {
    $value = $occurrence->getValue();
    $parent = $value['entity'];
    $start_date = new DrupalDateTime($value['start_date']);
    $end_date = new DrupalDateTime($value['end_date']);

    $recurrence_field_definition = $parent->getFieldDefinition($date_field_name);
    $recurrence_field_storage = $recurrence_field_definition->getFieldStorageDefinition();

    if ($recurrence_field_storage instanceof ThirdPartySettingsInterface) {
      [$occurrence_entity_type, $occurrence_field] = explode('.', $recurrence_field_storage->getThirdPartySetting('date_occur', 'instance_reference_field'));

      if ($occurrence_entity = $this->retrieveInstanceEntity($occurrence_entity_type, $occurrence_field, $parent->id(), $start_date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT, ['timezone' => DateTimeItemInterface::STORAGE_TIMEZONE]), $end_date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT, ['timezone' => DateTimeItemInterface::STORAGE_TIMEZONE]))) {
        $value['entity'] = $occurrence_entity;
        $value['parent_entity'] = $parent;
        $value['start_date'] = $occurrence_entity->get($date_field_name)->start_date->format('c', ['timezone' => DateTimeItemInterface::STORAGE_TIMEZONE]);
        $value['end_date'] = $occurrence_entity->get($date_field_name)->end_date->format('c', ['timezone' => DateTimeItemInterface::STORAGE_TIMEZONE]);
        $occurrence->setValue($value);
      }
    }

    return $occurrence;
  }

  /**
   * {@inheritdoc}
   */
  public function getNextOccurrences(DateRecurItem $item, string $occur_entity_type, string $occur_field_name, string $date_field_name, int $max_occurrences, ?DateOccurrenceInterface $start_occurrence): array {
    $start_date = $start_occurrence ? $start_occurrence->getStart()->getPhpDateTime() : new \DateTime('now');
    $occurrences = $this->getItemOccurrences($item, $max_occurrences, $start_date);
    if (count($occurrences)) {
      $occurrences = $this->overrideOccurrences($occurrences, $occur_entity_type, $occur_field_name, $date_field_name, $item->getEntity()->id());
    }
    return $occurrences;
  }

  protected function getItemOccurrences(DateRecurItem $item, int $maxOccurrences, \DateTimeInterface $start) {  
    $item_definition = $this->typedDataManager->createDataDefinition('date_occurrence');
    $count = 0;
    $occurrences = [];
    foreach ($item->getHelper()->getOccurrences($start, NULL, $maxOccurrences + 1) as $date_range) {
      if ($start == $date_range->getStart()) {
        continue;
      }
      if (++$count > $maxOccurrences) {
        break;
      }
      $occurrences[
        $date_range
          ->getStart()
          ->setTimeZone(new \DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE))
          ->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT)
        . '--' .
        $date_range
          ->getEnd()
          ->setTimeZone(new \DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE))
          ->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT)
      ] = $this->typedDataManager->create($item_definition, [
        'start_date' => $date_range->getStart()->format('c'),
        'end_date' => $date_range->getEnd()->format('c'),
        'entity' => $item->getEntity(),
        'field_delta' => 0,
      ]);
    }
    return $occurrences;
  }

  protected function overrideOccurrences(array $occurrences, string $occur_entity_type, string $occur_field_name, string $date_field_name, int $recur_item_id): array {
    // @todo DI 
    $query = \Drupal::entityQuery($occur_entity_type);
    $query->condition($occur_field_name . '.target_id', $recur_item_id);
    $date_conditions = $query->orConditionGroup();
    // Drupal makes a join for each set of conditions for the field. It will
    // explode, but it's not expected that so many rows would be shown.
    // Alternative? Drupal doesn't need to make a join every time? How to stop
    // it? Or one query for each occurrence? Then it could be included in getNextOccurrences.
    foreach ($occurrences as $occurrence) {
      $occur_condition = $query->andConditionGroup();
      $occur_condition->condition(
        $occur_field_name . '.value',
        $occurrence->get('start_date')
          ->getDateTime()
          ->setTimeZone(new \DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE))
          ->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT)
      );
      $occur_condition->condition(
        $occur_field_name . '.end_value',
        $occurrence->get('end_date')
          ->getDateTime()
          ->setTimeZone(new \DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE))
          ->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT)
      );
      $date_conditions->condition($occur_condition);
    }
    $query->condition($date_conditions);
    $query->accessCheck(TRUE);
    $ids = $query->execute();

    $item_definition = $this->typedDataManager->createDataDefinition('date_occurrence');
    // @todo DI
    foreach (\Drupal::entityTypeManager()->getStorage($occur_entity_type)->loadMultiple($ids) as $override) {
      $overridden_entity = $occurrences[$override->$occur_field_name->value . '--' . $override->$occur_field_name->end_value]->getEntity();
      $occurrences[$override->$occur_field_name->value . '--' . $override->$occur_field_name->end_value] = $this->typedDataManager->create($item_definition, [
        'start_date' => $override->$date_field_name->first()->start_date->format('c'),
        'end_date' => $override->$date_field_name->first()->end_date->format('c'),
        'entity' => $override,
        'field_delta' => 0,
        'parent_entity' => $overridden_entity,
      ]);
    }
    return $occurrences;
  }

  /**
   * @todo add to interface
   * @todo sapi this has changed
   */
  static public function getInstanceReferenceFieldForRecurField(FieldStorageDefinitionInterface $recur_field_storage) {
    if ($recur_field_storage instanceof ThirdPartySettingsInterface) {
      return $recur_field_storage->getThirdPartySetting('date_occur', 'instance_reference_field');
    }

    return '';
  }

}
