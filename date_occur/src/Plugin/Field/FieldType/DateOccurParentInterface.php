<?php

namespace Drupal\date_occur\Plugin\Field\FieldType;

use Drupal\Core\Entity\EntityInterface;
use Drupal\date_occur\Plugin\DataType\DateOccurrenceInterface;
use Drupal\date_recur\Plugin\Field\FieldType\DateRecurItem;

/**
 * Recurring date occurrence reference field item interface.
 */
interface DateOccurParentInterface {

  /**
   * Get Occurrence datatype for the field item.
   *
   * @return \Drupal\date_occur\Plugin\DataType\DateOccurrenceInterface
   *   The occurrence.
   */
  public function getOccurrence(): DateOccurrenceInterface;

  /**
   * Get parent recur field item.
   */
  public function getParentDateRecur(): DateRecurItem;

}
