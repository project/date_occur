<?php

namespace Drupal\date_occur\Plugin\Field\FieldType;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\date_occur\Plugin\DataType\DateOccurrenceInterface;
use Drupal\date_recur\Plugin\Field\FieldType\DateRecurItem;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Defines the 'date_occur_parent' field type.
 *
 * @FieldType(
 *   id = "date_occur_parent",
 *   label = @Translation("Recurring date occurrence reference"),
 *   description = @Translation("Field for occurrence instance reference to recurring date occurrence."),
 *   list_class = "\Drupal\date_occur\Plugin\Field\FieldType\DateOccurParentList",
 *   default_formatter = "entity_reference_label",
 *   cardinality = 1,
 * )
 */
class DateOccurParent extends EntityReferenceItem implements DateOccurParentInterface {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'target_field' => '',
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = parent::propertyDefinitions($field_definition);

    $properties['entity']
      ->setDescription(new TranslatableMarkup('The parent date entity'));

    $properties['field_delta'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Parent field delta'))
      ->setRequired(TRUE);

    $properties['value'] = DataDefinition::create('datetime_iso8601')
      ->setLabel(new TranslatableMarkup('Start date value'))
      ->setRequired(TRUE);

    $properties['end_value'] = DataDefinition::create('datetime_iso8601')
      ->setLabel(new TranslatableMarkup('End date value'))
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = parent::schema($field_definition);

    $schema['columns']['field_delta'] = [
      'description' => 'Parent field delta.',
      'type' => 'int',
      'unsigned' => TRUE,
    ];

    $schema['columns']['value'] = [
      'description' => 'The start date value.',
      'type' => 'varchar',
      'length' => 20,
    ];
    $schema['columns']['end_value'] = [
      'description' => 'The end date value.',
      'type' => 'varchar',
      'length' => 20,
    ];

    $schema['indexes']['recurrence'] = ['target_id', 'field_delta', 'value', 'end_value'];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $element['target_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Recurring date field to override'),
      '#default_value' => $this->getSetting('target_field') ? $this->getSetting('target_type') . '.' . $this->getSetting('target_field') : '',
      '#required' => TRUE,
      '#disabled' => $has_data,
      '#size' => 1,
    ];

    // This was originally designed with the idea you could reference another
    // entity, but covering all the cases got way too confusing for the one use
    // case it's being written for.
    $options = [];
    $field = $form_state->getFormObject()->getEntity();
    $entity_type_id = $field->getTargetEntityTypeId();
    $entityFieldManager = \Drupal::service('entity_field.manager');
    $usage = $entityFieldManager->getFieldMapByFieldType('date_recur');
    $fields = $usage[$entity_type_id];
    $entity_fields = [];
    foreach ($fields as $field_name => $field_map) {
        $entity_fields[$entity_type_id . '.' . $field_name] = $field_name . ' (' . implode(', ', $field_map['bundles']) . ')';
    }
    $options[$entity_type_id] = $entity_fields;
    $element['target_field']['#options'] = $options;

    $form['#entity_builders'][] = [static::class, 'storageSettingsBuild'];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $field = $form_state->getFormObject()->getEntity();
    $entity_type_id = $field->getTargetEntityTypeId();
    $form = parent::fieldSettingsForm($form, $form_state);
    $form['handler'] = [
      '#type' => 'value',
      '#value' => 'default:' . $entity_type_id,
    ];
    $form['handler_settings'] = [
      '#type' => 'value',
      '#value' => [],
    ];
    return $form;
  }

  /**
   * Callback storageSettingsForm entityBuilder.
   */
  public static function storageSettingsBuild($entity_type_id, $entity, &$form, FormStateInterface $form_state) {
    $form_settings = $form_state->getValue('settings');
    $target_field = FieldStorageConfig::load($form_settings['target_field']);

    $settings = $entity->get('settings');
    $settings['target_type'] = $target_field->getTargetEntityTypeId();
    $settings['target_field'] = $target_field->getName();
    $entity->set('settings', $settings);
  }

  /**
   * {@inheritdoc}
   */
  public static function calculateStorageDependencies(FieldStorageDefinitionInterface $field_definition) {
    $dependencies = parent::calculateStorageDependencies($field_definition);

    $entity_type = $field_definition->getSetting('target_type');
    $field_name = $field_definition->getSetting('target_field');
    $dependencies['config'][] = "field.storage.{$entity_type}.{$field_name}";
    return $dependencies;
  }

  /**
   * {@inheritdoc}
   */
  public function getOccurrence(): DateOccurrenceInterface {
    $definition = \Drupal::typedDataManager()->createDataDefinition('date_occurrence');
    $start = new DrupalDateTime($this->value, DateTimeItemInterface::STORAGE_TIMEZONE);
    $end = new DrupalDateTime($this->end_value, DateTimeItemInterface::STORAGE_TIMEZONE);
    return \Drupal::typedDataManager()->create($definition, [
      'start_date' => $start->format('c'),
      'end_date' => $end->format('c'),
      'parent_entity' => $this->entity,
      'entity' => $this->getEntity(),
      'field_delta' => $this->field_delta,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getParentDateRecur(): DateRecurItem {
    $field = $this->entity->{$this->getSetting('target_field')};
    $item = $field->first();
    return $item;
  }

}
