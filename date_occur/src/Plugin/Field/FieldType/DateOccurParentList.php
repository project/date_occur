<?php

declare(strict_types = 1);

namespace Drupal\date_occur\Plugin\Field\FieldType;

use Drupal\Core\Field\EntityReferenceFieldItemList;
use Drupal\date_occur\Event\DateOccurInstanceEvents;
use Drupal\date_occur\Event\DateOccurParentValueEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Date occurrence instance parent recurring date reference item list.
 */
class DateOccurParentList extends EntityReferenceFieldItemList {

  /**
   * An event dispatcher, primarily for unit testing purposes.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface|null
   */
  protected ?EventDispatcherInterface $eventDispatcher = NULL;

  /**
   * {@inheritdoc}
   */
  public function postSave($update): bool {
    parent::postSave($update);
    $event = new DateOccurParentValueEvent($this, !$update);
    $this->getDispatcher()->dispatch($event, DateOccurInstanceEvents::PARENT_FIELD_VALUE_SAVE);
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function delete(): void {
    parent::delete();
    $event = new DateOccurParentValueEvent($this, FALSE);
    $this->getDispatcher()->dispatch($event, DateOccurInstanceEvents::PARENT_FIELD_ENTITY_DELETE);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteRevision(): void {
    parent::deleteRevision();
    $event = new DateOccurParentValueEvent($this, FALSE);
    $this->getDispatcher()->dispatch($event, DateOccurInstanceEvents::PARENT_FIELD_REVISION_DELETE);
  }

  /**
   * Get the event dispatcher.
   *
   * @return \Symfony\Component\EventDispatcher\EventDispatcherInterface
   *   The event dispatcher.
   */
  protected function getDispatcher(): EventDispatcherInterface {
    if (isset($this->eventDispatcher)) {
      return $this->eventDispatcher;
    }
    return \Drupal::service('event_dispatcher');
  }

  /**
   * Set the event dispatcher.
   *
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   The event dispatcher.
   */
  public function setEventDispatcher(EventDispatcherInterface $eventDispatcher): void {
    $this->eventDispatcher = $eventDispatcher;
  }

}
