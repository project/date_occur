<?php

declare(strict_types = 1);

namespace Drupal\date_occur\Plugin\Field\FieldFormatter;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\date_recur\Entity\DateRecurInterpreterInterface;

/**
 * Recurring date occurrence basic field formatter.
 *
 * @FieldFormatter(
 *   id = "date_occur_basic_formatter",
 *   label = @Translation("Occurrence basic formatter"),
 *   field_types = {
 *     "date_occur_parent"
 *   }
 * )
 */
class DateOccurParentBasicFormatter extends EntityReferenceFormatterBase {

  /**
   * Constructs a new DateOccurParentBasicFormatter.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Third party settings.
   * @param \Drupal\Core\Entity\EntityStorageInterface $dateRecurInterpreterStorage
   *   The date recur interpreter entity storage.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, protected EntityStorageInterface $dateRecurInterpreterStorage) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager')->getStorage('date_recur_interpreter'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return [
      // Show number of occurrences.
      'show_parent_title' => '',
      'interpreter' => NULL,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $form = parent::settingsForm($form, $form_state);

    $form['show_parent_title'] = [
      '#type' => 'select',
      '#title' => $this->t('Show parent title'),
      '#description' => $this->t('Display the title of the parent item'),
      '#options' => [
        '' => $this->t('Never show title'),
        'different' => $this->t('Show if different'),
        'always' => $this->t('Always show title'),
      ],
      '#default_value' => $this->getSetting('show_parent_title'),
    ];

    $interpreterOptions = array_map(
      fn (DateRecurInterpreterInterface $interpreter): string => $interpreter->label() ?? (string) $this->t('- Missing label -'),
      $this->dateRecurInterpreterStorage->loadMultiple()
    );
    $form['interpreter'] = [
      '#type' => 'select',
      '#title' => $this->t('Recurring date interpreter'),
      '#description' => $this->t('Choose a plugin for converting rules into a human readable description.'),
      '#default_value' => $this->getSetting('interpreter'),
      '#options' => $interpreterOptions,
      '#required' => FALSE,
      '#empty_option' => $this->t('- Do not show interpreted rule -'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    if (!$items->isEmpty()) {
      $entity = $items->first()->getEntity();
    }
    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $parent) {
      if ($parent->id()) {
        $elements[$delta] = $this->viewItemEntity($parent, $entity);
      }
    }

    return $elements;
  }

  /**
   * Create render array for an individual item.
   *
   * @param \Drupal\Core\Entity\EntityInterface $parent
   *   The referenced parent entity.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The occurrence instance.
   *
   * @return array
   *   Render array.
   */
  protected function viewItemEntity(EntityInterface $parent, EntityInterface $entity): array {
    $build = [
      '#theme' => 'date_occur_basic_formatter',
    ];
    $cacheability = new CacheableMetadata();
    $cacheability->addCacheableDependency($parent);
    $recur_field_name = $this->fieldDefinition->getSetting('target_field');

    // Render the title.
    if ($show_title = $this->getSetting('show_parent_title')) {
      if (($show_title == 'different' && $entity->label() != $parent->label())
        || $show_title == 'always') {
        $build['#title'] = [
          '#plain_text' => $parent->label(),
        ];
      }
    }

    // Render the rule.
    if ($this->getSetting('interpreter')) {
      /** @var string|null $interpreterId */
      $interpreterId = $this->getSetting('interpreter');
      if ($interpreterId && ($interpreter = $this->dateRecurInterpreterStorage->load($interpreterId))) {
        assert($interpreter instanceof DateRecurInterpreterInterface);
        $rules = $parent->$recur_field_name->first()->getHelper()->getRules();
        $plugin = $interpreter->getPlugin();
        $cacheability->addCacheableDependency($interpreter);
        $build['#interpretation'] = $plugin->interpret($rules, 'en');
      }
    }

    $cacheability->applyTo($build);

    return $build;
  }

}
