<?php

namespace Drupal\date_occur\Plugin\DataType;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityInterface;

/**
 * Date occurrence date type interface.
 */
interface DateOccurrenceInterface {

  /**
   * Date range interval designator.
   *
   * Preferring '--' over '/' for use in URIs.
   */
  public const INTERVAL_DESIGNATOR = '--';

  /**
   * Get occurrence entity.
   *
   * This is either the entity defining with a Recurring Date field and a
   * recurrence rule, that has the specified occurrence; or it is an occurrence
   * instance entity that refers to a parent entity with a recurring date field
   * occurrence.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The occurrence entity.
   */
  public function getEntity(): EntityInterface;

  /**
   * Get recurrence parent if set.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The parent entity.
   */
  public function getParent(): ?EntityInterface;

  /**
   * Get occurrence start datetime.
   *
   * @return \Drupal\Core\Datetime\DrupalDateTime
   *   The occurrence start Drupal date time object.
   */
  public function getStart(): DrupalDateTime;

  /**
   * Get occurrence end datetime.
   *
   * @return \Drupal\Core\Datetime\DrupalDateTime
   *   The occurrence end Drupal date time object.
   */
  public function getEnd(): DrupalDateTime;

  /**
   * Recurrence range identifier.
   *
   * @return string
   *   ISO 8601 style date range. Using INTERVAL_DESIGNATOR.
   */
  public function getRangeId(): string;

}
