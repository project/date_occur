<?php

namespace Drupal\date_occur\Plugin\DataType;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\TypedData\Plugin\DataType\Map;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\date_recur\DateRange;

/**
 * Date occurrence data type.
 *
 * @DataType(
 *   id = "date_occurrence",
 *   label = @Translation("Date occurrence"),
 *   definition_class = "\Drupal\date_occur\TypedData\DateOccurrenceDefinition"
 * )
 */
class DateOccurrence extends Map implements DateOccurrenceInterface {

  /**
   * {@inheritdoc}
   */
  public function getEntity(): EntityInterface {
    return $this->get('entity')->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function getParent(): ?EntityInterface {
    return $this->get('parent_entity')->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function getStart(): DrupalDateTime {
    return $this->get('start_date')->getDateTime();
  }

  /**
   * {@inheritdoc}
   */
  public function getEnd(): DrupalDateTime {
    return $this->get('end_date')->getDateTime();
  }

  /**
   * {@inheritdoc}
   */
  public function getRangeId(): string {
    return static::buildOccurrenceDeltaIdentifier(new DateRange($this->get('start_date')->getDateTime()->getPhpDateTime(), $this->get('end_date')->getDateTime()->getPhpDateTime()));
  }

  /**
   * Build an identifier for a date range occurrence.
   *
   * @param \Drupal\date_recur\DateRange $occurrence
   *   The occurrence.
   *
   * @return string
   *   The identifier.
   */
  protected static function buildOccurrenceDeltaIdentifier(DateRange $occurrence): string {
    return implode(DateOccurrenceInterface::INTERVAL_DESIGNATOR, array_map(function ($item) {
      $cloned = clone $item;
      $cloned->setTimeZone(new \DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE));
      // @todo Might as well write timezone UTC with Z here. Is it to vary? It could.
      return $cloned->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT . '\Z');
    }, [$occurrence->getStart(), $occurrence->getEnd()]));
  }

}
