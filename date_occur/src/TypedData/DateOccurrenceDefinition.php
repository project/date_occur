<?php

namespace Drupal\date_occur\TypedData;

use Drupal\Core\Entity\TypedData\EntityDataDefinition;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\MapDataDefinition;

/**
 * Date occurrence typed date definition.
 *
 * @see \Drupal\date_occur\Plugin\DataType\DateRecurOccurrence
 */
class DateOccurrenceDefinition extends MapDataDefinition {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions() {
    return [
      'start_date' => DataDefinition::create('datetime_iso8601')
        ->setLabel($this->t('Start date'))
        ->setDescription($this->t('The computed start DateTime object.')),
      'end_date' => DataDefinition::create('datetime_iso8601')
        ->setLabel($this->t('End date'))
        ->setDescription($this->t('The computed end DateTime object.')),
      'field_delta' => DataDefinition::create('integer')
        ->setLabel($this->t('Field delta'))
        ->setDescription($this->t('The field delta used to generate the occurrence.')),
      'entity' => EntityDataDefinition::create()
        ->setLabel($this->t('Entity')),
      'parent_entity' => EntityDataDefinition::create()
        ->setLabel($this->t('Parent entity'))
        ->setDescription($this->t('If the entity is an occurrence instance the entity with the repeating date.')),
    ];
  }

}
